
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {
  DrawerNavigator,
} from 'react-navigation';
import HomeScreen from './src/Second';
//import HomeScreen from './src/gbgAuth';
//import HomeScreen from './src/UploadFromCameraRoll';
import Second from './src/Second';
import Third from './src/Third';
import Forth from './src/Login';
//import Forth from './src/Forth';
import Fifth from './src/Fifth1';
import Sixth from './src/Sixth';
import Seventh from './src/Seventh';
import Eight from './src/Eight';
import Nine from './src/Nine';
import Welcome from './src/Welcome';
import Sidebar from './sidebar';
import Logout from './src/Logout';
import TransactionHistory from './src/TransactionHistory';
import FeeCalculator from './src/FeeCalculator';
import Rate from './src/Rate/Rate';
import Beneficiary from './src/Beneficiary/Beneficiary';
import BeneficiaryList from './src/Beneficiary/BeneficiaryList';
import AddBeneficiary from './src/Beneficiary/addBenificiary';
import BeneficiarBankProfile from './src/Beneficiary/addBankProfile';
import UpdatePassword from './src/UserProfile/UpdatePassword';
import PurposePage from './src/Beneficiary/purposePage';
import ReviewPage from './src/Beneficiary/reviewPage';
import TransactionPage from './src/Beneficiary/TransactionPage';
import MyProfile from './src/UserProfile/MyProfile';
import SingleBeneficiary from './src/Beneficiary/SingleBeneficiary';
import EditBeneficiary from './src/Beneficiary/EditBeneficiary';
import EditBankDetails from './src/Beneficiary/EditBankProfile';
import UpdateMyProfile from './src/UserProfile/UpdateMyProfile';
import TransactionList from './src/Transaction/TransactionList';
import SingleTransaction from './src/Transaction/SingleTransaction';
import Privacy from './src/Privacy';
import Kyc from './src/Kyc/kyc'

import FbUserProfile from './src/UserProfile/addFbUserProfile';

const Forex = DrawerNavigator({
  Home: { screen: HomeScreen ,
  	header: null,
    navigationOptions: {
        header: null
    }},
  Second: { screen: Second,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Third: { screen: Third,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Forth: { screen: Forth,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Fifth: { screen: Fifth,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Sixth: { screen: Sixth,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Seventh: { screen: Seventh,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Eight: { screen: Eight,
  	header: null,
    navigationOptions: {
        header: null
    } },
    Nine: { screen: Nine,
      header: null,
      navigationOptions: {
          header: null
      } },
    Welcome: { screen: Welcome,
        header: null,
        navigationOptions: {
            header: null
        } },
    Logout: { screen: Logout,
          header: null,
          navigationOptions: {
              header: null
     } },
    TransactionHistory : {
            screen: TransactionHistory,
          header: null,
          navigationOptions: {
              header: null
          }
    },
    FeeCalculator : {
            screen: FeeCalculator,
            header: null,
            navigationOptions: {
                header: null,
              //  drawerLockMode: 'locked-closed'
            }
    },
    Rate : {
      screen : Rate,
      header : null,
      navigationOptions : {
        header:null
      }
    },
   
    BeneficiaryList : {
      screen : BeneficiaryList,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    Beneficiary : {
      screen : Beneficiary,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    AddBeneficiary : {
      screen : AddBeneficiary,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    BeneficiarBankProfile : {
      screen : BeneficiarBankProfile,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    UpdatePassword : {
      screen : UpdatePassword,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    PurposePage : {
      screen : PurposePage,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    ReviewPage : {
      screen : ReviewPage,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    TransactionPage : {
      screen : TransactionPage,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    MyProfile : {
      screen : MyProfile,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    SingleBeneficiary : {
      screen : SingleBeneficiary,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    EditBeneficiary : {
      screen : EditBeneficiary,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    EditBankDetails : {
      screen : EditBankDetails,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    UpdateMyProfile : {
      screen : UpdateMyProfile,
      header : null,
      navigationOptions : {
        header:null
      }
    },
    TransactionList : {
      screen : TransactionList,
      header : null,
      navigationOptions : {
        header : null
      }
    },
    SingleTransaction : {
      screen : SingleTransaction,
      header : null,
      navigationOptions : {
        header : null
      }
    },
    Privacy : {
      screen : Privacy,
      header : null,
      navigationOptions : {
        header : null
      }
    },
    Kyc : {
      screen : Kyc,
      header : null,
      navigationOptions : {
        header : null
      }
    },
    FbUserProfile : {
      screen : FbUserProfile,
      header : null,
      navigationOptions : {
        header : null,
      
      }
    }
},

{
  contentComponent: props => <Sidebar {...props} />,
  drawerPosition:'left',
 
},
);

export default Forex;
