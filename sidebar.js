import React, { Component } from 'react';
import {View,StyleSheet,TouchableHighlight,ScrollView,AppRegistry,Alert,Image,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView, TouchableOpacity,SafeAreaView} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import LocalData from './src/Components/LocalData';
import authData from './headerData';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch,Card,CardItem,Thumbnail} from 'native-base';
export default class Sidebar extends Component {
  constructor(props){
    super(props);
    this.state ={
      userName :  ''
    }
  }
  componentDidMount(){
    console.log(this.props);
    console.log('first userName State'+this.state.userName+'Global'+global.LoggedUsername);
    if(global.CustomerKey){
      console.log('function call ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
     // this.userProfile();
    }else{
      console.log('not call-----------------------------------------------------------------------')
    }
 //AsyncStorage.getItem("LoggedUsername").then((value) =)
   
 AsyncStorage.getItem("LoggedUsername").then((value) => {
  if(value){
    global.loggedCustomerName = value;
    console.log('LoggedUsername'+value);
    this.setState({userName : value})
   console.log('seccond userName State'+ this.state.userName +'Global'+ global.LoggedUsername);
   
  }else{
//this.setState({userName : global.usersName});
  }
}).done();

//  LocalData.getUserData().then((response) => {
//       response = JSON.parse(response);
//       console.log('LocalData response') 
//       console.log(response);
//       if(response){
//        // this.setState({userName : response.uName})
//       }
       
//     });
    AsyncStorage.getItem("IsApproved").then((value) => {
      if(value){
       global.IsApproved = value;
     //  console.log(global.IsApproved);
      }
   }).done();
  
  
 }
//  async userProfile(){
//   var authDataToken =authData();
//        try
//        { 
//            let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/'+global.CustomerKey, 
//            { 
//                method: 'GET',
//                    headers: 
//                    { 
//                        'Accept': 'application/json', 
//                        'Content-Type': 'application/json', 
//                        'Authorization' : authDataToken,
                       
//                        }
//                }); 
//                let res = await response.text(); 
//                console.log('sdhhhshhhhdhdhdshdshdshdshdhsdh')
//                    console.log(res);
//                    let result= JSON.parse(res); 
//                    var jsonResult =  result.Status[0];
//                    if(jsonResult.errorCode == 1000){
//                            var CustomerInfo = result.CustomerInfo[0];
//                            var FirstName = CustomerInfo.FirstName=='null'?'':CustomerInfo.FirstName;
//                           var LastName =  CustomerInfo.LastName=='null'?'':CustomerInfo.LastName;
//                           var MiddleName= CustomerInfo.MiddleName=='null'?'':CustomerInfo.MiddleName;
//                            global.userName = FirstName+' '+LastName;
//                          //  global.LoggedUsername = FirstName+' '+LastName;
//                         //   console.log(global.userName);
//                         console.log('affafdfdsafafadsfd'+FirstName+' '+LastName);
//                         this.setState({userName : FirstName+' '+LastName});
//                    }else{
//                       let errors = await response;
//                       throw errors;
//                    }
//    }catch (errors) { 
//                     console.log('catch');
//                     console.log(JSON.stringify(errors));
//                   }
// }

  render() {
   
   // const { navigate } = this.props.navigation;
 
    return (
       <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <Container style={{backgroundColor:'#094F66'}}>
     <ScrollView contentContainerStyle={styles.contentContainer}>

     <List>
          
     {!global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',marginTop:30}}>
              <TouchableOpacity  onPress={() => this.props.navigation.navigate("Forth")} underlayColor='#094F66'>
              <Text style={styles.menuText} 
            onPress={() => this.props.navigation.navigate("Forth")}>Login</Text>
             </TouchableOpacity>
            </ListItem>}

            {!global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66'}}>
            <TouchableOpacity underlayColor='#094F66' onPress={() => this.props.navigation.navigate("Seventh")}>
              <Text style={styles.menuText} 
            >Registration</Text>
            </TouchableOpacity>
            </ListItem>}
            {/*global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}}>
              <Text style={styles.menuText}  onPress={() => this.props.navigation.navigate("FeeCalculator")}>Rates</Text>
            </ListItem>*/}
           {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',marginTop:30}}>
                 <View style={{alignItems:'center',flex:1}}>
            
           
       <Text style={{color:'#fff'}}>Welcome, {global.loggedCustomerName} </Text>
       </View>
                
            </ListItem>
  } 
    {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} >
<TouchableOpacity  onPress={() => this.props.navigation.navigate("MyProfile")} underlayColor='#094F66'>
              <Text style={styles.menuText}>My Profile</Text>
            </TouchableOpacity>
            </ListItem>
  }
 
  {global.IsApproved=='0' && 
  <ListItem style={{backgroundColor:'#094F66',}} 
 >
  <TouchableOpacity  onPress={() => this.props.navigation.navigate("Kyc")} underlayColor='#094F66'>
    <Text style={styles.menuText}>Complete KYC verification</Text>
    </TouchableOpacity>
  </ListItem>
  }
  
   <ListItem style={{backgroundColor:'#094F66',}} button
  onPress={() => this.props.navigation.navigate("Kyc")}>
    <Text style={styles.menuText}>Complete KYC verification</Text>
  </ListItem> 
  {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} 
           >
            <TouchableOpacity  onPress={() => this.props.navigation.navigate("FeeCalculator")} underlayColor='#094F66'>
              <Text style={styles.menuText}>Fee Calculator</Text>
              </TouchableOpacity>
            </ListItem>
  }
            {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} 
            >
            <TouchableOpacity onPress={() => this.props.navigation.navigate("BeneficiaryList")} underlayColor='#094F66'>
              <Text style={styles.menuText}>Beneficiary</Text>
              </TouchableOpacity>
            </ListItem>
          }
           {!global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} 
           >
            <TouchableOpacity onPress={() => this.props.navigation.navigate("TransactionHistory")} underlayColor='#094F66'>
              <Text style={styles.menuText}>Transaction Status</Text>
              </TouchableOpacity>
            </ListItem>
           }
            {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}}
            >
            <TouchableOpacity onPress={() => this.props.navigation.navigate("TransactionList")} underlayColor='#094F66'> 
              <Text style={styles.menuText}>Transaction History</Text>
              </TouchableOpacity>
            </ListItem>
            }
            
          {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} >
            <TouchableOpacity onPress={() => this.props.navigation.navigate("UpdatePassword")} underlayColor='#094F66'>
              <Text style={styles.menuText}>Change Password</Text>
              </TouchableOpacity>
            </ListItem>
          }
            {global.CustomerKey && 
            <ListItem style={{backgroundColor:'#094F66',}} 
          >
            <TouchableOpacity  onPress={() => this.props.navigation.navigate("Logout")} underlayColor='#094F66'>
              <Text style={styles.menuText}>Logout</Text>
              </TouchableOpacity>
            </ListItem>
          }
          </List>

          </ScrollView>
      
      </Container>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
    LoginDiv : {
        backgroundColor : '#094F66',
        paddingVertical : 11, 
          borderRadius:5,
          borderWidth: 1,
          borderColor: '#ccc',
      
      },
      extraText : {
        color : '#fff',
        textAlign: 'center',
            fontSize: 18,
      },
      contentContainer: {
        backgroundColor : '#094F66',
      //  paddingVertical : 10,
      },
      menuText : {
        color:'#fff'
    }
})    
//AppRegistry.registerComponent('sideBar', () => sideBar) //Entry Point    and Root Component of The App




