/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,StatusBar,SafeAreaView
} from 'react-native';
import HomeScreen from "./menu";
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build();


export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eee'}}>
      
     <HomeScreen />
     </View>
    );
  }
}

