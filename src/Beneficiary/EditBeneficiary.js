import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,Keyboard,KeyboardAvoidingView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import authData from '../../headerData';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNPickerSelect from 'react-native-picker-select';
export default class Fifth extends Component {
    constructor(props){
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.onCountryChange = this.onCountryChange.bind(this);
    this.typographyRef = this.updateRef.bind(this, 'typography');
    this.countryRef = this.updateCountryRef.bind(this, 'typography');
    global.count = 0;
    this.inputRefs = {};
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      senderCountry : [],
      senderState : [],
      isError : false,
      errorMsg : '',
      firstName : '',
      firstNameError : false,
      countryError : false,
      stateError : false,
      cityError : false,
      mobileError :false,
      country : '',
      stateName : '',
      city : '',
      mobile  : '',
      countryLabel : ' Select country to load state / Provinces',
      stateLabel : ' Select state / Provinces',
      companyError :false,
      lastName : '',
      middleName : '',
      addressLine1 : '',
      addressLine2 : '',
      beneficiaryResult : [],
      zipCode : '',
      emailAddress : '',
      businessName : '',
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
    this.getSenderCountry();
   // this.getBeneficiaryEx();
    this.getBeneficiary();
   if(global.firstName){
    // alert('sfdfdf');
     if(global.isCompany){
    //  global.businessName = this.state.businessName;
    this.setState({businessName : global.businessName})
    this.setState({accountType : global.accountType})
    this.setState({isCompany : true});
     }
    this.setState({firstName : global.firstName})
    this.setState({lastName : global.lastName})
    this.setState({middleName : global.middleName})
    this.setState({addressLine1 : global.addressLine1})
    this.setState({addressLine2 : global.addressLine2})
    this.setState({country : global.country})
    if(global.country){
     // this.getSenderCountry();
      this.getSenderState(global.country);
      console.log('stateName'+global.stateName);
    }
    this.setState({stateName : global.stateName})
    this.setState({zipCode : global.zipCode})
    this.setState({emailAddress : global.emailAddress})
    this.setState({mobile : global.mobile})
    this.setState({city : global.city})
   }

 }
 onChangeText(text) {
    console.log(text);
    this.setState({country:text,countryLabel:''});
    if(text){
        this.getSenderState(text)
    }
    
   }
   onCountryChange(text){
    console.log(text);
    this.setState({stateName:text,stateLabel:''});
       
   }
   updateRef(name, ref) {
     this[name] = ref;
   }
   updateCountryRef(name, ref) {
    this[name] = ref;
  }

 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
   if(value=='Individual'){
    this.setState({isCompany : false});

   }else{
    this.setState({isCompany : true});

   }
 }
 async getBeneficiaryEx(){
  const { params } = this.props.navigation.state;
  let bDetails = params.id;
  var authDataToken =authData();
  global.beneficiaryKey = params.id;
//    console.log('getBeneficiary****'+authDataToken);
this.setState({visible:true});
  try
  { 
      let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/'+bDetails, 
      { 
          method: 'GET',
              headers: 
              { 
                  'Accept': 'application/json', 
                  'Content-Type': 'application/json', 
                  'Authorization' : authDataToken,
                  
                  }
          }); 
          let res = await response.text(); 
          var authDataToken =authData();
       // console.log('getBeneficiary///////// :-'+authDataToken);
              console.log(res);
              let result= JSON.parse(res); 
              var jsonResult =  result.Status[0];
              if(jsonResult.errorCode==1000){
                  var BeneficiaryInfo = result.BeneficiaryInfo[0]; 
                  this.getSenderState(BeneficiaryInfo.CountryKey);
                  this.setState({visible:false});
              }else{
                this.setState({visible:false});
                  let errors = res;
                  throw errors;
              }
             
}catch (errors) { 
  this.setState({visible:false});
             //  console.log(errors);
               console.log(JSON.stringify(errors));
             }
 }
 async getBeneficiary(){
  this.setState({visible:true});
    const { params } = this.props.navigation.state;
    let bDetails = params.id;
    var authDataToken =authData();
    global.beneficiaryKey = params.id;
//    console.log('getBeneficiary****'+authDataToken);
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/'+bDetails, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.text(); 
            var authDataToken =authData();
         // console.log('getBeneficiary///////// :-'+authDataToken);
                console.log(res);
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.errorCode==1000){
                    var BeneficiaryInfo = result.BeneficiaryInfo[0];
                    this.setState({ beneficiaryResult : result.BeneficiaryInfo})
                    console.log(BeneficiaryInfo);
                 
                    this.setState({firstName : BeneficiaryInfo.FirstName });
                    this.setState({middleName : BeneficiaryInfo.MiddleName });
                    this.setState({lastName : BeneficiaryInfo.LastName });
                    this.setState({addressLine1 : BeneficiaryInfo.AddressLine1 });
                    this.setState({addressLine2 : BeneficiaryInfo.AddressLine2 });
                    this.setState({city : BeneficiaryInfo.CityName });
                    this.setState({zipCode : BeneficiaryInfo.ZipCode });
                    this.setState({mobile : BeneficiaryInfo.MobileNo });
                    this.setState({emailAddress : BeneficiaryInfo.EmailAddress});
                    this.setState({country : BeneficiaryInfo.CountryKey})
                    this.setState({stateName : BeneficiaryInfo.StateKey})
                    this.getSenderState(BeneficiaryInfo.CountryKey);
                    console.log('BeneficiaryInfo.StateKey'+BeneficiaryInfo.StateKey);
                    console.log('stateId'+this.state.stateName);
                    this.setState({visible:false});
                }else{
                    let errors = res;
                    throw errors;
                    this.setState({visible:false});
                }
               
  }catch (errors) { 
               //  console.log(errors);
               this.setState({visible:false});
                 console.log(JSON.stringify(errors));
               }
   }
 async getSenderCountry(){
   var authDataToken =authData();
   this.setState({visible:true});
        try
        { 
            let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/countries', 
            { 
                method: 'GET',
                    headers: 
                    { 
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json', 
                        'Authorization' : authDataToken,
                        
                        }
                }); 
                let res = await response.text(); 
                    // console.log(res);
                    let result= JSON.parse(res); 
                    console.log(result);
                    var jsonResult =  result.Status[0];
                    if(jsonResult.errorCode == 1000){
                      this.setState({visible:false});
                            var country = result.CountriesInfo;
                            var res = [];
                            for(var i=0;i<country.length;i++){ 
                                res.push({value : country[i].Key,label :country[i].Value });
                            }  
                            this.setState({senderCountry : res});
                    }else{
                      this.setState({visible:false});
                      let errors = result;
                      throw errors;
                    }
    }catch (errors) { 

      this.setState({visible:false});
                     console.log('catch');
                     console.log(JSON.stringify(errors))
                   }
 }
 /*async getSenderState(key){
     console.log(key);
    var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/lists/states/'+key, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
                      console.log(res);
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                             var country = result.StatesInfo;
                             var res = [];
                             for(var i=0;i<country.length;i++){ 
                                 res.push({value : country[i].Key,label :country[i].Value });
                             }  
                             this.setState({senderState : res});
                     }else{
                      let errors = result;
                      throw errors;
                     }
     }catch (errors) { 
                    //  console.log('catch');
                    console.log(JSON.stringify(errors))
                    }
 }*/

  getSenderState(key){
    var authDataToken =authData();
         this.setState({visible:true});
             fetch('http://api.remitanywhere.com/restapi/lists/states/'+key, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }).then((response)=>response.json()).then((res)=>{
                   console.log(res)
                   let result= res;
                   var jsonResult =  result.Status[0];
                   if(jsonResult.errorCode == 1000){
                    var country = result.StatesInfo;
                    var res = [];
                    for(var i=0;i<country.length;i++){ 
                        res.push({value : country[i].Key,label :country[i].Value });
                    }  
                    this.setState({senderState : res});
                  }
                  this.setState({visible:false});
                 }).catch((error)=>{
                   console.error(error);
                 });
                
    
  }

 
  
 onSubmit(){
  global.firstName = this.state.firstName;
  global.lastName = this.state.lastName;
  global.middleName = this.state.middleName;
  global.addressLine1 = this.state.addressLine1;
  global.addressLine2 = this.state.addressLine2;
  global.country = this.state.country;
  global.stateName = this.state.stateName;
  global.city = this.state.city;
  global.zipCode = this.state.zipCode;
  global.emailAddress = this.state.emailAddress;
  global.mobile = this.state.mobile;
  if(this.state.accountType=='Individual'){
    global.isCompany = false;
    global.businessName = "";
    global.businessLicenseNumber = "";
   
  }else{
    global.isCompany = true;
    global.businessName = this.state.businessName;
    global.businessLicenseNumber = this.state.businessLicenseNumber;
  }
  global.accountType = this.state.accountType;
 // global.countryKey = this.state.countryKey;
 if(global.firstName==''){
   console.log('if');
this.setState({firstNameError:true,isError:true,errorMsg:'First name must be required !'});
 }else if(global.country==''){
  this.setState({countryError:true,isError:true,errorMsg:'Country name must be required !',firstNameError:false});

 }else if(global.stateName==''){
  this.setState({stateError:true,isError:true,errorMsg:'State name must be required !',countryError:false});
   
 }else if(global.city==''){
  this.setState({cityError:true,isError:true,errorMsg:'City name must be required !',stateError:false});
   
}else if(global.mobile==''){
  this.setState({mobileError:true,isError:true,errorMsg:'Mobile No. must be required !',cityError:false});

}else if( global.accountType=='Company' && global.businessName ==''){
  
  this.setState({mobileError:true,isError:true,errorMsg:'Company Name must be required !',cityError:false});
 
  
}else{
  this.setState({mobileError:false,isError:false,errorMsg:'',stateError:false});
  
  console.log('else');
              const { navigate } = this.props.navigation;
                    navigate('EditBankDetails');
                //   this.updateBeneficary();
}
                    }
                    async updateBeneficary(){
                      var authDataToken =authData();
                      const { params } = this.props.navigation.state;
                      let bDetails = params.id;
                      //http://api.remitanywhere.com/restapi/beneficiary/profile/update
                      try
                      { 
                          let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/update', 
                          { 
                              method: 'POST',
                                  headers: 
                                  { 
                                      'Accept': 'application/json', 
                                      'Content-Type': 'application/json', 
                                      'Authorization' : authDataToken,
                                      
                                      },
                                      body :JSON.stringify({
                                        "beneficiaryKey":bDetails
                                        ,"customerKey":global.CustomerKey
                                        ,"firstName":global.firstName
                                        ,"middleName":global.middleName
                                        ,"lastName":global.lastName
                                        ,"businessName":null
                                        ,"addressLine1":global.addressLine1
                                        ,"addressLine2":global.addressLine2
                                        ,"countryKey":global.country
                                        ,"stateKey":global.stateName
                                        ,"cityName":global.city
                                        ,"zipCode":global.zipCode
                                        ,"emailAddress":global.emailAddress
                                        ,"mobileNo":global.mobile
                                      
                                      })
                              }); 
                              let res = await response.text(); 
                                   console.log(res);
                                  let result= JSON.parse(res); 
                                  var jsonResult =  result.Status[0];
                                  if(jsonResult.errorCode == 1000){
                                          
                                  }else{
                                   let errors = res;
                                   throw errors;
                                  }
                  }catch (errors) { 
                                 //  console.log('catch');
                                 console.log(JSON.stringify(errors))
                                 }
                    }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
   return (
     <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
         <KeyboardAvoidingView
         style={styles.container}
         behavior="padding" >
      
       <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('BeneficiaryList',{id:params.id})}> <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Update Beneficiary</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('BeneficiaryList',{id:params.id})}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Beneficiaries Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never">
       {this.state.isError==true && <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>{this.state.errorMsg}</Text>}

       <View style={{flexDirection:'row',backgroundColor:'transparent',alignItems:'center',paddingLeft:25,marginTop:15,paddingRight:30}}>
             <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,paddingLeft:20,borderTopLeftRadius:3,borderBottomLeftRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' outerCircleColor ='#F06124' value={'Individual'} onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Individual</Text>
                 </RadioButton></View>
                 <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,borderTopRightRadius:3,borderBottomRightRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' value={'Company'} outerCircleColor ='#F06124' onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Company</Text>
                 </RadioButton></View>
       </View>
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="First Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onSubmitEditing={()=>this.middleNameInput.focus()}
      onChangeText={(text) => this.setState({firstName:text})}
      value={this.state.firstName}
      autoFocus={true}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         
         <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Middle Name" style={[styles.InputText,this.state.mName&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.middleNameInput=input}
      onSubmitEditing={()=>this.lastNameInput.focus()}
      onChangeText={(text) => this.setState({middleName:text})}
      value={this.state.middleName}
       ></TextInput>
       </View>
         </View>
         <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Last Name" style={[styles.InputText,this.state.lNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.lastNameInput=input}
       onSubmitEditing={()=>this.address1Input.focus()}
      onChangeText={(text) => this.setState({lastName:text})}
      value={this.state.lastName}
       ></TextInput>
        </View>
        
         </View>

           {this.state.isCompany&& <View style={styles.inputDiv}>
           <View style={{flex:1}}>
         <TextInput
       placeholder="Business Name" style={[styles.InputText,this.state.btNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({businessName:text})}
      value={this.state.businessName}
       
       ></TextInput>
        </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
       }
             {this.state.isCompany&& <View style={styles.inputDivSecond}>
             <View style={{flex:1}}>
         <TextInput
       placeholder="License No" style={[styles.InputText,this.state.ddNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
         onChangeText={(text) => this.setState({businessLicenseNumber:text})}
      value={this.state.businessLicenseNumber}
         
       ></TextInput>
         </View>
         </View>
       }
        <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Address Line 1" style={[styles.InputText,this.state.fdirstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.address1Input=input}
      onSubmitEditing={()=>this.address2Input.focus()}
       onChangeText={(text) => this.setState({addressLine1:text})}
      value={this.state.addressLine1}
       
       ></TextInput>
         </View>
         </View>
         <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Address Line 2" style={[styles.InputText,this.state.firstNamddeError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({addressLine2:text})}
       ref={(input)=>this.address2Input=input}
       value={this.state.addressLine2}
      
       ></TextInput>
         </View>
         </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         
       {/*}  <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label={this.state.countryLabel}
              data={this.state.senderCountry}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              baseColor = {'#484848'}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={[{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5},this.state.countryError&&styles.errorInput]}
             
            />  */}
             <RNPickerSelect
            placeholder={{
              label: 'Select country to load state / Provinces',
              value: '',
            }}
          items={this.state.senderCountry}
          onValueChange={
          (item) => {
            this.setState({
              country: item,
              
            });
            console.log(item);
           this.onChangeText(item)
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.country}
          
         
        />
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
        {/* <Dropdown
              ref={this.countryRef}
              onChangeText={this.onCountryChange}
              label={this.state.stateLabel}
              data={this.state.senderState}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}  : stateName : StateKey
              fontSize={14}
              inputContainerStyle={[{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5},this.state.stateError&&styles.errorInput]}
              baseColor = {'#484848'}
            /> */}
            <RNPickerSelect
            placeholder={{
              label: 'Select state / Provinces',
              value: '',
            }}
          items={this.state.senderState}
          onValueChange={
          (item) => {
            this.setState({
              stateName: item,
               });
             this.onCountryChange(item)
           }
        }
          style={{ ...pickerSelectStyles }}
          value={this.state.stateName}
        />
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="City" style={[styles.InputText,this.state.cityError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({city:text})}
       value={this.state.city}
       onSubmitEditing={()=>this.zipCodeInput.focus()}
       ></TextInput>
        </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Zip / Postal Code" style={[styles.InputText,this.state.firsftNamfeError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({zipCode:text})}
       ref={(input)=>this.zipCodeInput=input}
       onSubmitEditing={()=>this.mobileInput.focus()}
       value={this.state.zipCode}
       
       ></TextInput>
         </View>
         </View>
          <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Mobile #" style={[styles.InputText,this.state.fifffrstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="numeric"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({mobile:text})}
       ref={(input)=>this.mobileInput=input}
       onSubmitEditing={()=>this.emailInput.focus()}
       value={this.state.mobile}
       
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDivSecond}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Email Address" style={[styles.InputText,this.state.firstNadfdmeError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="done" 
       placeholderTextColor='#e1e1e1'
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({emailAddress:text})}
       ref={(input)=>this.emailInput=input}
       onSubmitEditing={Keyboard.dismiss}
       value={this.state.emailAddress}
       
       ></TextInput>
        </View>
       
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TouchableHighlight style={styles.next} onPress={(e)=>{this.onSubmit()}}>
         <Text style={{textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,}}>Continue </Text>
         </TouchableHighlight>
         </View>
       </View>
</ScrollView>

       </View>

     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 20,
  
},
continue : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 10,
  paddingHorizontal:10,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff',
  
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},

errorInput : {
 
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
  fontSize:16
},stickDiv : {
  color:'red',fontSize:16,paddingLeft:5
},
inputDiv : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:20},
inputDivSecond : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:30}
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    backgroundColor: 'white',
    color:'#484848'
  },
   icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: 'gray',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  }
});