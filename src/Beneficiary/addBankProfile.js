import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,
  KeyboardAvoidingView,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import RadioButton from 'radio-button-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import authData from '../../headerData';
import RNPickerSelect from 'react-native-picker-select';
export default class Fifth extends Component {
    constructor(props){
    super(props);
    this.onChangeDeliveryCurrency = this.onChangeDeliveryCurrency.bind(this);
    this.typographyRef = this.updateRef.bind(this,'typography');
    this.onChangeDeliveryMethode = this.onChangeDeliveryMethode.bind(this);
   
    this.MethodRef = this.updateMethodRef.bind(this,'typography');
    this.onBankChange = this.onBankChange.bind(this);
    this.bankRef = this.bankRef.bind(this,'typography');
    
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      delievryCurrency : [],
      deliveryMethod : [],
      bankName : [],
      branchName : [],
      bankDetailShow : false,
      delievryCurrencyLabel : ' Delivery Currency',
      deliveryMethodLabel : ' Delivery Method',
     bankNameLabel : ' Bank Name',
     branchNameLabel : ' ',
     delievryCurrencySelect : '',
     deliveryMethodSelect : '',
     bankKeySelect : '',
     branchNumber : '',
     accountNumber:'',
     bicCode:'',
     pinCode : '',
     isErrorMsgShow : false,
     isErrorMsg : '',
     bankNameValue:''
    } 
  }
  componentDidMount() {
   
    this.getPayeeCurrency();
    //this.getbankName();
    this.getDeliveryMethodeFun();
 }
 onChangeDeliveryCurrency(text) {
    console.log(text);
    this.setState({delievryCurrencySelect:text,delievryCurrencyLabel : ''});
  //  this.getSenderState(text)
   }
   onChangeDeliveryMethode(text){
    console.log(text);
    this.setState({deliveryMethodSelect:text,deliveryMethodLabel : ''});
    this.getbankName();
   }
   updateRef(name, ref) {
     this[name] = ref;
   }
   updateMethodRef(name, ref) {
    this[name] = ref;
  }
  onBankChange(text){
    console.log(text);
    this.setState({bankNameValue : text});
    var a = text.split('-');
    this.setState({bankKeySelect:a[0]});
    this.setState({bankNameLabel:''});
    this.setState({bankNameSelect:a[1]});
    console.log(this.state.bankKeySelect,this.state.bankNameSelect);
    this.setState({bankDetailShow : true})
       this.getbranchName();
   }
   bankRef(name, ref) {
    this[name] = ref;
  }
 async getPayeeCurrency(){
   console.log(global.country);
   this.setState({visible:true});
   var authDataToken =authData();
        try
        { 
            let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/currencies/'+global.country, 
            { 
                method: 'GET',
                    headers: 
                    { 
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json', 
                        'Authorization' : authDataToken,
                        
                        }
                }); 
                let res = await response.text(); 
                    // console.log(res);
                    let result= JSON.parse(res); 
                    var jsonResult =  result.Status[0];
                    if(jsonResult.errorCode == 1000){
                      this.setState({visible:false});
                            var country = result.CurrenciesInfo;
                            var res = [];
                            for(var i=0;i<country.length;i++){ 
                                res.push({value : country[i].Key,label :country[i].Value });
                            }  
                            this.setState({delievryCurrency : res});
                    }else{
                      this.setState({visible:false});
                      
                      let error = res;
                    throw error;
                    }
                    
    }catch (error) { 
      this.setState({visible:false});
      
                     //console.log('catch');
                     console.log(JSON.stringify(error));

       global.setTimeout(() => {
        Alert.alert(
          'Warning',
        'Something Went wrong! Contact to support team',
          [
          {text : 'OK', onPress:()=>{this.goBaneficiary()}}
          ]
         );
      }, 50);
                     
                   }
 }
 async getDeliveryMethodeFun(){
     console.log('global.country');
    var authDataToken =authData();
    this.setState({visible:true});
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/lists/deliverymethods/'+global.country, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
                      console.log(res);
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                      this.setState({visible:false});
                             var country = result.DeliveryMethodsInfo;
                             var res = [];
                             for(var i=0;i<country.length;i++){ 
                                 res.push({value : country[i].Key,label :country[i].Value });
                             }  
                             this.setState({deliveryMethod : res});
                     }else{
                      this.setState({visible:false});
                      let error = res;
                      throw error;
                     }
     }catch (error) { 
      this.setState({visible:false});
                      console.log('catch');
                      console.log(JSON.stringify(res));
                    }
  }
  async getbankName(){
    console.log('key');
   var authDataToken =authData();
        try
        { 
            let response = await fetch('http://api.remitanywhere.com/restapi/lists/beneficiary/banks', 
            { 
                method: 'POST',
                    headers: 
                    { 
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json', 
                        'Authorization' : authDataToken,
                        
                        },
                        body: JSON.stringify({
                            "countryKey":global.country
                            ,"deliveryMethodKey":this.state.deliveryMethodSelect
                            ,"currency":this.state.delievryCurrencySelect
                        
            
                           })
                }); 
                let res = await response.text(); 
                     console.log(res);
                    let result= JSON.parse(res); 
                    var jsonResult =  result.Status[0];
                    if(jsonResult.errorCode == 1000){
                            var country = result.BanksInfo;
                            var res = [];
                            for(var i=0;i<country.length;i++){ 
                              
                                res.push({value : country[i].Key+'-'+country[i].Value,label :country[i].Value });
                            }  
                            this.setState({bankName : res});
                    }else{
                      let error = res;
                      throw error;
                    }
    }catch (error) { 
                     console.log('catch');
                     console.log(JSON.stringify(error));
                   }
 }
 async getbranchName(){
  console.log('key'+'Bkey'+this.state.bankKeySelect+'crr'+this.state.delievryCurrencySelect);
  console.log(typeof(Number(this.state.bankKeySelect)));
  console.log(typeof(this.state.delievryCurrencySelect));
  
 var authDataToken =authData();
      try
      { 
          let response = await fetch('http://api.remitanywhere.com/restapi/lists/beneficiary/branches', 
          { 
              method: 'POST',
                  headers: 
                  { 
                      'Accept': 'application/json', 
                      'Content-Type': 'application/json', 
                      'Authorization' : authDataToken,
                      
                      },
                      body: JSON.stringify({
                        "bankKey":14
                        ,"currency":"EUR"
                     
                      
          
                         })
              }); 
              let res = await response.text(); 
                   console.log(res);
                  let result= JSON.parse(res); 
                  var jsonResult =  result.Status[0];
                  if(jsonResult.errorCode == 1000){
                          var country = result.BranchesInfo;
                          var res = [];
                          for(var i=0;i<country.length;i++){ 
                            
                              res.push({value : country[i].Key+'-'+country[i].Value,label :country[i].Value });
                          }  
                          this.setState({branchName : res});
                  }else{
                    let error = res;
                    throw error;
                  }
  }catch (error) { 
                   console.log('catch');
                   console.log(JSON.stringify(error));
                 }
}
onSubmitValidate(){
  var delievryCurrencySelect = this.state.delievryCurrencySelect;
  var deliveryMethodKey = Number(this.state.deliveryMethodSelect);
  var bankName = this.state.bankKeySelect;
  var branchNumber = this.state.branchNumber;
  var accountNumber = this.state.accountNumber;
  var bicCode = this.state.bicCode;
  var pinCode = this.state.pinCode;
  if(delievryCurrencySelect==''){
    console.log('delievryCurrencySelect Error');
    this.setState({isErrorMsgShow : true,isErrorMsg:'Please Select Delivery Currency!'})
  }else if(deliveryMethodKey==''){
   console.log('deliveryMethodKey Error');
   this.setState({isErrorMsgShow : true,isErrorMsg:'Please Select Delivery Method!'})
   
  }else if(bankName==''){
    console.log('bankName Error');
    this.setState({isErrorMsgShow : true,isErrorMsg:'Please Select Bank Name!'})
    
  }else if(branchNumber==''){
console.log('branchNumber Error')
this.setState({isErrorMsgShow : true,isErrorMsg:'Please Enter Branch number!'})

  }else if(accountNumber==''){
console.log('accountNumber Error');
this.setState({isErrorMsgShow : true,isErrorMsg:'Please Enter account Number!'})

  }else if(bicCode==''){
console.log('bicCode Error');
this.setState({isErrorMsgShow : true,isErrorMsg:'Please Enter BIC/Swift Code!'})

  }else if(pinCode==''){
    console.log('pinCode Error');
this.setState({isErrorMsgShow : true,isErrorMsg:'Please Enter Security Code!'})
    
  }else{
    console.log('fine');
this.setState({isErrorMsgShow : false,isErrorMsg:''})
    this.onSubmitFun();
  }
  console.log('click');
}

 async onSubmitFun(){
     this.setState({visible:true});
    var authDataToken =authData();
  var deliveryMethodKey = Number(this.state.deliveryMethodSelect);
  var delievryCurrencySelect = this.state.delievryCurrencySelect;
  var accountNumber= this.state.accountNumber;
  var bankName = this.state.bankKeySelect;
  var branchNumber = this.state.branchNumber;
  var bicCode = this.state.bicCode;
  var pinCode = this.state.pinCode;
  var branchName = this.state.branchName;
  var branchCity = this.state.branchCity;
  var branchAddress = this.state.branchAddress;
  if(accountNumber){
console.log(accountNumber);
  }else{
    accountNumber = "99999999";
  }
 // bankName =bankName.length<0 ? bankName : "HABIB BANK LTD";
 // bicCode =bicCode.length<0 ? bicCode : "HBLSCT";
  //branchName =branchName.length<0 ? branchName : "ASKARI BRANCH, LAHORE.";
  //branchNumber =branchNumber.length<0 ? branchNumber : "39327248";
  //branchCity =branchCity.length<0 ? branchCity : "Lahore";
 // branchAddress = branchAddress.length<0 ? branchAddress : "NEW AIRPORT ROAD, LAHORE CANTT.";
  bankName = Number(bankName);
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/insert', 
        { 
            method: 'POST',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    },
                    body: JSON.stringify({
                        "customerKey":global.CustomerKey
                        ,"firstName":global.firstName
                        ,"middleName":global.middleName
                        ,"lastName":global.lastName
                        ,"businessName":global.businessName
                        ,"addressLine1": global.addressLine1
                        ,"addressLine2":global.addressLine2
                        ,"countryKey":global.country
                        ,"stateKey":global.stateName
                        ,"stateName":"PUNJAB"
                        ,"cityName":global.city
                        ,"zipCode":global.zipCode
                        ,"emailAddress":global.emailAddress
                        ,"mobileNo":global.mobile
                        ,"genderKey":null
                        ,"dateOfBirth":null
                        ,"idTypeKey":null
                        ,"idType":null
                        ,"beneficiaryID":null
                        ,"securityPinNumber":null
                        ,"accountNo":accountNumber
                        ,"accountTypeKey":null
                        ,"beneficiaryBankKey":bankName
                        ,"beneficiaryBankName":bankName
                        ,"beneficiaryBankSwiftCode":bicCode
                        ,"routingNumber":null
                        ,"beneficiaryBranchKey":null
                        ,"beneficiaryBranchName":branchName 
                        ,"beneficiaryBranchNumber":branchNumber 
                        ,"beneficiaryBranchCity":branchCity 
                        ,"beneficiaryBranchAddress":branchAddress 
                        ,"careOfPersonName":null
                        ,"careOfPersonRelationshipKey":null
                        ,"altBeneficiary1":null
                        ,"altBeneficiary2":null
                        ,"altBeneficiary3":null
                        ,"altBeneficiary4":null
                        ,"deliveryMethodKey":3
                        ,"deliveryMethodCurrency":delievryCurrencySelect
                        ,"defaultPickupNetworkKey":null
                        ,"defaultPickupLocationKey":null
                    }
                    )
            }); 
            let res = await response.text(); 
               
     this.setState({visible:false});
     console.log(res);
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.errorCode == 1000){
                       console.log(jsonResult.errorMessage);
                      
                      /* Alert.alert(
                        'Success',
                        'Beneficiary added successfully',
                        [
                        {text : 'OK', onPress:()=>{this.goBaneficiary()}}
                        ]
                       );*/
                       global.setTimeout(() => {
                        Alert.alert(
                          'Success',
                          'Beneficiary added successfully',
                          [
                          {text : 'OK', onPress:()=>{this.goBaneficiary()}}
                          ]
                         );
                      }, 200);
                      // alert('Beneficiary added successfully');
                }else if(jsonResult.errorCode == 1001){
                  this.setState({visible:false});
                  console.log(typeof(deliveryMethodKey));
                  console.log(typeof( global.country));
                  console.log(typeof( global.stateName));
                
                   /* Alert.alert(
                        'Warning',
                        'Failed to insert beneficiary profile, please contact support team.'
                    );*/
                    global.setTimeout(() => {
                      Alert.alert(
                        'Warning',
                        'Failed to insert beneficiary profile, please contact support team.',
                        [
                        {text : 'OK', onPress:()=>{this.goBaneficiary()}}
                        ]
                       );
                    }, 200);
                    console.log('fail');
                }else{
                  let error = res;
                     throw error;
                }
}catch (error) { 
    this.setState({visible:false});
                 console.log('catch');
                 console.log(error);
               }
 }
 goBaneficiary(){
    this.setState({visible:false});
    const { navigate } = this.props.navigation;
    navigate('BeneficiaryList');
 }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
    <KeyboardAvoidingView
         style={styles.container}
         behavior="padding" >
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('AddBeneficiary')}><FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Delivery Details</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}}>
               <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome>
               </Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Beneficiaries Bank Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never">
       
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       {this.state.isErrorMsgShow && <Text style={{paddingHorizontal:25,color:'red',paddingVertical:10}}>{this.state.isErrorMsg}</Text>}

       <View style={styles.inputDiv}>
         
        {/* <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeDeliveryCurrency}
              label={this.state.delievryCurrencyLabel}
              data={this.state.delievryCurrency}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              baseColor = {'#484848'}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}
             
            /> */}
            <View style={{flex:1}}>
             <RNPickerSelect
            placeholder={{
              label: 'Delivery Currency',
              value: '',
            }}
          items={this.state.delievryCurrency}
          onValueChange={
          (item) => {
            console.log(item);
           this.onChangeDeliveryCurrency(item)
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.delievryCurrencySelect}
         
        />
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         
         <View style={styles.inputDiv}>
        {/*} <Dropdown
              ref={this.MethodRef}
              onChangeText={this.onChangeDeliveryMethode}
              label={this.state.deliveryMethodLabel}
              data={this.state.deliveryMethod}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}
              baseColor = {'#484848'}
            /> */}
            <View style={{flex:1}}>
           <RNPickerSelect
            placeholder={{
              label: 'Delivery Method',
              value: '',
            }}
          items={this.state.deliveryMethod}
          onValueChange={
          (item) => {
            console.log(item);
           this.onChangeDeliveryMethode(item)
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.deliveryMethodSelect}
         
        />
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDiv}>
       {/*  <Dropdown
              ref={this.bankRef}
              onChangeText={this.onBankChange}
              label={this.state.bankNameLabel}
              data={this.state.bankName}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}
              baseColor = {'#484848'}
            /> */}
            <View style={{flex:1}}>
             <RNPickerSelect
            placeholder={{
              label: 'Bank Name',
              value: '',
            }}
          items={this.state.bankName}
          onValueChange={
          (item) => {
            console.log(item);
           this.onBankChange(item)
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.bankNameValue}
         
        />
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         
           {this.state.bankDetailShow&& <View>
            <View style={styles.inputDiv}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Branch Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({branchName:text})}
       onSubmitEditing={()=>this.branchAddressInput.focus()}
       ></TextInput>
        
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDivSecond}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Branch Address" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({branchAddress:text})}
       ref={(input)=>this.branchAddressInput=input}
       onSubmitEditing={()=>this.branchCityInput.focus()}
       ></TextInput>
         </View>
         
         </View>
         <View style={styles.inputDivSecond}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Branch City" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({branchCity:text})}
       ref={(input)=>this.branchCityInput=input}
       onSubmitEditing={()=>this.branchNumberInput.focus()}
       ></TextInput>
        </View>
      
         </View>
         <View style={styles.inputDiv}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Branch Number" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#484848'
      keyboardType={ "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({branchNumber:text})}
       ref={(input)=>this.branchNumberInput=input}
       onSubmitEditing={()=>this.accountNumberInput.focus()}
       ></TextInput>
       </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
       
         <View style={styles.inputDiv}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Account Number" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#484848'
      keyboardType={ "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({accountNumber:text})}
       ref={(input)=>this.accountNumberInput=input}
       onSubmitEditing={()=>this.bicNumberInput.focus()}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>

         <View style={styles.inputDiv}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="BIC/Swift Code" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({bicCode:text})}
       ref={(input)=>this.bicNumberInput=input}
       onSubmitEditing={()=>this.securityNumberInput.focus()}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         
         </View>}
         <View style={styles.inputDiv}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Security Pin" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent"  
       placeholderTextColor='#484848'
       returnKeyType={ "done" }
       keyboardType = { "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({pinCode:text})}
       ref={(input)=>this.securityNumberInput=input}
       maxLength={4}
       ></TextInput>
        </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmitValidate()}}>Continue </Text>
         </View> */}
         <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
       
         <Text style={styles.cancelBtn} onPress={()=>navigate('FeeCalculator')}>Cancel </Text>
         <Text style={styles.next} onPress={(e)=>{this.onSubmitValidate()}}>Continue </Text>
         </View>
       </View>
</ScrollView>

       </View>
     </KeyboardAvoidingView>
      </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {
  flex:0.5,
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 20,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,
},cancelBtn : {
  flex:0.5,
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 20,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,
  marginHorizontal:4
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},stickDiv : {
  color:'red',fontSize:16,paddingLeft:5
},
inputDiv : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:20},
inputDivSecond : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:30}
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    backgroundColor: 'white',
    color:'#484848'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: 'gray',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  }
});