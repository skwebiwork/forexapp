import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,Keyboard,KeyboardAvoidingView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import authData from '../../headerData';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import CheckBox from 'react-native-check-box'
import Privacy from '../Privacy';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
export default class ReviewPage extends Component {
    constructor(props){
    super(props);
  
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      senderCountry : [],
      senderState : [{value: "other",label : "Other"},{value: "spot",label : "Spot"},{value: "today",label : "Today"},{value: "tommorrow",label : "Tomorrow"}],
      isErrorMsgs : '',
      errorMsg : '',
      firstName : '',
      firstNameError : false,
      countryError : false,
      stateError : false,
      cityError : false,
      country : '',
      stateName : '',
      isTerms : false,
      city : '',
      countryLabel : ' Select Transaction purpose',
      stateLabel : ' Select Source of Income',
      processToday : '',
      beneficiariesFullName : 'N/A',
      beneficiariesEmail : 'N/A',
      beneficiariesCurrency : 'N/A',
      beneficiariesPayment : 'N/A',
      beneficiaryResult:[],
      isCheckeds : false,
      chkError : false,
      AccountsInfo : '',
      isLoading : false,
      newRes : {},
      depositoryAccountKey1 : null
      
    } 
  }
  componentDidMount() {
    const { params } = this.props.navigation.state;
   console.log(params.amountJson);
if(params.amountJson){
    var amountJson = params.amountJson
    AsyncStorage.getItem("customerCountryKey").then((value) => {
        if(value){
         global.CountryKey = value;
         console.log(value);
        
         this.getBankInfo(value,amountJson.senderCountry);
        }
      }).done();
 // var tamt = Number(amountJson.beneficiaryAmount) + Number(amountJson.exchangeFee);
    this.setState({beneficiaryAmount : amountJson.beneficiaryAmount});
    this.setState({exchageRate : amountJson.exchageRate});
    this.setState({exchangeFee : amountJson.exchangeFee});
    this.setState({receiveCountry : amountJson.receiveCountry});
    this.setState({senderCountryRate : amountJson.senderCountry})
    this.setState({sendingAmount : amountJson.sendingAmount});
    var amountDue = Number(amountJson.exchangeFee)+Number(amountJson.sendingAmount)+Number(amountJson.HandlingFeeAmount)-Number(amountJson.discountValue);
     //  console.log('amountDue'+amountDue);
    this.setState({amountDue : amountDue});
    this.setState({HandlingFeeAmount : amountJson.HandlingFeeAmount});
    this.setState({discountValue : amountJson.discountValue});
    

}
  if(params.id){
    //  console.log(params.id);
    var beneficiariesDetails = params.id;
    this.setState({beneficiariesFullName :beneficiariesDetails.BeneficiaryFullName })
    this.setState({beneficiariesEmail :beneficiariesDetails.EmailAddress})
    this.setState({beneficiariesCurrency :beneficiariesDetails.DeliveryMethodCurrency})
    this.setState({beneficiariesPayment :beneficiariesDetails.DeliveryMethodName })
  }
   // StatusBar.setHidden(true);
   
    this.getBeneficiary();
    //this.getBeneficiaryBranch();
    var dt = Date();
    var dtArr = dt.split(' ');
    var ndt = dtArr[1]+' '+dtArr[2]+', '+dtArr[3];
    this.setState({currentDt : ndt});

 }
 async getBankInfo(country,sendingCurrency){
  console.log(country,sendingCurrency);
    var authDataToken =authData();
    this.setState({isLoading:true});
//    console.log('getBeneficiary****'+authDataToken);
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/location/accounts?countryKey='+country+'&currency='+sendingCurrency, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.text(); 
            var authDataToken =authData();
         console.log('getBeneficiary///////// :-'+authDataToken);
                console.log(res);
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.errorCode==1000){
                    var r = result.AccountsInfo[0];
                    this.setState({AccountsInfo:r.FQN })
                    this.setState({depositoryAccountKey1:r.Key })
                    console.log(r.FQN);
                    console.log(r);
                      console.log(this.state.AccountsInfo);
                      this.setState({isLoading:false});
                    
                }else{
                    let errors = res;
                    throw errors;
                }
    this.setState({isLoading:false});
               
  }catch (errors) { 
    this.setState({isLoading:false});
                 console.log(errors);
                 console.log(JSON.stringify(errors));
               }
 
 }
 onSubmit(){
     console.log('isChecked',this.state.isCheckeds);
     console.log('chkError',this.state.chkError);
   //  this.setState({ chkError: true })
    //  if(this.state.isChecked==false){
        
    // this.setState({ chkError: true })
    //     console.log('false',this.state.chkError);
        
    //  }else{
    //     this.setState({ chkError: false })
         
    //     const { navigate } = this.props.navigation;
    // this.compleTransaction();
     
    //  }

    if(this.state.isCheckeds){
        this.setState({chkError: this.state.isCheckeds})
        const { navigate } = this.props.navigation;
        this.callCheck('true');
        this.compleTransaction();

         }else{
            // alert('please select the terms')
            // this.callCheck('false');
         this.setState({chkError : !this.state.isCheckeds});
         console.log('chkError',this.state.chkError);
         }
    
 }
 callCheck(status){
    this.setState({chkError : status});
 }
    onClick(chk){
        console.log(chk);
        this.setState({isCheckeds:chk});
        console.log(this.state.isCheckeds);
    }
 async getBeneficiary(){
    const { params } = this.props.navigation.state;
    let bDetails = params.id;
    var authDataToken =authData();
  this.setState({isLoading:true});
//    console.log('getBeneficiary****'+authDataToken);
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/'+bDetails.BeneficiaryKey, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.text(); 
            this.setState({isLoading:false});
            var authDataToken =authData();
         // console.log('getBeneficiary///////// :-'+authDataToken);
                console.log(res);
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.errorCode==1000){
                    this.setState({ beneficiaryResult : result.BeneficiaryInfo})
                    this.setState({newRes : result.BeneficiaryInfo[0]})
                    console.log(result.BeneficiaryInfo);
                }else{
                    let errors = res;
                    throw errors;
                }
               
  }catch (errors) { 
               //  console.log(errors);
                 console.log(JSON.stringify(errors));
               }
   }


   addDays(dateObj, numDays) {
    dateObj.setDate(dateObj.getDate() + numDays);
    return dateObj;
  }
                    async compleTransaction(){
                        console.log(global.CustomerApprovalStatus);
                       if(global.CustomerApprovalStatus=='KYC-Approved (Verified)'){

                     //  this.setState({visible:true});
                    var authDataToken =authData();
                    var beneficiaryResult = this.state.beneficiaryResult;
                    console.log(beneficiaryResult);
                    var beneficiaryAmount = this.state.beneficiaryAmount;
                    var nextWeek = this.addDays(new Date(), 1);
      let n = nextWeek.getDate();
      n = n < 10 ? "0" + n : n;
      let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
      let datass = mm+'/'+n+'/'+nextWeek.getFullYear();
      var remitPurpose = global.transactionPurpose;
      var sourceOfIncome = global.IncomeSorce;
                    try
                    { 
                        let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/transaction/insert', 
                        { 
                            method: 'POST',
                                headers: 
                                { 
                                    'Accept': 'application/json', 
                                    'Content-Type': 'application/json', 
                                    'Authorization' : authDataToken,
                                    
                                    },
                                    body : JSON.stringify({
                                       "customerKey":beneficiaryResult[0].CustomerKey
                                        ,"beneficiaryKey":beneficiaryResult[0].BeneficiaryKey
                                        ,"deliveryMethodKey":beneficiaryResult[0].DeliveryMethodKey
                                        ,"paymentMethodKey1":beneficiaryResult[0].DeliveryMethodKey
                                        ,"paymentMethodFee1":'0.0'
                                        ,"payingCurrency1":this.state.senderCountryRate
                                        ,"payingAmount1":this.state.amountDue
                                        ,"SCAmount":this.state.exchangeFee
                                        ,"exRateAmount":this.state.exchageRate
                                        ,"fxCurrency":this.state.receiveCountry
                                        ,"fxAmount":this.state.beneficiaryAmount
                                        ,"remittanceDate":datass
                                        ,"sendingAmount":this.state.sendingAmount
                                        ,"sendEmailToCustomer" : 1
                                        ,"sourceOfIncome" :sourceOfIncome,
                                        "remitPurpose" :remitPurpose,
                                        "depositoryAccountKey1" : this.state.depositoryAccountKey1

                                    })
                            }); 
                            let res = await response.text(); 
                            var jsonResponse = {
                                "customerKey":beneficiaryResult[0].CustomerKey
                                        ,"beneficiaryKey":beneficiaryResult[0].BeneficiaryKey
                                        ,"deliveryMethodKey":beneficiaryResult[0].DeliveryMethodKey
                                        ,"paymentMethodKey1":beneficiaryResult[0].DeliveryMethodKey
                                        ,"paymentMethodFee1":this.state.exchangeFee
                                        ,"payingCurrency1":this.state.senderCountryRate
                                        ,"payingAmount1":this.state.amountDue
                                        ,"SCAmount":this.state.exchangeFee
                                        ,"exRateAmount":this.state.exchageRate
                                        ,"fxCurrency":this.state.receiveCountry
                                        ,"fxAmount":this.state.beneficiaryAmount
                                        ,"remittanceDate":datass
                                        ,"sendingAmount":this.state.sendingAmount
                                        ,"sendEmailToCustomer" : 1
                                        ,"sourceOfIncome" :sourceOfIncome,
                                        "remitPurpose" :remitPurpose,
                                        "depositoryAccountKey1" : this.state.depositoryAccountKey1

                             }
                             console.log(jsonResponse);
                            var authDataToken =authData();
                         // console.log('getBeneficiary///////// :-'+authDataToken);
                        
                                console.log(res);
                                this.setState({visible:false});
                                let result= JSON.parse(res); 
                                var jsonResult =  result.Status[0];
                                if(jsonResult.errorCode==1000){
                                 this.setState({visible:false});
                                    
                                   var TransactionInfo = result.TransactionInfo[0];
                                   const { navigate } = this.props.navigation;
                                   navigate('TransactionPage',{TransactionInfo : TransactionInfo,AccountsInfo:this.state.AccountsInfo})
                                }else if(jsonResult.errorCode==1001){
                                          alert(jsonResult.errorMessage);
                                }else{
                                    let errors = await response;
                                    throw errors;
                                }
                               
                  }catch (errors) { 
                                // console.log(errors);
                                 console.log(JSON.stringify(errors));
                               }
                               }else {
                          var msg =  'Your profile is under review and waiting for approval. Once your profile is verified then you can create transaction. We apologize for the inconvenience. You can contact us at 0292205132 for further detail.';
                        console.log(msg);
                        // console.log('global.IsApproved'+global.IsApproved);
                        // AsyncStorage.getItem("customerID1No").then((value) => {
                        //     if(value){
                        //         global.customerID1No = value;
                        //     }
                        // });
                        
                        if(global.IsApproved=='0'){
                            global.setTimeout(() => {
                                Alert.alert(
                                  'Warning',
                                 'Your profile is under review and waiting for approval. Once your profile is verified then you can create transaction. We apologize for the inconvenience. You can contact us at 0292205132 for further detail.',
                                  [
                                  {text : 'Complete KYC', onPress:()=>{this.goToKycScreen()}}
                                  ]
                                 );
                              }, 50);
                        }else{
                            global.setTimeout(() => {
                                Alert.alert(
                                  'Warning',
                                 'Your profile is under review and waiting for approval. Once your profile is verified then you can create transaction. We apologize for the inconvenience. You can contact us at 0292205132 for further detail.',
                                  [
                                  {text : 'Ok', onPress:()=>{this.goHomeScreen()}}
                                  ]
                                 );
                              }, 50);
                        }
                      

                        }

                    }
                    goToKycScreen  (){
                        const { navigate } = this.props.navigation;
                      
                        navigate('Kyc')
                    }
                    goHomeScreen(){
                        const { navigate } = this.props.navigation;
                      
                        navigate('FeeCalculator');
                    }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
   
  //  navigate('ReviewPage',{id:params.id,amountJson:params.amountJson}
   return (
     <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
         <KeyboardAvoidingView
         style={styles.container}
         behavior="padding" >
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.isLoading}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 300,
    borderRadius: 3,
  
    }}> 
    <Text style={{textAlign:'right',marginRight:10,paddingVertical:4}} onPress={()=>this.setState({visible:false})}>X</Text>
      <Text style={{textAlign:'center',paddingVertical:12}}>Data Save successfully</Text>
    
   </View>
   </View>
   </Modal>
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.isTerms}>
      <View style={{flex: 1,
    flexDirection: 'column',
   }} >
      <View style={{backgroundColor: '#fff',
    
   }}> 
   <View style={{borderBottomWidth:1,borderBottomColor:'#000',paddingBottom:10,flexDirection:'row',marginTop:10}}>
   <Text style={{flex:0.9,paddingHorizontal:5,paddingTop:20,fontSize:18,color:'red',textAlign:'center',}}> PRIVACY & POLICY </Text>
   <Text style={{flex:0.1,textAlign:'center',color:'#000000',marginTop:20}} onPress={()=>this.setState({isTerms : !this.state.isTerms})}><FontAwesome style={{fontSize: 18,color:'#000',textAlign:'center',}}>{Icons.close}</FontAwesome></Text>
   </View>
   <ScrollView>
   <Privacy />
   </ScrollView>
   </View>
   </View>
   </Modal>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('PurposePage',{id:params.id,amountJson:params.amountJson})}> <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Review Page</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('FeeCalculator')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
       {/* <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Beneficiary Information</Text>
       </View>
       */}
       <ScrollView style = {styles.scrollViewStyle}>
     
       
<View style={{paddingVertical:10,flexDirection:'column',}}>
       <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
          <Text style={styles.detailHeading}>Beneficiary Details:</Text>
      </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
               <Text>
                   Name:
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
              {this.state.newRes.FirstName} {this.state.newRes.LastName}
               </Text> 
               </View>
     
         </View> 
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                        Contact No.:
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
              {this.state.newRes.MobileNo!=null && <Text style={styles.textDetails}>{this.state.newRes.MobileNo}</Text> }
                   {this.state.newRes.MobileNo==null && this.state.newRes.MobileNo=='' && <Text style={styles.textDetails}>N/A</Text> }
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Email:
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
            {this.state.newRes.EmailAddress}
             </Text> 
        </View>

         </View>

</View> 

<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Delivery Informations:</Text>
      </View>
   
      <View style={styles.labelContainerDiv}>
      <View style={styles.labelDiv}>
               <Text>
                   Type & Currency :
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
               {this.state.beneficiariesCurrency} - {this.state.beneficiariesPayment}</Text> 
               </View>
     
         </View> 
         <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row',}}>
         <View style={{flex:1,borderColor:'gray',borderWidth:1,paddingBottom:8,borderRadius:4,paddingHorizontal:5,paddingVertical:8}}>
         <Text style={styles.textDetailsBox}>
           Bank / Account Info
         </Text>
        </View>
  </View> 
  <View style={styles.labelContainerDiv}>
  <View style={styles.labelDiv}>
                   <Text>
                        Bank Name. :
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
                    <Text style={styles.textDetails}>{this.state.newRes.BeneficiaryBankName}
                    </Text> 
             </View>
     
       
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Branch Name :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
            {this.state.newRes.BeneficiaryBranchName}
             </Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Branch Address :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.newRes.BeneficiaryBranchAddress}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                 Branch City :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.newRes.BeneficiaryBranchCity}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                 Branch Number :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.newRes.BeneficiaryBranchNumber}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Account No. :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.newRes.AccountNo_ProtectiveValue}</Text> 
        </View>

         </View>

</View> 
<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Payment Method Details:</Text>
      </View>
   
      <View style={styles.labelContainerDiv}>
      <View style={styles.labelDiv}>
               <Text>
                   Type  :
                </Text>
               </View>

               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>{this.state.beneficiariesCurrency} - {this.state.beneficiariesPayment}</Text> 
               </View>
     
         </View> 
         <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row',}}>
        
        <View style={{flex:1,borderColor:'gray',borderWidth:1,paddingBottom:8,borderRadius:4,paddingHorizontal:5,paddingVertical:8}}>
        <Text style={styles.textDetailsBox}>
          Payment Account Info
         </Text>
        </View>
  </View> 
  <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Deposit Bank Info. :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailss}>{this.state.AccountsInfo}</Text> 
        </View>

         </View>
</View> 
 <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>
         Transaction Information
     </Text>
      </View>
      <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Sending Amount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}>{this.state.senderCountryRate} {this.state.sendingAmount}</Text> 
        </View>

     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
            Handling Fee :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.senderCountryRate}  {this.state.HandlingFeeAmount} </Text> 
        </View>

     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Exchange Rate :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.senderCountryRate} to {this.state.receiveCountry} : {this.state.exchageRate}</Text> 
        </View>

     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Service Charges :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.senderCountryRate} {this.state.exchangeFee}</Text> 
        </View>
     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Discount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{this.state.senderCountryRate} {this.state.discountValue}</Text> 
        </View>
     </View> 
    
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Amount Due :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}>{this.state.senderCountryRate} {this.state.amountDue}</Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Beneficiary Amount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}>{this.state.receiveCountry} {this.state.beneficiaryAmount}</Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Transaction Purpose :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{global.transactionPurpose}</Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Source of Income :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{global.IncomeSorce}</Text> 
        </View>
     </View> 
</View>  
<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Schedule Details:</Text>
      </View>
   
         <View style={{paddingHorizontal : 10,paddingVertical:4,flexDirection:'row',}}>
        
               <View style={{flex:0.4,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8}}>
               <Text>
                   Perform On  :
                </Text>
               </View>

               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>{this.state.currentDt}</Text> 
               </View>
     
         </View> 
</View> 

        
       </View> 
       {!this.state.isLoading &&<View><View style={{paddingHorizontal : 10,paddingVertical:10,}}>
{this.state.chkError==true&&<Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>Please confirm terms and condition</Text>}
<View style={{flexDirection:'row'}}>
<CheckBox
     style={{flex: 0.1,}}
     onClick={()=>this.onClick(!this.state.isCheckeds)}
     isChecked={this.state.isCheckeds}
     checkBoxColor = {'#F06124'}
     rightText={''}
 /> 
 
 <Text style={{flex:0.9,color:'#F06124',alignItems:'center'}}>
 Check here to confirm that you have read and agree that<Text style={{textDecorationLine : 'underline',color:'#F06124'}} onPress={()=>this.setState({isTerms : !this.state.isTerms})}> terms & conditions </Text>.
 </Text>
 </View>

 </View>
       <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
         
         <Text style={styles.cancelBtn} onPress={()=>navigate('FeeCalculator')}>Cancel </Text>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}} >Continue </Text>
         </View>
         </View> }
</ScrollView>

       </View>

     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 20,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
},
cancelBtn : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 20,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
    marginHorizontal:4
},
continue : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 10,
  paddingHorizontal:10,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff',
  
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},

errorInput : {
  borderColor: '#dd4b39',
  borderWidth: 2,
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
  fontSize:16
},
textDetails : {color:'#F06124'},
textDetailsBox : {
    color:'#F06124',
    fontWeight:'bold'
},
textDetailss:{
    color:'#F06124',
    fontWeight:'bold',
    paddingVertical:8
},
textDetailsGreen : {
    color:'green',
    fontWeight:'bold'
},
detailHeading : {color:'#094F66',fontSize:16,fontWeight:'bold'},
labelContainerDiv : {paddingHorizontal : 10,paddingVertical:4,flexDirection:'row',},
labelDiv:{flex:0.4,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
valueDiv : {flex:0.5,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
blankDiv : {flex:0.1,borderBottomColor:'gray',borderBottomWidth:1,},

});