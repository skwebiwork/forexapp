import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight
  
} from 'react-native';
import { StatusBar } from 'react-native';
import { Container, Header, Content, Thumbnail,Badge  } from 'native-base'
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
//import SideBar from '../sidebar';
import styles from './BeneficiaryStyle';
import authData from '../../headerData';
export default class TransactionPage extends Component {
    constructor(props){
    super(props);
    this.state ={
      visible: false,
      isShow : false,


    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
   const { params } = this.props.navigation.state;
   var TransactionInfo = params.TransactionInfo;
   console.log(TransactionInfo);
   this.setState({ReferenceNo : TransactionInfo.ReferenceNo})
   var AccountsInfo = params.AccountsInfo;
   console.log(AccountsInfo);
   var arr = AccountsInfo.split('(');
   console.log(arr);
   
 }
                     
pressNav(){
 // Keyboard.dismiss();
  this.props.navigation.navigate("DrawerOpen");
}
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
        <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
   
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
        <View style={{flex:.2}}>
        <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 20,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
     </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Remittance Completed</Text>
            </View>
            <View style={{flex:.2}}>
            <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('FeeCalculator')}>
            <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome>
            </Text>
         </View>
        </View>

       {/* <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10,fontSize: 18}} >Transaction</Text>
       </View> */}
       {/* <View style={styles.logoContainer}>
         <Image source ={require('../img/app-logo-small.png')} />
        </View> */}
     {/*  <View style={{paddingHorizontal:15,flexDirection:'row',}}>
       <Text style={{flex:0.5,backgroundColor:'#5b6553',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,marginRight:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#5b6553'}}>Rates</Text>
       <Text style={{flex:0.5,backgroundColor:'#F06124',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#F06124'}}>Calculator</Text>
       </View> */}

<ScrollView >
          <View style={{paddingHorizontal:14,paddingVertical:20}}>
<View style={{paddingVertical:20,paddingHorizontal:10,flexDirection:'column',backgroundColor:'#eee',borderColor:'#eee',borderRadius:5,}}>
  
       <Text style={{color:'#484848',fontSize:20,paddingVertical:10}}>Transaction Successfull</Text>  
       <Text style={{color:'#484848',fontSize:14,paddingVertical:6,paddingVertical:8}}>Thankyou for sending money through <Text style={{color:'#F06124'}}>BFX AUSTRALIA!</Text></Text>  
       <Text style={{backgroundColor:'#094F66',paddingVertical:10,paddingHorizontal:15,color:'#fff',marginVertical:10}}>
              Order Number : {this.state.ReferenceNo}
       </Text>
  <Text style={{color:'#484848',fontSize:13,paddingVertical:6}}>
       To Complete the transaction, you must make a deposit for the total amount in the following bank acount.
  </Text>    
  <Text style={styles.labelText}> Account Details : {params.AccountsInfo} </Text>

  {/*<Text style={styles.labelText}>Pay to : <Text style={{fontWeight:'normal'}}>BFX AUSTRALIA PTY LTD</Text></Text>  
  <Text style={styles.labelText}>Bank Name : <Text style={{fontWeight:'normal'}}>NATIONAL AUSTRALIA BANK</Text></Text>  
  <Text style={styles.labelText}>Bank Branch : <Text style={{fontWeight:'normal'}}>NATIONAL AUSTRALIA BANK</Text></Text>  
  <Text style={styles.labelText}>Account Number : <Text style={{fontWeight:'normal'}}>851844076</Text></Text>  
  <Text style={styles.labelText}>Sort Code / Routing # / BSB # : <Text style={{fontWeight:'normal'}}>082-356</Text></Text>
  <Text style={styles.labelText}>BIC (SWIFT Code) : <Text style={{fontWeight:'normal'}}>NATAAU3303M</Text></Text>
  <Text style={styles.labelText}>BAN : <Text style={{fontWeight:'normal'}}>082-356</Text></Text>
  <Text style={styles.labelText}>Bank address : <Text style={{fontWeight:'normal'}}>494 Marrickville Road, Dulwich Hill, Sydney, NSW, 2203</Text></Text>  */}
  
  <Text style={{color:'#484848',fontSize:13,paddingVertical:6}}>
   Failure to make a deposit will result in cancelation of this transaction.
  </Text> 
  <Text style={{color:'#484848',fontSize:13,paddingVertical:6}}>
    Print the receipt for this transaction and kept it for your record. An email containing the receipt of your transaction has also been sent to you.
  </Text>
  <Text style={{color:'#484848',fontSize:13,paddingVertical:6}}>
     You can always check the status of your transaction on our app or by calling the help line at <Text style={{fontWeight:'bold'}}>(2) 92205132</Text> during the business hours.
  </Text>
  <Text style={{color:'#484848',fontSize:13,paddingVertical:6}}>
    We appreciate your business and look forward to see you again soon.
  </Text>

</View>
      
</View> 
<View style={{paddingHorizontal:14,paddingVertical:10}}>
<Text onPress={()=>navigate('FeeCalculator')} style={{color:'#fff',fontSize:16,paddingVertical:10,backgroundColor:'#F06124',textAlign:'center',fontWeight:'bold', borderRadius: 4, overflow:"hidden",}}>
    Done
  </Text>
</View>


</ScrollView>
     
       
       
     
     
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
