import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,Keyboard,KeyboardAvoidingView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import authData from '../../headerData';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNPickerSelect from 'react-native-picker-select';
export default class Fifth extends Component {
    constructor(props){
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.onCountryChange = this.onCountryChange.bind(this);
    this.typographyRef = this.updateRef.bind(this, 'typography');
    this.countryRef = this.updateCountryRef.bind(this, 'typography');
    global.count = 0;
    
    this.inputRefs = {};
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      senderCountry : [],
      beneficiaryResult :[],
      senderState : [{value: "Spot",label : "Spot"},],
      isError : false,
      errorMsg : '',
      firstName : '',
      firstNameError : false,
      countryError : false,
      stateError : false,
      cityError : false,
      country : '',
      stateName : '',
      city : '',
      discountValue : 0.00,
      countryLabel : ' Select Transaction purpose',
      stateLabel : ' Select Source of Income',
      processToday : '',
      beneficiariesFullName : 'N/A',
      beneficiariesEmail : 'N/A',
      beneficiariesCurrency : 'N/A',
      beneficiariesPayment : 'N/A',
      favColor: 'Select Source of Income',
      favColors : 'Select Transaction purpose',
      isCoupan : false,
      isCouponError : false,
      items: [
        {
          label: 'Red',
          value: 'red',
        },
        {
          label: 'Orange',
          value: 'orange',
        },
        {
          label: 'Blue',
          value: 'blue',
        },
      ],
      favSport: undefined,
      items2: [
        {
          label: 'Football',
          value: 'football',
        },
        {
          label: 'Baseball',
          value: 'baseball',
        },
        {
          label: 'Hockey',
          value: 'hockey',
        },
      ],
    } 
  }
  
  componentDidMount() {
    const { params } = this.props.navigation.state;
  //  console.log(params.id);
    var beneficiariesDetails = params.id;
    this.setState({beneficiariesFullName :beneficiariesDetails.BeneficiaryFullName })
    this.setState({beneficiariesEmail :beneficiariesDetails.EmailAddress})
    this.setState({beneficiariesCurrency :beneficiariesDetails.DeliveryMethodCurrency})
    this.setState({beneficiariesPayment :beneficiariesDetails.DeliveryMethodName })
   // StatusBar.setHidden(true);
    this.getSenderCountry();
    global.IncomeSorce = null;
    global.transactionPurpose =null;
    this.getBeneficiary();
    //this.getBeneficiaryBranch();
    
    var dt = Date();
    var dtArr = dt.split(' ');
    var ndt = dtArr[0]+' '+dtArr[1]+' '+dtArr[2]+', '+dtArr[3];
    this.setState({currentDt : ndt});
 }
 onChangeText(text) {
  //  console.log(text);
    this.setState({country:text,stateLabel:''});
    global.IncomeSorce = text;
    
   // this.getSenderState(text)
   }
   onCountryChange(text){
    console.log(text);
    this.setState({stateName:text,countryLabel:''});
       global.transactionPurpose = text;
   }
   updateRef(name, ref) {
     this[name] = ref;
   }
   updateCountryRef(name, ref) {
    this[name] = ref;
  }

 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
   if(value=='Individual'){
    this.setState({isCompany : false});

   }else{
    this.setState({isCompany : true});

   }
 }
 async getSenderCountry(){
   var authDataToken =authData();
        try
        { 
            let response = await fetch('http://api.remitanywhere.com/restapi/lists/sourceofincome', 
            { 
                method: 'GET',
                    headers: 
                    { 
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json', 
                        'Authorization' : authDataToken,
                        
                        }
                }); 
                let res = await response.text(); 
                    console.log(res);
                    let result= JSON.parse(res); 
                    var jsonResult =  result.Status[0];
                    if(jsonResult.errorCode == 1000){
                            var country = result.SourceOfIncomeInfo;
                            var res = [];
                          //  res.push({value : "Select Source of Income",label : "Select Source of Income"})
                            for(var i=0;i<country.length;i++){ 
                                res.push({value : country[i].Value,label :country[i].Value });
                            }  
                           
                            this.setState({senderCountry : res});
                    }
    }catch (error) { 
                     console.log('catch');
                   }
 }
 
  async getBeneficiary(){
  
    const { params } = this.props.navigation.state;
    let bDetails = params.id;
    var authDataToken =authData();
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/'+bDetails.BeneficiaryKey, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.text(); 
            console.log(res);
            let result= JSON.parse(res); 
            var jsonResult =  result.Status[0];
            if(jsonResult.errorCode==1000){
                this.setState({ beneficiaryResult : result.BeneficiaryInfo})
                console.log(result.BeneficiaryInfo);
            }else{
              let errors = res;
              throw errors;
            }
               
  }catch (error) { 
                 console.log(error);
               }
   }

 onSubmit(){
 // navigate('ReviewPage',{id:params.id,amountJson:params.amountJson}
//
                    }
                    submitFun(){
                      var purpose = this.state.favColors;
                      var sourceIncome = this.state.favColor;
                      var isCoupan = this.state.isCoupan;
                      console.log('purpose'+purpose);
                      console.log('sourceIncome'+sourceIncome);
                      if(purpose=='Select Transaction purpose'){
                          this.setState({errorMsg : "Please select Transaction purpose",isError:true})
                      }else if(sourceIncome=='Select Source of Income'){
                        this.setState({errorMsg : "Please select Income Source",isError:true})

                      }else if(isCoupan===true){
                        this.setState({errorMsg : "Please enter a valid coupon",isError:true})
                      }
                      
                      else{
                        this.setState({errorMsg : "",isError:false})
                        
                        const { navigate } = this.props.navigation;
                        const { params } = this.props.navigation.state;
                        params.amountJson.discountValue = this.state.discountValue;
                      navigate('ReviewPage',{id:params.id,amountJson:params.amountJson})
                      }
                    }
                    addDays(dateObj, numDays) {
                      dateObj.setDate(dateObj.getDate() + numDays);
                      return dateObj;
                    }
                    async changeBlurDiscountFun(){
                      this.setState({errorMsg : "",isError:false})
                      this.setState({isCoupan:false,isCouponError:false});
                      const { params } = this.props.navigation.state;
                      var amountJson = params.amountJson;
                      var Coupon = this.state.Coupon;
                      var sendingCurrency = amountJson.senderCountry;
                      var receiveCountry = amountJson.receiveCountry
                      console.log(Coupon);
                      var BeneficiaryInfo = this.state.beneficiaryResult;
                      var CustomerKey = BeneficiaryInfo[0].CustomerKey;
                      var DeliveryMethodKey = BeneficiaryInfo[0].DeliveryMethodKey;
                      var receiveCountryKey = BeneficiaryInfo[0].CountryKey;
                       var senderCountryKey = global.customerCountryKey;
                  var paymentMethod = 1;
                  var serviceCharge =  amountJson.exchangeFee;
                  var remmitanceDate = "2018-05-02";
                  var nextWeek = this.addDays(new Date(), 1);
      let n = nextWeek.getDate();
      n = n < 10 ? "0" + n : n;
      let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
//      let datass = mm+'/'+n+'/'+nextWeek.getFullYear();
      let datass = nextWeek.getFullYear()+'-'+mm+'-'+n;
                  var param = {
                    "couponCode":Coupon
                    ,"customerKey":CustomerKey
                    ,"senderCountryKey":senderCountryKey
                    ,"receiverCountryKey":receiveCountryKey
                    ,"paymentMethodKey":paymentMethod
                    ,"deliveryMethodKey":DeliveryMethodKey
                    ,"currencyFrom":sendingCurrency
                    ,"currencyTo":receiveCountry
                    ,"payeeLocationKey":1
                    ,"serviceCharges":serviceCharge
                    ,"remittanceDate":datass
                  }
                  console.log(param);
                  var authDataToken =authData();
                  try
                  { 
                      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/fee/discountbycoupon', 
                      { 
                          method: 'POST',
                              headers: 
                              { 
                                  'Accept': 'application/json', 
                                  'Content-Type': 'application/json', 
                                  'Authorization' : authDataToken,
                                  
                                  },
                                  body : JSON.stringify(param)
                          }); 
                          let res = await response.text(); 
                          console.log(res);
                          let result= JSON.parse(res); 
                          var jsonResult =  result.Status[0];
                          if(jsonResult.errorCode==1000){
                              var DiscountInfo = jsonResult.DiscountInfo[0];
                              var discountValue = DiscountInfo.discountValue;
                     this.setState({discountValue : discountValue});
                     this.setState({isCoupan : false,isCouponError:false});
                          }else if(jsonResult.errorCode==1002){
                            this.setState({isCoupan : true});
                           // this.setState({Coupon : ''})
                      //   this.CoupanInput.focus();
                            // global.setTimeout(() => {
                            //   Alert.alert(
                            //     'Warning',
                            //     'This coupon code is invalid or does not exist. Please use a valid code.',
                            //     [
                            //     {text : 'OK'}
                            //     ]
                            //    );
                            // }, 50);
                            this.setState({errorMsg : "This coupon code is invalid or does not exist. Please use a valid code.",isCouponError:true})
                          }else if(jsonResult.errorCode==1003){
                            this.setState({isCoupan : true});
                        //    this.setState({Coupon : ''})
                         //   this.CoupanInput.focus();
                            // global.setTimeout(() => {
                            //   Alert.alert(
                            //     'Warning',
                            //     'This coupon code is expired. Please use a different code',
                            //     [
                            //     {text : 'OK'}
                            //     ]
                            //    );
                            // }, 50);
                            this.setState({errorMsg : "This coupon code is expired. Please use a different code.",isCouponError:true})
                            
                          }else{
                            let errors = res;
                            throw errors;
                          }
                             
                }catch (error) { 
                               console.log(error);
                             }
                  
                    }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    
   return (
     <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
         <KeyboardAvoidingView
         style={styles.container}
         behavior="padding" >
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 300,
    borderRadius: 3,
  
    }}> 
    <Text style={{textAlign:'right',marginRight:10,paddingVertical:4}} onPress={()=>this.setState({visible:false})}>X</Text>
      <Text style={{textAlign:'center',paddingVertical:12}}>Data Save successfully</Text>
     
    
    
   </View>
   </View>
   </Modal>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('Beneficiary')}> <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Send Money</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('Beneficiary')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Beneficiary Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}>
     
      
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10}}>
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Beneficiary Details:</Text>
      </View>
   
         <View style={{paddingHorizontal : 25,paddingVertical:4,flexDirection:'row'}}>
        
               <View style={{flex:0.4,}}>
               <Text>
                   Name:
                </Text>
               </View>
               <View style={{flex:0.1}}></View>
               <View style={{flex:0.5}}>
               <Text style={styles.textDetails}>{this.state.beneficiariesFullName}</Text> 
               </View>
     
         </View> 
        <View style={{paddingHorizontal : 25,paddingVertical:4,flexDirection:'row'}}>
        
               <View style={{flex:0.4,}}>
                   <Text>
                        Email:
                  </Text>
              </View>
              <View style={{flex:0.1}}></View>
               <View style={{flex:0.5}}>
                    <Text style={styles.textDetails}>{this.state.beneficiariesEmail}</Text> 
             </View>
     
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:4,flexDirection:'row'}}>
        
        <View style={{flex:0.4,}}>
            <Text>
                 Currency:
            </Text>
        </View>
        <View style={{flex:0.1}}></View>
        <View style={{flex:0.5}}>
             <Text style={styles.textDetails}>{this.state.beneficiariesCurrency}</Text> 
        </View>

         </View>
  <View style={{paddingHorizontal : 25,paddingVertical:4,flexDirection:'row'}}>
        
        <View style={{flex:0.4,}}>
           <Text>
               Payment Type:
          </Text>
        </View>
        <View style={{flex:0.1}}></View>
        <View style={{flex:0.5}}>
           <Text style={styles.textDetails}>{this.state.beneficiariesPayment}</Text>
         </View>

  </View> 
</View> 
    <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,}}>
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>
         Purpose & Source of Funds
         </Text>
      </View>
      {this.state.isError&&<Text style={{color:'red',paddingHorizontal : 25,paddingVertical:10,}}>{this.state.errorMsg}</Text>}
      <View style={{paddingHorizontal : 15,paddingVertical:7,flexDirection:'row'}}>
      <View style={{flex:0.9,}}>
     <RNPickerSelect
            placeholder={{
              label: 'Type of transaction',
              value: 'Select Transaction purpose',
            }}
          items={this.state.senderState}
          onValueChange={
          (item) => {
            this.setState({
              favColors: item,
              
            });
            console.log(item);
            global.transactionPurpose = item;
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.favColors}
         
        />
        </View>
        <Text style={{color:'red',flex:.1,textAlign:'center',fontSize:16}}>*</Text>
       </View>

        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
        
         <Dropdown
              ref={this.countryRef}
              onChangeText={this.onCountryChange}
              label={this.state.countryLabel}
              data={this.state.senderState}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={[{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5},this.state.stateError&&styles.errorInput]}
              baseColor = {'#484848'}
            />

            
         </View> */}
   <View style={{paddingHorizontal : 15,paddingVertical:7,flexDirection:'row'}}>
      <View style={{flex:0.9}}>
     <RNPickerSelect
         placeholder={{
          label: 'Select Source of Income',
          value: 'Select Source of Income',
        }}
          items={this.state.senderCountry}
          onValueChange={
          (item) => {
            this.setState({
              favColor: item,
            });
            console.log(item);
            global.IncomeSorce = item;
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.favColor}
         
        />
        </View>
         <Text style={{color:'red',flex:.1,textAlign:'center',fontSize:16}}>*</Text>
       </View>
      {/*   <View style={{paddingHorizontal : 25,paddingVertical:7,}}>
         
         
         <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label={this.state.stateLabel}
            
              data={this.state.senderCountry}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              baseColor = {'#484848'}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={[{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5},this.state.countryError&&styles.errorInput]}
             
            />
       
         </View> */}
         </View> 
         <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10}}>
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>
         Coupon Code
     </Text>
      </View>
      {this.state.isCouponError&&<Text style={{color:'red',paddingHorizontal : 25,paddingVertical:10,}}>{this.state.errorMsg}</Text>}
      
         <View style={{paddingHorizontal : 15,paddingVertical:10,flexDirection:'row'}}>
              
               <View style={{flex:0.9}}>
               <TextInput
       placeholder="Coupon Code" style={[styles.InputText,this.state.firsftNamfeError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({Coupon:text})}
       onBlur = {e=>this.changeBlurDiscountFun()}
       value = {this.state.Coupon}
       ref={(input)=>this.CoupanInput=input}
       ></TextInput>
               </View>
         </View>   
         </View> 

        <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10}}>
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>
         Transaction Schedule
     </Text>
      </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,flexDirection:'row'}}>
               <View style={{flex:0.3,}}>
                   <Text>
                        Process On : 
                  </Text>
            </View>
               <View style={{flex:0.7}}>
               <Text style={styles.radioText}>{this.state.currentDt}</Text></View>
         </View>   
         </View> 
    
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={()=>this.submitFun() }>Continue </Text>
         </View>
       </View>
</ScrollView>

       </View>

     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 20,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
},
continue : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 10,
  paddingHorizontal:10,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff',
  
  
},
radioText : {
  paddingHorizontal:7,
 color:'#F06124',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},

errorInput : {
  borderColor: '#dd4b39',
  borderWidth: 2,
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
  fontSize:16
},
textDetails : {color:'#F06124'},

  containerss: {
    paddingTop: 30,
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    backgroundColor: 'white',
    color:'#484848'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: 'gray',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  }
});