import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,Keyboard,KeyboardAvoidingView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import authData from '../../headerData';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import CheckBox from 'react-native-check-box'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
export default class SingleBeneficiary extends Component {
    constructor(props){
    super(props);
  
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      senderCountry : [],
      senderState : [{value: "other",label : "Other"},{value: "spot",label : "Spot"},{value: "today",label : "Today"},{value: "tommorrow",label : "Tomorrow"}],
      isErrorMsgs : '',
      errorMsg : '',
      firstName : '',
      firstNameError : false,
      countryError : false,
      stateError : false,
      cityError : false,
      country : '',
      stateName : '',
      city : '',
      countryLabel : ' Select Transaction purpose',
      stateLabel : ' Select Source of Income',
      processToday : '',
      beneficiariesFullName : 'N/A',
      beneficiariesEmail : 'N/A',
      beneficiariesCurrency : 'N/A',
      beneficiariesPayment : 'N/A',
      beneficiaryResult:[],
      isChecked : false,
      chkError : false,
      beneficiaryAmount:'',
      
    } 
  }

 
 
  componentDidMount() {
    const { params } = this.props.navigation.state;


   // StatusBar.setHidden(true);
   
    this.getBeneficiary();
    //this.getBeneficiaryBranch();
    var dt = Date();
    var dtArr = dt.split(' ');
    var ndt = dtArr[1]+' '+dtArr[2]+', '+dtArr[3];
    this.setState({currentDt : ndt});

 }
 onSubmit(){
     console.log('isChecked');
   //  this.setState({ chkError: true })
     if(!(this.state.isChecked)){
        
    this.setState({ chkError: true })
        console.log('false',this.state.chkError);
        
     }else{
        this.setState({ chkError: false })
         
        const { navigate } = this.props.navigation;
      //  navigate('TransactionPage');
    this.compleTransaction();
     
     }
    
 }
 onClick(chk){
    console.log(chk);
   this.setState({isChecked:chk}); 
   console.log(this.state.isChecked);
}


  async getBeneficiary(){
    this.setState({visible:true});
    const { params } = this.props.navigation.state;
    let bDetails = params.id;
    var authDataToken =authData();
   console.log(bDetails+'getBeneficiary****'+authDataToken);
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/'+bDetails, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.text(); 
            var authDataToken =authData();
            this.setState({visible:false});
         // console.log('getBeneficiary///////// :-'+authDataToken);
                console.log(res);
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.errorCode==1000){
                    this.setState({ beneficiaryResult : result.BeneficiaryInfo})
                    console.log(result.BeneficiaryInfo);
                    console.log(result.BeneficiaryInfo[0].OptionName);
                    this.setState({beneficiariesCurrency : result.BeneficiaryInfo[0].OptionName})
                    this.setState({beneficiariesPayment: result.BeneficiaryInfo[0].DeliveryMethodCurrency});
                }else{
                    let errors = result;
                    throw errors;
                }
               
  }catch (errors) { 
               //  console.log(errors);
                 console.log(errors);
               }
   }
   deletePress(){
   
    Alert.alert(
      'Confirmation',
      'Are sure want to delete Beneficiary?',
      [
        {text: 'Yes', onPress: () => this.deleteBeneficiary()},
        {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
      ]
  );

 }
 async deleteBeneficiary(){
    const { params } = this.props.navigation.state;
    let bDetails = params.id;
    var authDataToken =authData();
  this.setState({visible : true});
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/beneficiary/profile/delete', 
        { 
            method: 'POST',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    },body : JSON.stringify({
                        "beneficiaryKey": bDetails,
                        "customerKey": global.CustomerKey
                    })
            }); 
            let res = await response.text(); 
            var authDataToken =authData();
         // console.log('getBeneficiary///////// :-'+authDataToken);
                console.log(res);
                this.setState({visible : false});
                let result= JSON.parse(res); 
                var jsonResult =  result.Status[0];
                if(jsonResult.ErrorCode==1000){
                    const { navigate } = this.props.navigation;
                    navigate('BeneficiaryList');
                }else{
                    let errors = res;
                    throw errors;
                }
               
  }catch (errors) { 
               //  console.log(errors);
                 console.log(JSON.stringify(errors));
               }
   }
 
                    goTOFeeCalculator  (){
                        const { navigate } = this.props.navigation;
                        navigate('FeeCalculator')
                    }
                    editPress(){
                        const { navigate } = this.props.navigation;
                        const { params } = this.props.navigation.state;
                        console.log(params.id);
                        navigate('EditBeneficiary',{id:params.id});
                    }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
  //  navigate('ReviewPage',{id:params.id,amountJson:params.amountJson}
   return (
     <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
         <KeyboardAvoidingView
         style={styles.container}
         behavior="padding" >
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('BeneficiaryList')}> <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Beneficiary Information</Text>
            </View>
            <View style={{flex:.2,flexDirection:'row'}}>
              
               <Text style={{flex:0.5,textAlign:'center'}}  onPress={()=>this.editPress()}> <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}} >{Icons.pencil}</FontAwesome> </Text>
          <Text style={{flex:0.5}} onPress={()=>this.deletePress()}> <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.trash}</FontAwesome> </Text>
           
            </View>
        </View>
       {/* <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Beneficiary Information</Text>
       </View>
       */}
       <ScrollView style = {styles.scrollViewStyle}>
     
       <FlatList 
                      data={this.state.beneficiaryResult}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>  
<View style={{paddingVertical:10,flexDirection:'column',}}>
       <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row'}}>
          <Text style={styles.detailHeading}>Beneficiary Details:</Text>
          <View style={{flex:0.2,flexDirection:'row'}}>
           {/* <Text style={{textAlign:'center'}}  onPress={()=>this.editPress()}> <FontAwesome style={{fontSize: 18,color:'#094F66',textAlign:'center'}} >{Icons.pencil}</FontAwesome> </Text>
          <Text style={{textAlign:'center'}} onPress={()=>this.deletePress()}> <FontAwesome style={{fontSize: 18,color:'#094F66',textAlign:'center'}}>{Icons.trash}</FontAwesome> </Text>
           */}
          </View>
      </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
               <Text>
                   Name:
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
              {item.FirstName} {item.LastName}
               </Text> 
               </View>
     
         </View> 
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                        Contact No.:
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
              {!item.MobileNo=='' && <Text style={styles.textDetails}>{item.MobileNo}</Text> }
                   {item.MobileNo==null && <Text style={styles.textDetails}>N/A</Text> }
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Email:
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
            {item.EmailAddress}
             </Text> 
        </View>

         </View>

</View> 

<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Delivery Informations:</Text>
      </View>
   
      <View style={styles.labelContainerDiv}>
      <View style={styles.labelDiv}>
               <Text>
                   Type & Currency :
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
               {item.OptionName} - {item.DeliveryMethodCurrency}</Text> 
               </View>
     
         </View> 
         <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row',}}>
         <View style={{flex:1,borderColor:'gray',borderWidth:1,paddingBottom:8,borderRadius:4,paddingHorizontal:5,paddingVertical:8}}>
         <Text style={styles.textDetailsBox}>
           Bank / Account Info
         </Text>
        </View>
  </View> 
  <View style={styles.labelContainerDiv}>
  <View style={styles.labelDiv}>
                   <Text>
                        Bank Name. :
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
                    <Text style={styles.textDetails}>
                    {item.BeneficiaryBankName}
                    </Text> 
             </View>
     
       
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Branch Name :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
            {item.BeneficiaryBranchName}
             </Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Branch Address :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{item.BeneficiaryBranchAddress}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                 Branch City :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{item.BeneficiaryBranchCity}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                 Branch Number :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{item.BeneficiaryBranchNumber}</Text> 
        </View>

         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
                 Account No. :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{item.AccountNo_ProtectiveValue}</Text> 
        </View>

         </View>

</View> 
{/*<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>Payment Method Details:</Text>
      </View>
   
      <View style={styles.labelContainerDiv}>
      <View style={styles.labelDiv}>
               <Text>
                   Type  :
                </Text>
               </View>

               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>{this.state.beneficiariesCurrency} - {this.state.beneficiariesPayment}</Text> 
               </View>
     
         </View> 
         <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row',}}>
        
        <View style={{flex:1,borderColor:'gray',borderWidth:1,paddingBottom:8,borderRadius:4,paddingHorizontal:5,paddingVertical:8}}>
        <Text style={styles.textDetailsBox}>
          Payment Account Info
         </Text>
        </View>
  </View> 
  <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Deposit Bank Info. :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailss}>BFX Australia Pty Ltd (National Australia Bank - Act.No.:851844076 - Sort Code/Routing#/BSB#: 082-356)</Text> 
        </View>

         </View>
</View> */}
 {/*<View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,}}>
     <Text style={{color:'#094F66',fontSize:16,fontWeight:'bold'}}>
         Transaction Information
     </Text>
      </View>
      <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Sending Amount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}></Text> 
        </View>

     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Exchange Rate :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}></Text> 
        </View>

     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Service Charges :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}></Text> 
        </View>
     </View>  
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Discount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}></Text> 
        </View>
     </View> 
    
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Amount Due :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}></Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Beneficiary Amount :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetailsGreen}></Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Transaction Purpose :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{global.transactionPurpose}</Text> 
        </View>
     </View> 
     <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
            <Text>
                Source of Income :
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>{global.IncomeSorce}</Text> 
        </View>
     </View> 
</View>   */}
 

    
       </View> }/>
</ScrollView>

       </View>

     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 20,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
},
cancelBtn : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 20,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
    marginHorizontal:4
},
continue : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 10,
  paddingHorizontal:10,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff',
  
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},

errorInput : {
  borderColor: '#dd4b39',
  borderWidth: 2,
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
  fontSize:16
},
textDetails : {color:'#F06124'},
textDetailsBox : {
    color:'#F06124',
    fontWeight:'bold'
},
textDetailss:{
    color:'#F06124',
    fontWeight:'bold',
    paddingVertical:8
},
textDetailsGreen : {
    color:'green',
    fontWeight:'bold'
},
detailHeading : {color:'#094F66',fontSize:16,fontWeight:'bold',flex:0.9},
labelContainerDiv : {paddingHorizontal : 10,paddingVertical:4,flexDirection:'row',},
labelDiv:{flex:0.4,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
valueDiv : {flex:0.5,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
blankDiv : {flex:0.1,borderBottomColor:'gray',borderBottomWidth:1,},

});