import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import { Container, Header, Content, Thumbnail,Badge  } from 'native-base'
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
//import SideBar from '../sidebar';
import styles from './BeneficiaryStyle';
import authData from '../../headerData';
export default class Beneficiary extends Component {
    constructor(props){
    super(props);
    global.count = 0;
    this.onChangeText = this.onChangeText.bind(this);
        this.typographyRef = this.updateRef.bind(this, 'typography');
    this.state ={
      accountType : 'Individual',
      visible: false,
      reasone:[],
      beneficiaryList :[],
      isShow : false,

    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
  this.getBeneficiary();
    /*AsyncStorage.getItem('currenctList', (err, result) => {
      // console.log(result);
       let cat = JSON.parse(result);
      
     });*/
     global.firstName = '';
     global.lastName = '';
     global.middleName = '';
     global.addressLine1 = '';
     global.addressLine2 = '';
     global.country = '';
     global.stateName = '';
     global.city = '';
     global.zipCode = '';
     global.emailAddress = '';
     global.mobile = '';

 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 onChangeText(text) {
    //console.log(text);
    this.setState({getReason:text});
   }
   updateRef(name, ref) {
     this[name] = ref;
   }
  // http://api.remitanywhere.com/restapi/lists/beneficiary/branches

              
               async getCurrencyCode(key){
                var authDataToken =authData();
                 //  console.log(key);
                try
                { 
                  let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/currencies/'+key, 
                    { 
                      method: 'GET',
                          headers: 
                            { 
                               'Accept': 'application/json', 
                               'Content-Type': 'application/json', 
                               'Authorization' : authDataToken,
                              
                              }
                         }); 
                         let res = await response.text(); 
                     //   console.log(res);
                       
                           let result= JSON.parse(res); 
                           var jsonResult =  result.Status[0];
                           if(jsonResult.errorCode == 1000){
                            var country = result.CurrenciesInfo[0];
                            return country;
                           }
                          
                       
                          } catch (error) { 
               
                  console.log('catch');
             
                          }
                          }
                    
 onSubmit(){
    const { navigate } = this.props.navigation;
    Alert.alert(
        'Message',
        'Are you sure want to logout!',
        [
            {text : 'Cancle', onPress:()=>{console.log('cancel')}},
          {text : 'OK', onPress:()=>{navigate('Forth')}}
        ]
       );
   
  
     
 }
 getAllCountries(){

  AsyncStorage.getItem('currenctList', (err, result) => {
    // console.log(result);
     let cat = JSON.parse(result);
     this.setState({
       // dataSource: this.state.dataSource.cloneWithRows(data)
       currencyList : cat,
       addCurrency : true
      })
   });
 }
 async addCurrencyList(data){
   //console.log('gsgdsgdsgdsgds');
   console.log(data);
   var sendingCurrency = this.state.sendingCurrency;
   var receivingCurrency = data.currenctKey;
   var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth()+1; //January is 0!
   
   var yyyy = today.getFullYear();
   if(dd<10){
       dd='0'+dd;
   } 
   if(mm<10){
       mm='0'+mm;
   } 
   var currentDate = mm+'/'+dd+'/'+yyyy;

   console.log(today+'\n'+sendingCurrency+'\n'+receivingCurrency);
   this.setState({addCurrency : false});
  var ExRateInfo = await this.getRates(sendingCurrency,receivingCurrency,currentDate);
  
  console.log(ExRateInfo);
  var ExRate = ExRateInfo.ExRate;
  var CurrencyFrom = ExRateInfo.CurrencyFrom;
  var CurrencyTo = ExRateInfo.CurrencyTo;
  this.saveCustomerRate(CurrencyFrom,CurrencyTo,currentDate,ExRate);
   {/*
  "sendingCurrency":"USD"
	,"receivingCurrency":"GMD"
	,"date":"02/15/2018"
	,"deliveryMethodKey":1
	,"paymentMethodKey":1
	,"payeeLocationKey":1
	,"customerKey":null

  */}
 }


 async getBeneficiary(){
 // console.log(sendingCurrency,receivingCurrency,currentDate,ExRate);
 var authDataToken =authData();
 //  console.log(key);
try
{ 
  let response = await fetch('http://api.remitanywhere.com/restapi/customer/beneficiaries/select', 
    { 
      method: 'POST',
          headers: 
            { 
               'Accept': 'application/json', 
               'Content-Type': 'application/json', 
               'Authorization' : authDataToken,
              
              },
              body : JSON.stringify({
                "customerKey":global.CustomerKey
	
	,"beneWithDMofMobileTopup":0

              })
         }); 
         let res = await response.text(); 
       console.log(res);
       
           let result= JSON.parse(res); 
           var jsonResult =  result.Status[0];
           if(jsonResult.errorCode == 1000){
             console.log(result.BeneficiariesInfo);
             if(result.BeneficiariesInfo.length>0){
                this.setState({isShow : true});
                this.setState({beneficiaryList : result.BeneficiariesInfo});
             }else{
              this.setState({isShow : false});
             }
          // var ExRateInfo = result.ExRateInfo[0];
         //  return ExRateInfo;
           }else{
            let errors = res;
            throw errors;
           }
          
       
          } catch (error) { 

  console.log('catch');
  console.log(error);
          }
}
nameFun(fName,lName){
  var f = fName.substring(0, 1).toUpperCase();
  var l = lName.substring(0, 1).toUpperCase();
  return f+l;
}
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
        <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
   
       <View
         style={styles.imageContainer}>
       {/*  <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
        <View style={{flex:.2}}>
        <TouchableHighlight underlayColor='transparent' onPress={()=>navigate('FeeCalculator')}>
        <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} >
        <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome>
        </Text>
        </TouchableHighlight>
     </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Select a Beneficiary</Text>
            </View>
            <View style={{flex:.2}}>
            <TouchableHighlight underlayColor='transparent' onPress={()=>navigate('FeeCalculator')}>
            <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}}>
            <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome>
            </Text>
            </TouchableHighlight>
         </View>
        </View>

     {/*   <View style={{height:40,backgroundColor:'#094F66',flexDirection:'row'}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10,fontSize: 18,flex:0.9}} >Add Beneficiary</Text>
             <Text style={{flex:0.1,paddingTop:10}} onPress={()=>navigate('AddBeneficiary')}>
                         <FontAwesome style={{fontSize: 15,color:'#fff',textAlign:'center'}}>{Icons.plus}</FontAwesome>
                      </Text>  
   </View> */}
       {/* <View style={styles.logoContainer}>
         <Image source ={require('../img/app-logo-small.png')} />
        </View> */}
     {/*  <View style={{paddingHorizontal:15,flexDirection:'row',}}>
       <Text style={{flex:0.5,backgroundColor:'#5b6553',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,marginRight:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#5b6553'}}>Rates</Text>
       <Text style={{flex:0.5,backgroundColor:'#F06124',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#F06124'}}>Calculator</Text>
       </View> */}
<View style={{paddingVertical:20,paddingHorizontal:2,flexDirection:'column',}}>
      
  
          <TouchableHighlight underlayColor='transparent' onPress={()=>navigate('AddBeneficiary')} style={{flexDirection:'row',borderBottomColor:'#5b6553',borderBottomWidth:1,paddingVertical:15,paddingHorizontal:10,backgroundColor:'transparent'}}>
     <View style={{flexDirection:'row',flex:1}}>
        
     <Text style={{textAlign:'left',fontSize:16,color:'#5b6553',flex:0.9,paddingVertical:5,}}>
     <FontAwesome style={{fontSize: 20,color:'#094F66',textAlign:'center'}}>{Icons.plusCircle}</FontAwesome>    Add a Beneficiary</Text>
     
   {/*} <Text style={{flex:0.1,height:32,width:30,borderRadius:15,fontSize:14,color:'#094F66', textAlign: 'center',borderColor:'#094F66',overflow:'hidden',borderWidth:1,paddingVertical:5,
                      }}>
                         <FontAwesome style={{fontSize: 15,color:'#094F66',textAlign:'center'}}>{Icons.plus}</FontAwesome>
                      </Text>  */}

       <Text style={{flex:0.1,textAlign:'right',paddingVertical:5,}} ><FontAwesome style={{fontSize: 20,color:'#094F66',textAlign:'center'}}>{Icons.angleRight}</FontAwesome> </Text>  
     </View>
 </TouchableHighlight> 
 <View style={{paddingTop:20}}> 
 <View style={{height:45,backgroundColor:'#094F66',borderTopRightRadius:6,borderTopLeftRadius:6,overflow:'hidden',}}>
             <Text style={{color:'#fff',fontWeight:'normal',fontSize: 16,paddingLeft:15,paddingVertical:10}}>Beneficiaries</Text>
       </View>
         
       </View>
       {this.state.isShow && <View style = {[styles.listMainDiv,{paddingTop:3}]}> 
<FlatList 
                      data={this.state.beneficiaryList}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                      <View style={{flex:1,borderBottomColor:'#5b6553',borderBottomWidth:1,paddingVertical:6 }}>
                        <TouchableHighlight onPress={()=>navigate('PurposePage',{id:item,amountJson:params.amountJson})}>
                        <View style={styles.oddDiv}>
                        <View style={{flex:0.1,alignSelf:'center'}}>
                        
                       <Text style={{height:36,width:34,backgroundColor:'#094F66',overflow:'hidden',fontSize:14,color:'#F06124', textAlign: 'center',paddingVertical:8,borderRadius:17.5,
                      }}>{this.nameFun(item.FirstName,item.LastName)}</Text>

                  
                        </View>
                        <View style={{flex:0.7,flexDirection:'column',paddingHorizontal:25,alignSelf:'center'}}>
                       
                        <Text style={{fontSize:16,color:'#5b6553'}}>{item.BeneficiaryFullName}</Text>
                           <Text style={{paddingVertical:4,fontSize:16,color:'#094F66',}}>{item.DeliveryMethodCurrency}</Text>  
                          
                        </View>
                        
                                   <View style={{flex:0.2,alignSelf:'center'}}>
                                <Text style={{color:'gray',textAlign:'right',paddingVertical:10}}><FontAwesome style={{fontSize: 26,color:'teal'}}>{Icons.angleRight}</FontAwesome></Text>
                        </View>
                    </View>
                  
                 </TouchableHighlight>
                    
                  </View>
                      }/>
                      </View>}
                      {!this.state.isShow && <View style={[styles.oddDiv,{paddingTop:15,borderBottomRightRadius:6,borderBottomLeftRadius:6,overflow:'hidden',}]}>
   <Text style={{color:'red'}}>No Beneficiary added yet !</Text>
</View>}
</View>
      
       

{this.state.isShow && <View style={{marginBottom:40,paddingHorizontal:5,}}>

{/*<FlatList 
                      data={this.state.beneficiaryList}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                      <View style={{flex:1,borderBottomColor:'gray',borderBottomWidth:1,paddingVertical:5, }}>
                        
                        <View style={styles.oddDiv}>
                        <View style={{flex:0.1,}}>
                        
                       <Text style={{height:35,width:35,backgroundColor:'red',borderRadius:17.5,overflow:'hidden',padding:7,fontSize:14,color:'#5b6553',}}> AJ</Text>
                  
                        </View>
                        <View style={{flex:0.7,flexDirection:'column',paddingHorizontal:15}}>
                       
                        <Text style={{fontSize:16,color:'#5b6553'}}>{item.BeneficiaryFullName}</Text>
                           <Text style={{paddingVertical:4,fontSize:16,color:'#5b6553',}}>{item.DeliveryMethodCurrency} Account</Text>  
                          
                        </View>
                        
                                   <View style={{flex:0.2,}}>
                                <Text style={{color:'gray',textAlign:'center'}}><FontAwesome style={{fontSize: 26,color:'teal'}}>{Icons.angleRight}</FontAwesome></Text>
                        </View>
                    </View>
                  
                 
                    
                  </View>
                      }/> */}
</View>

}
     
       
       
     
     
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
