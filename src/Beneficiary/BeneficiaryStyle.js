//import { StyleSheet } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({

    container : {
      flex: 1,
      backgroundColor: '#fff',
      flexDirection:'column',
      
    },
    imageContainer : {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    textContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    
    
    },
    logoContainer: { alignItems: 'center',paddingVertical:35},
    logoText:{color:'#fff',fontSize:12,textAlign:'center',},
    next : {
    
    backgroundColor:'#F06124',
    borderRadius: 18,
    paddingVertical : 10,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    },
    bottomInstructions : {
    color:'#fff',
    fontSize:14,
    textAlign:'center',
    paddingVertical:25,
    paddingHorizontal:10,
    marginTop:'40%',
    marginBottom:'5%',
    },
    InputText : {
      paddingVertical : 10,
      borderRadius:3,
      borderColor: '#5b6553',
       borderWidth: 1,
       paddingHorizontal: 5,
       backgroundColor:'#5b6553',
       textAlign:'center',
       color:'#ffffff',
      
    },
    radioText : {
      paddingHorizontal:7,
     color:'#000',
      justifyContent : 'center',
    },
    scrollViewStyle : {
      paddingVertical:'1%',
      marginBottom:40,
    },
    oddDiv : {
       
        backgroundColor:'#fff',
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:8
    },
    secondDiv : {
        backgroundColor:'#F06124',
        flexDirection:'row',
        paddingHorizontal:5,
        
    },
    textInputAlt : {
        borderColor: '#dd4b39',
      },
      datePickerCss : {
        width: '100%',
        borderRadius:5,
        borderColor: 'gray',
      },containerStyle :{
        borderColor:'gray',
        borderWidth:1,
        borderRadius: 5,
        backgroundColor:'#fff'
       
      },	
      itemTextStyle : {
       paddingHorizontal:5,
      },
      textContainers: {
        flex: 1,
        backgroundColor: 'transparent',
        
        
        },labelText : {
          color:'#000',fontSize:13,fontWeight:'bold',paddingVertical:4
        },
        listMainDiv:  {height:400,borderBottomRightRadius:6,borderBottomLeftRadius:6,overflow:'hidden',},
        '@media (max-width: 320)': {
          listMainDiv:  {height:300},
     
          
        }
    });
    EStyleSheet.build();
    export default styles;