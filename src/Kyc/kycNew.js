import React, { Component } from 'react';
import {
  Platform,Text,View,Image,TextInput,ScrollView,Modal,ActivityIndicator,AsyncStorage,Keyboard,Alert,FlatList
} from 'react-native';
import{Thumbnail,Toast} from 'native-base';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
import DatePicker from 'react-native-datepicker';
import EStyleSheet from 'react-native-extended-stylesheet';
import RNPickerSelect from 'react-native-picker-select';
import PhotoUpload from 'react-native-photo-upload'
import SoapRequest from 'react-native-soap-request';
import authData from '../../headerData';
import { parseString  } from 'react-native-xml2js';
//import SoapRequest from '../xmlCode';
export default class KYC extends Component {
    constructor(){
    super();

    this.state ={
      documentType : '',
      visible: false ,
      isPersonal : true,
      document : [{"label":"Passport","value":"Passport Number"},{"label":"Driving License","value":"Driving License Number"}],
      gender :[{"label":"Male","value":"Male"},{"label":"Female","value":"Female"},{"label":"Other","value":"value"}],
      title :  [{"label":"Mr","value":"Mr"},{"label":"Mrs","value":"Mrs"},{"label":"Miss","value":"Miss"},{"label":"Ms","value":"Ms"},{"label":"Other","value":"Other"}],
      titleValue:'',
      genderValue:'',
      foreName:'',
      middleName:'',
      surName:'',
      dob:'',
      docNumber:'',
      expiryDate:'',
      senderCountry :[],
      country:'',
      firstNameError :false,
      isError : false,
      errorMsg : '', 
      surNameError : false,
      isResult : false,
      MatchData : [],
      uploadImg : '',
      uploadImgName : ''
    } 
  }
  componentDidMount() {
  this.getSenderCountry();
 }
 async getSenderCountry(){
  var authDataToken =authData();
  this.setState({visible:true});
       try
       { 
           let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/countries', 
           { 
               method: 'GET',
                   headers: 
                   { 
                       'Accept': 'application/json', 
                       'Content-Type': 'application/json', 
                       'Authorization' : authDataToken,
                       
                       }
               }); 
               let res = await response.text(); 
                   // console.log(res);
                   let result= JSON.parse(res); 
                   var jsonResult =  result.Status[0];
                   if(jsonResult.errorCode == 1000){
                     this.setState({visible:false});
                           var country = result.CountriesInfo;
                           var res = [];
                           for(var i=0;i<country.length;i++){ 
                               res.push({value : country[i].Value,label :country[i].Value });
                           }  
                           this.setState({senderCountry : res});
                   }else{
                     this.setState({visible:false});
                     let errors = res;
                     throw errors;
                   }
   }catch (errors) { 

     this.setState({visible:false});
                    console.log('catch');
                    console.log(errors);
                  }
}
 pressNav(){
    Keyboard.dismiss();
    this.props.navigation.navigate("DrawerOpen");
  }
  submitFun(){
   // this.uploadDocument();
     var title=this.state.titleValue;
     var foreName = this.state.foreName;
     var middleName = this.state.middleName;
     var surName = this.state.surName;
     var dob = this.state.dob;
     var dobArr = dob.split('-');
     var dobDay = dobArr[0];
     var dobMonth = dobArr[1];
     var dobYear = dobArr[2];
     var documentType = this.state.documentType;
     var docNumber = this.state.docNumber;
     var expiryDate = this.state.expiryDate;
     var expiryDateArr = expiryDate.split('-');
     var expiryDay = expiryDateArr[0];
     var expiryMonth = expiryDateArr[1];
     var expiryYear = expiryDateArr[2];
     var country = this.state.country;
     var gender = this.state.genderValue;
     var uploadImgName = this.state.uploadImgName;
    if(documentType==''){
      this.setState({isError : true,errorMsg : 'Please select Document Type',surNameError:false,firstNameError:false});
         
    }else if(docNumber==''){
      this.setState({isError : true,errorMsg : 'Document number must be required!',surNameError:false,firstNameError:false});
   
    }else if(docNumber.length<=10){
      this.setState({isError : true,errorMsg : 'Please Enter Full Document Number!',surNameError:false,firstNameError:false});
   
    }else if(expiryDate==''){
      this.setState({isError : true,errorMsg : 'Date of expiry must be required!',surNameError:false,firstNameError:false});
      
    
    }else if(country==''){
      this.setState({isError : true,errorMsg : 'Country must be required!',surNameError:false,firstNameError:false});
      
    }else{
      this.setState({isError : false,errorMsg : '',surNameError:false,firstNameError:false});
      if(documentType){
        if(documentType=='Passport Number'){
           this.passportAuth(title,foreName,middleName,surName,gender,dobDay,dobMonth,dobYear,docNumber,expiryDay,expiryMonth,expiryYear,country);
        }else{
           this.licenseAuth(title,foreName,middleName,surName,gender,dobDay,dobMonth,dobYear,docNumber,expiryDay,expiryMonth,expiryYear,country);
        }
      }
    }                   
  }
  continueFun(){
    var foreName = this.state.foreName;
    var gender = this.state.genderValue;
    var surName = this.state.surName;
    var dob = this.state.dob;
    if(foreName==''){
   this.setState({isError : true,errorMsg : 'First Name must be required!',firstNameError:true});
    }else if(surName==''){
   this.setState({isError : true,errorMsg : 'SurName must be required!',surNameError:true,firstNameError:false});
   
    }else if(gender==''){
      this.setState({isError : true,errorMsg : 'Gender must be required!',surNameError:false,firstNameError:false});

    }else if(dob==''){
      this.setState({isError : true,errorMsg : 'Date of birth must be required!',surNameError:false,firstNameError:false});

    }else{
      this.setState({isError : false,errorMsg : '',surNameError:false,firstNameError:false});
      this.setState({isPersonal : !this.state.isPersonal});
    }
    
  }
  async passportAuth(title,foreName,middleName,surName,gender,dobDay,dobMonth,dobYear,docNumber,expiryDay,expiryMonth,expiryYear,country){
  //alert(title+':'+foreName+':'+middleName+':'+surName+':'+gender+':'+dobDay+':'+dobMonth+':'+dobYear+':'+docNumber+':'+expiryDay+':'+expiryMonth+':'+expiryYear+':'+country);
    // this.setState({visible:true});
   /* var id1ExpiryDate = expiryYear+'-'+expiryMonth+'-'+expiryDay;
    var dateOfBirth = dobYear+'-'+dobMonth+'-'+dobDay; */
    const soapRequest = new SoapRequest({
       security: {
         username: 'mbarreto@bfxaustralia.com.au',
         password: 'BFXaustralia15471000**'
       },
       targetNamespace: 'http://www.id3global.com/ID3gWS/2013/04',
       commonTypes: '',
       requestURL: 'https://pilot.id3global.com/ID3gWS/ID3global.svc/Soap11_Auth',
       SOAPAction : 'http://www.id3global.com/ID3gWS/2013/04/IGlobalAuthenticate/AuthenticateSP'
     });
     const xmlRequest = soapRequest.createRequest({
      /* 'ns:CheckCredentials': {   
               'ns:AccountName': 'mbarreto@bfxaustralia.com.au',
               'ns:Password': 'BFXaustralia15471000**' 
       }*/
       'ns:AuthenticateSP' : {
         'ns:ProfileIDVersion' : {
            'ns:ID' : '2395d852-224b-42bc-9877-5c1b864ea2e1',
            'ns:Version' : 0
         },
         'ns:CustomerReference' : {
          },
         'ns:InputData' : {
           'ns:Personal' : {
             'ns:PersonalDetails' : {
               'ns:Title' : title,
               'ns:Forename' : foreName,
               'ns:MiddleName' : middleName,
               'ns:Surname' : surName,
               'ns:Gender' : gender,
               'ns:DOBDay' : dobDay,
               'ns:DOBMonth' :dobMonth,
               'ns:DOBYear' : dobYear
             }
           },
           'ns:IdentityDocuments' : {
              'ns:InternationalPassport' : {
                 'ns:Number' : docNumber,
                 'ns:ExpiryDay' : expiryDay,
                 'ns:ExpiryMonth':expiryMonth,
                 'ns:ExpiryYear' : expiryYear,
                 'ns:CountryOfOrigin':country
              }
           }
         }
      }
     });
   //  alert(await soapRequest.sendRequest());
   try {
    let response = await fetch('https://pilot.id3global.com/ID3gWS/ID3global.svc/Soap11_Auth', {
        method: 'POST',
        headers: {
          'Accept': 'text/xml',
          'Content-Type': 'text/xml',
          'SOAPAction' : 'http://www.id3global.com/ID3gWS/2013/04/IGlobalAuthenticate/AuthenticateSP'
        },
        body: xmlRequest
        });
    alert(xmlRequest);
    var xmlResponse = await response.text();
    console.log('xmlResponse', xmlResponse);
    
    // Beware this relies on sync callback behaviour which apparently could change in future versions of react-native-xml2js
    parseString(xmlResponse, (err, result) => {
      if (err) {
        alert(err);
        throw (err);
      }
      alert(result)
      responseDoc = result;
      var tracksArray = result['s:Envelope']['s:Body'][0].AuthenticateSPResponse[0].AuthenticateSPResult[0].ResultCodes[0].GlobalItemCheckResultCodes[0];
      alert(tracksArray);
     // alert(tracksArray['Alert'][0]);
      if(tracksArray['Alert'][0]=='Nomatch'){
        console.warn('Mismatch');
        //alert('No match Please try again!');
        global.setTimeout(() => {
         Alert.alert(
           'Warning',
           'Passport Didnot matched Please try again!',
           [
           {text : 'OK', onPress:()=>{console.log('test')}}
           ]
          );
       }, 200);
       if(tracksArray['Mismatch'][0].GlobalItemCheckResultCode){
         var GlobalItemCheckResultCode = tracksArray['Mismatch'][0].GlobalItemCheckResultCode;
         console.log(GlobalItemCheckResultCode);
        // GlobalItemCheckResultCode[0].Code[0]=='1021'
      //  this.setState({isResult : true})
        //this.setState({MatchData : GlobalItemCheckResultCode})
        }
      }else if(tracksArray['Alert'][0]=='Match'){
        console.log('success');
        if(tracksArray['Match'][0].GlobalItemCheckResultCode){
         var GlobalItemCheckResultCode = tracksArray['Match'][0].GlobalItemCheckResultCode;
         console.log(GlobalItemCheckResultCode);
        // GlobalItemCheckResultCode[0].Code[0]=='1021'
       var len =  GlobalItemCheckResultCode.length;
       if(len>4){
         this.setState({isResult : true})
         this.setState({MatchData : GlobalItemCheckResultCode})
         //this.updateProfile(dateOfBirth,id1ExpiryDate,docNumber,'Passport');
        //this.uploadDocument();
       }else{
         var GlobalItemCheckResultCode = tracksArray['Mismatch'][0].GlobalItemCheckResultCode;
         this.setState({isResult : true})
         this.setState({MatchData : GlobalItemCheckResultCode})
         console.log(GlobalItemCheckResultCode);
       }
       console.log('lenght'+len);
        
        }
      
 
      }else{
             alert('Else Not Working......')
      }
    });
  
 //   console.log(this.responseDoc);
    return responseDoc;

  } catch(error) {
    alert(error);
  }
 }
 async licenseAuth(title,foreName,middleName,surName,gender,dobDay,dobMonth,dobYear,docNumber,expiryDay,expiryMonth,expiryYear,country){
  this.setState({visible:true});
  const soapRequest = new SoapRequest({
     security: {
       username: 'mbarreto@bfxaustralia.com.au',
       password: 'BFXaustralia15471000**'
     },
     targetNamespace: 'http://www.id3global.com/ID3gWS/2013/04',
     commonTypes: '',
     requestURL: 'https://pilot.id3global.com/ID3gWS/ID3global.svc/Soap11_Auth',
     SOAPAction : 'http://www.id3global.com/ID3gWS/2013/04/IGlobalAuthenticate/AuthenticateSP'
   });
   const xmlRequest = soapRequest.createRequest({
    /* 'ns:CheckCredentials': {   
             'ns:AccountName': 'mbarreto@bfxaustralia.com.au',
             'ns:Password': 'BFXaustralia15471000**' 
     }*/
     'ns:AuthenticateSP' : {
       'ns:ProfileIDVersion' : {
          'ns:ID' : 'c4b52474-f072-41c4-a434-23687164d4d1',
          'ns:Version' : 0
       },
       'ns:CustomerReference' : {
        },
       'ns:InputData' : {
         'ns:Personal' : {
           'ns:PersonalDetails' : {
             'ns:Title' : title,
             'ns:Forename' : foreName,
             'ns:MiddleName' : middleName,
             'ns:Surname' : surName,
             'ns:Gender' : gender,
             'ns:DOBDay' : dobDay,
             'ns:DOBMonth' :dobMonth,
             'ns:DOBYear' : dobYear
           }
         },
         'ns:IdentityDocuments' : {
            'ns:DrivingLicence' : {
               'ns:Number' : docNumber,
               
            }
         }
       }
    }
   });
   
   const response = await soapRequest.sendRequest();
   console.log(response);
   var tracksArray = response['s:Envelope']['s:Body'][0].AuthenticateSPResponse[0].AuthenticateSPResult[0].ResultCodes[0].GlobalItemCheckResultCodes[0]
   ;
   console.log(tracksArray);
  
}
async updateProfile(dateOfBirth,id1ExpiryDate,customerID1No,customerIDType){
  this.setState({visible:true});
  console.log(dateOfBirth,id1ExpiryDate,customerID1No,customerIDType);
  var authDataToken =authData();
  try
  { 
      let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/update', 
      { 
          method: 'POST',
              headers: 
              { 
                  'Accept': 'application/json', 
                  'Authorization' : authDataToken,
                  'Content-Type': 'application/json', 
                  },
                  body : JSON.stringify({
                    "customerKey":global.CustomerKey
                    ,"dateOfBirth" : dateOfBirth
                   ,"id1ExpiryDate":id1ExpiryDate
                   ,"customerID1No":customerID1No
                   ,"customerIDType":customerIDType
                  
                  })                 
                 }); 
          let res = await response.text(); 
          console.log(res);
          this.setState({visible:false});
              let result= JSON.parse(res); 
              var jsonResult =  result.Status[0];
              if(jsonResult.errorCode == 1000){
                      
              }else{
               let error = await response;
               throw error;
              }
}catch (error) { 

            
               console.log(error);
            
             }
}
async uploadDocument(){
 
  var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/document/customer/upload', 
             { 
                 method: 'POST',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Authorization' : authDataToken,
                         'Content-Type': 'application/json', 
                         },
                         body : JSON.stringify({
                          "customerKey":global.CustomerKey
                          ,"documentName":this.state.uploadImgName
                          ,"documentDescription":"Customer's Document"
                          ,"documentBytesAsBase64String":this.state.uploadImg
                          ,"documentTypeKey": 2
                          ,"documentTypeName": this.state.documentType
                        
                         })                 
                        }); 
                 let res = await response.text(); 
                
                     console.log(res);
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                      global.setTimeout(() => {
                        Alert.alert(
                          'Success',
                          'Thanks for Completing Kyc!',
                          [
                          {text : 'OK', onPress:()=>{console.log('test')}}
                          ]
                         );
                      }, 200);
                     }else{
                      let error = await response;
                      throw error;
                     }
     }catch (error) { 
    
                      console.log('catch'+global.CustomerKey);
                      console.log(error);
                    
                    }
}
  render() {
  
    const { navigate } = this.props.navigation;
   return (
     <View
       style={styles.container}>
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
  
   <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.isResult}
      >
      <View style={{flex: 1,
    alignItems: 'center',
   paddingTop:100,
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#eee',
    height: 280,
    width: 300,
    borderRadius: 3,
   
    flexDirection:'column'
  }}> 
  <View style={{flexDirection:'row',borderBottomColor:'#fff',borderBottomWidth:1,paddingVertical:10,backgroundColor:'#F06124'}}>
   <Text style={{textAlign:'center',fontSize:16,flex:1,color:'#fff',fontWeight:'bold'}}>KYC Verification</Text>
   </View>
   <View style={{paddingVertical:25,paddingHorizontal:10}}>
   <ScrollView style={{height:130}}>
   <FlatList 
                      data={this.state.MatchData}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>  
<View>
       <Text> {item.Description[0]}</Text>
</View>
                    }
                    />
            
   </ScrollView>
   <View style={{flexDirection:'row',paddingHorizontal:10,paddingTop:25}}>
   <Text style={styles.continueBtn} onPress={()=>navigate('FeeCalculator')}>Continue</Text>
       <Text style={styles.skipBtn} onPress={()=>this.setState({isResult:!this.state.isResult})}>Skip</Text>
   </View>
   </View>
   
   </View>
   </View>
   </Modal>
       <View
       >
        {/* <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />*/}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center',}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 20,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>KYC Verification</Text>
            </View>
            {/*<View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Home')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
            */}
        </View> 
        <View style={{height:40,backgroundColor:'#094F66',}}>
        {this.state.isPersonal==true && <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Personal Information</Text>} 
            {this.state.isPersonal==false && <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Identity Document</Text>} 
       </View>
        <ScrollView style = {styles.scrollViewStyle}>
        {this.state.isError && <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
         <Text style={styles.errorText}>{this.state.errorMsg}</Text>
        </View>}
{this.state.isPersonal==true && <View style={{flex:1,flexDirection:'row',paddingHorizontal : 5,borderColor:'#ccc',paddingVertical:10,flexDirection:'column'}}>
{/*<PhotoUpload
   onPhotoSelect={avatar => {
     if (avatar) {
       console.log('Image base64 string: ', avatar)
     }
   }}
 >
   <Image
     style={{
       paddingVertical: 30,
       width: 150,
       height: 150,
       borderRadius: 75
     }}
     resizeMode='cover'
     source={{
       uri: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
     }}
   />
 </PhotoUpload> */}
 
<View style={styles.DropdownDiv}>
              <RNPickerSelect    
                    placeholder={{
                        label: 'Select Title',
                        value: null,
                    }}       
                      items={this.state.title}
                      onValueChange={
                          (item) => {
                            console.log(item);
                            this.setState({titleValue : item})
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.titleValue}
                    
                    />
</View>
<View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
      <View style={{flex:1}}>
                  <TextInput
                        placeholder="First Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
                        underlineColorAndroid = "transparent" returnKeyType="next" 
                        placeholderTextColor='#e1e1e1'
                        keyboardType="default"
                        autoCapitalize="none"
                        autoCorrect={false}
                        onSubmitEditing={()=>this.middleNameInput.focus()}
                        onChangeText={(text) => this.setState({foreName:text})}
                        value={this.state.foreName}
                ></TextInput>
       </View>
        <Text style={styles.stickDiv}>*</Text>
</View>
<View style={{paddingVertical:10,flex:1,flexDirection:'row',paddingLeft:25,paddingRight:38}}>
         <View style={{flex:1}}>
                <TextInput
                      placeholder="Middle Name" style={[styles.InputText,this.state.middleNameError&&styles.errorInput]} 
                      underlineColorAndroid = "transparent" returnKeyType="next" 
                      placeholderTextColor='#e1e1e1'
                      keyboardType="default"
                      autoCapitalize="none"
                      autoCorrect={false}
                      onSubmitEditing={()=>this.surNameInput.focus()}
                      onChangeText={(text) => this.setState({middleName:text})}
                      value={this.state.middleName}
                      ref={(input)=>this.middleNameInput=input}
              ></TextInput>
        </View>
       
</View>
<View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
         <View style={{flex:1}}>
              <TextInput
                    placeholder="Surname" style={[styles.InputText,this.state.surNameError&&styles.errorInput]} 
                    underlineColorAndroid = "transparent" returnKeyType="next" 
                    placeholderTextColor='#e1e1e1'
                    keyboardType="default"
                    autoCapitalize="none"
                    autoCorrect={false}
                    ref={(input)=>this.surNameInput=input}
                    onChangeText={(text) => this.setState({surName:text})}
                    value={this.state.surName}
                   
              ></TextInput>
        </View>
        <Text style={styles.stickDiv}>*</Text>
</View>
   
 <View style={styles.DropdownDivSec}>
         <View style={{flex:1}}>
              <RNPickerSelect    
                    placeholder={{
                        label: 'Select Gender',
                        value: null,
                    }}       
                      items={this.state.gender}
                      onValueChange={
                          (item) => {
                            console.log(item);
                            this.setState({genderValue : item})
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.genderValue}
                    
                    />
          </View>
          <Text style={styles.stickDiv}>*</Text>
   </View>
  <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
                <View style={{flex:1}}>
                        <DatePicker
                                  style ={{width: '100%',borderRadius:5,borderColor: '#fff',backgroundColor:'#fff',}}
                                  date={this.state.dob}
                                  mode="date"
                                  format="DD-MM-YYYY"
                                  maxDate="31-12-2046"
                                  mode="date"
                                  placeholder="Date of Birth"
                                  showIcon={true}
                                  ref={(input)=>this.dateInput=input}
                                  confirmBtnText="Confirm"
                                  cancelBtnText="Cancel"
                                  customStyles={{
                                  dateInput: {
                                  alignItems : 'flex-start',
                                  padding:7,borderRadius:5,  borderColor: '#fff', 
                                  },
                                }}
                                onDateChange={(date_in) => {this.setState({dob: date_in});}}/>
            </View>
        <Text style={styles.stickDiv}>*</Text>
   </View>
        
   <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row',paddingTop:'20%'}}>
            <Text style={styles.next}  onPress={()=>this.continueFun()}>Continue </Text>
            <Text style={styles.cancelBtn} onPress={()=>navigate('FeeCalculator')}>Cancel </Text>
    </View>
</View>}
{this.state.isPersonal==false && <View style={{flex:1,flexDirection:'row',paddingHorizontal : 5,borderColor:'#ccc',paddingVertical:10,flexDirection:'column'}}>
 <View style={styles.DropdownDivSec}>
           <View style={{flex:1}}> 
             <RNPickerSelect    
                    placeholder={{
                        label: 'Select Document',
                        value: null,
                    }}       
                      items={this.state.document}
                      onValueChange={
                          (item) => {
                            console.log(item);
                            if(item){
                              this.setState({documentType : item})
                            }else{
                              this.setState({documentType : ''})
                            }
                            
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.documentType}
              />
            </View>
          <Text style={styles.stickDiv}>*</Text> 
         </View>
<View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
        <View style={{flex:1}}>    
              <TextInput
                      placeholder={`${this.state.documentType}`} style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
                      underlineColorAndroid = "transparent" returnKeyType="next" 
                      placeholderTextColor='#e1e1e1'
                      keyboardType="default"
                      autoCapitalize="none"
                      autoCorrect={false}
                    
                      onChangeText={(text) => this.setState({docNumber:text})}
                      value={this.state.docNumber}
              ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text> 
 </View>
<View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>    
                  <DatePicker
                          style ={{width: '100%',borderRadius:5,borderColor: '#fff',backgroundColor:'#fff',}}
                          date={this.state.expiryDate}
                          mode="date"
                          format="DD-MM-YYYY"
                       
                          maxDate="31-12-2046"
                          mode="date"
                          placeholder="Expiry Date"
                          showIcon={true}
                          ref={(input)=>this.dateInput=input}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                          dateInput: {
                              alignItems : 'flex-start',
                              padding:7,borderRadius:5,  borderColor: '#fff', 
                          },
                        }}
                        onDateChange={(date_in) => {this.setState({expiryDate: date_in});}}/>
            </View>
            <Text style={styles.stickDiv}>*</Text>  
</View>
<View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'}}>
 <View style={{flex:1,backgroundColor:'#fff',paddingVertical:10,borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}>
 <PhotoUpload
  
   onPhotoSelect={avatar => {
    console.log('Image base64 string: ', avatar)
     if (avatar) {
       this.setState({uploadImg : avatar});
       
     }
   }}
  
   onResponse = {response=>{
     //console.log(response.fileName);
     this.setState({uploadImgName : response.fileName});
    // this.uploadDocument();
   }}
  
 >
 
 <Text style={{color:'#484848',textAlign:'left',width:'100%',flex:0.5}}>Upload Document</Text>
 <Text style={{color:'#484848',width:'100%',flex:0.5}}>{this.state.uploadImgName}</Text>

 </PhotoUpload>

 </View>
        <Text style={styles.stickDiv}>*</Text>
 </View>
<View style={styles.DropdownDivSec}>
              <View style={{flex:1}}>  
                    <RNPickerSelect
                                placeholder={{
                                  label: 'Select country ',
                                  value: '',
                                }}
                              items={this.state.senderCountry}
                              onValueChange={
                              (item) => {
                                this.setState({
                                  country: item,
                                  
                                });
                                console.log(item);
                              
                              }
                            }
                              
                              style={{ ...pickerSelectStyles }}
                              value={this.state.country}
                    
                    />
            </View>
            <Text style={styles.stickDiv}>*</Text> 
</View>
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
                <Text style={styles.nextButton} onPress={()=>this.submitFun() }>Continue </Text>
         </View> */}

         <View style={{paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row',paddingTop:'20%'}}>
            <Text style={styles.next}  onPress={()=>this.setState({isPersonal : !this.state.isPersonal})}>Back </Text>
            <Text style={styles.cancelBtn} onPress={()=>this.submitFun() }>Continue </Text>
    </View>
</View>}       
       </ScrollView>
       
       </View>
     </View>
   );
 }
}
const styles = EStyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
  marginTop:20,
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:25},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
nextButton : {
paddingRight:5,
backgroundColor:'#F06124',
borderRadius: 4,
paddingVertical : 20,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
 
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },stickDiv : {
    color:'red',fontSize:22,paddingLeft:5
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 2,
  },
  inputDiv : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:20},
  flagImage : {height:40,width:40, borderRadius: 40/2,},
  sendDiv : {paddingHorizontal : 25,paddingTop:'20%',},
  DropdownDiv : {paddingLeft : 25,paddingRight:38,paddingVertical:10,},
  DropdownDivSec : {paddingHorizontal : 25,paddingVertical:10,flex:1,flexDirection:'row'},
  imageDiv : {flex:0.3,paddingVertical:10,paddingHorizontal:10},
  next : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 15,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
},
cancelBtn : {
    flex:0.5,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 15,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
    marginHorizontal:4
},
skipBtn : {flex:0.5,backgroundColor:'#F06124',textAlign:'center',paddingVertical:10,borderRadius:5,overflow:'hidden',marginLeft:5,color:'#fff'},
  continueBtn : {flex:0.5,backgroundColor:'#F06124',textAlign:'center',paddingVertical:10,borderRadius:5,overflow:'hidden',color:'#fff'},
 
  '@media (max-width: 320)': {
    sendDiv : {
      paddingHorizontal : 25,
      paddingTop:'20%',
    },
    flagImage : {height:35,width:35, borderRadius: 35/2,},
  imageDiv : {flex:0.3,paddingVertical:10,paddingHorizontal:6},
  
  nextButton : {
marginBottom : 25,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 15,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
    },
    
  }
 
});
EStyleSheet.build();
const pickerSelectStyles = EStyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingTop: 10,
    paddingHorizontal: 5,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    color:'#000',
    backgroundColor:'#fff'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: '#F06124',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  
  }
});