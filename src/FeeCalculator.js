import React, { Component } from 'react';
import {
  Platform,Text,View,Image,TextInput,ScrollView,Modal,ActivityIndicator,AsyncStorage,Keyboard,Alert,SafeAreaView
} from 'react-native';
import{Thumbnail,Toast} from 'native-base';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
import authData from '../headerData';
import EStyleSheet from 'react-native-extended-stylesheet';
import RNPickerSelect from 'react-native-picker-select';
import SoapRequest from 'react-native-soap-request';
const crypto = require('../crypto');
var utf8 = require('utf8');
var Buffer = require('buffer/').Buffer;
import Upload from 'react-native-background-upload'
import Privacy from './Privacy';
import { parseString  } from 'react-native-xml2js';
export default class FeeCalculator extends Component {
    constructor(){
    super();
    global.count = 0;
    this.onChangeText = this.onChangeText.bind(this);
    this.onCountryChange = this.onCountryChange.bind(this);
    this.typographyRef = this.updateRef.bind(this, 'typography');
    this.countryRef = this.updateCountryRef.bind(this, 'typography');
    this.onChangeDeliveryMethode = this.onChangeDeliveryMethode.bind(this);
    this.onChangeDeliveryCurrency = this.onChangeDeliveryCurrency.bind(this);
    this.typographyCurrencyRef = this.updateCuurencyRef.bind(this,'typography');
   this.typographyPayMethodRef = this.typographyPayMethodRef.bind(this,'typography');
   this.onChangePayMethod = this.onChangePayMethod.bind(this);
    this.MethodRef = this.updateMethodRef.bind(this,'typography');
    this.state ={
      accountType : 'Individual',
      visible: false,
      sendFrom : 'Australia - AUD',
      sendTo : 'India',
      deliverIn : 'INR',
      amount : '',
      result : false,
      validAmountErr:false,
      amountCheck:false,
      receivingAmount :"" ,
      exChangeRate : '0.00',
      input : '1000',
      showImg : true,
      sendImg : true,
      senderCountry : [],
     // senderCountry : [{"label":"AUD","value":"AUD"},{"label":"GBP","value":"GBP"},{"label":"EUR","value":"EUR"},{"label":"USD","value":"USD"}],
      flagIcons : [{"name":"AUD","flagIcon":"australia.png"},{"name":"EUR","flagIcon":"united-kingdom.png"},{"name":"CZK","flagIcon":"czech-republic.png"},{"name":"GBP","flagIcon":"united-kingdom.png"},
      {"name":"INR","flagIcon":"tiranga.png"},{"name":"USD","flagIcon":"usa.png"},{"name":"USD","flagIcon":"usa.png"},{"name":"JPY","flagIcon":"japan.png"},{"name":"PLN","flagIcon":"poland.png"},{"name":"BHD","flagIcon":"bhd.jpeg"},
      {"name":"BDT","flagIcon":"bdt.png"},{"name":"VUV","flagIcon":"vav.jpeg"},{"name":"AED","flagIcon":"aed.jpeg"}, {"name":"THB","flagIcon":"thb.png"},
      {"name":"CHF","flagIcon":"chf.png"},{"name":"SEK","flagIcon":"sek.jpeg"}, {"name":"LKR","flagIcon":"lkr.jpeg"},{"name":"SBD","flagIcon":"sbd.jpeg"},
      {"name":"SGD","flagIcon":"sgd.jpeg"},{"name":"SAR","flagIcon":"sar.png"},{"name":"WST","flagIcon":"wst.png"},
      {"name":"PHP","flagIcon":"php.jpeg"}, {"name":"PGK","flagIcon":"pgk.jpeg"}, {"name":"PKR","flagIcon":"pkr.jpeg"},
      {"name":"OMR","flagIcon":"omr.png"}, {"name":"NOK","flagIcon":"nok.jpeg"},{"name":"NZD","flagIcon":"nzd.png"},
      {"name":"MXN","flagIcon":"mxn.jpeg"},{"name":"KWD","flagIcon":"kwd.jpeg"},{"name":"KES","flagIcon":"kes.jpeg"},
      {"name":"JOD","flagIcon":"jod.jpeg"}, {"name":"ILS","flagIcon":"ils.png"},{"name":"IDR","flagIcon":"idr.jpeg"},
      {"name":"HUF","flagIcon":"huf.jpeg"},{"name":"HKD","flagIcon":"hkd.jpeg"},
      {"name":"FJD","flagIcon":"fjd.png"},{"name":"DKK","flagIcon":"dkk.jpeg"},{"name":"CNY","flagIcon":"cny.jpeg"},{"name":"XPF","flagIcon":"xpf.jpeg"},
      {"name":"CAD","flagIcon":"require('./img/cad.png')"}],
      deliveryMethod : [],
      delievryCurrency : [],
      receiverCountry :[],
      payMethod : [{"label":"Send","value":"send"},{"label":"Receive","value":"receive"}],
     // receiverCountry : [ {"label":"BHD","value":"BHD"}, {"label":"BDT","value":"BDT"},{"label":"CAD","value":"CAD"},{"label":"XPF","value":"XPF"},{"label":"AUD","value":"AUD"},{"label":"GBP","value":"GBP"},{"label":"EUR","value":"EUR"},{"label":"USD","value":"USD"},{"label":"INR","value":"INR"},{"label":"DKK","value":"DKK"},],
        sending:"AUD",
        blankMsg:'',
        sendingCurrency : "AUD",
        receive : "EUR",
        receivingCurrency : "EUR",
        conFeeCurrency : 'AUD',
        ConFee : '0.00',
        uri: require('./img/france.png'),
        defaultSenderFlag : require('./img/round-flag.png'),
        calculateRec : null,
        handlingFeeAmount : '0.00',
        deliveryMethodCode : '',
        paymentMethodCode : '',
        isKyc : false,
        isTerms : false,
        isBenifieciary : false
      } 
  }
  componentDidMount() {
  //  this.fetchEvents();
 /// this.getGbg();
  this.getAllSenderCurrency();
   this.getPayeeCurrency(this.state.sendingCurrency);
 //  console.log('global.CountryKey'+global.customerCountryKey);
   //this.getPayMentMethods(global.customerCountryKey);
   //this.getDeliveryMethods(global.customerCountryKey);
    this.loginFun();
   // this.getAllCountry();
   this.getUserDetails();
 
   AsyncStorage.getItem("customerCountryKey").then((value) => {
    if(value){
     global.CountryKey = value;
     console.log('getPayMentMethodsgetPayMentMethodsgetPayMentMethods'+value);
     this.getPayMentMethods(value);
     this.getDeliveryMethods(value);
    }
  }).done();
//   AsyncStorage.getItem("IsApproved").then((value) => {
//     if(value){
//         console.log(value);
      
//       if(value=='0'){
//         alert(value);
//         global.IsApproved = value;
//         console.log(global.IsApproved);
//         this.setState({isKyc:true});
//       }else{
//         alert(value);
//          global.IsApproved = value;
//          this.setState({isKyc:false});
//       }
   
//     }
//  }).done();
 }
  onChangeText(text) {
// this.setState({sendingCurrency : text});
  var imgName = text;
  console.log('textggsgsgggf'+text);
if(text){ 
  this.setState({sendImg : true})
  this.getPayeeCurrency(text);
 if(text=='AUD'){
  this.setState({ receivingCurrency : 'EUR'});
  this.getExcahngeRate(text,'EUR');
  this.setImgPath('EUR');
 }else{
  this.setState({ receivingCurrency : 'AUD'});
  this.getExcahngeRate(text,'AUD');
  this.setImgPath('AUD');
 }
  if(imgName=='AUD'){
  this.setState({defaultSenderFlag: require('./img/round-flag.png'),})
   }else if(imgName=='EUR'){
    this.setState({defaultSenderFlag: require('./img/france.png'),})  
   }
   else if(imgName=='GBP'){
  this.setState({defaultSenderFlag: require('./img/gb.png'),})
  }else if(imgName=='USD'){
    this.setState({defaultSenderFlag: require('./img/usa.png'),})
  }else if(imgName=='PLN'){
    this.setState({defaultSenderFlag: require('./img/portugal.png'),})
  }else if(imgName=='JPY'){
    this.setState({defaultSenderFlag: require('./img/japan.png'),})
  }else if(imgName=='HUF'){
    //this.setState({uri: require('./img/flg/huf.jpeg'),}) 
    this.setState({defaultSenderFlag: require('./img/flg/huf.jpeg'),})
  }else{
  }
}else{
this.setState({sendImg : false})
this.setState({showImg:false});
this.setState({receiverCountry : []});
this.setState({receivingCurrency : ''})
this.setState({HandlingFeeAmount : '0.0',ConFee : '0.0',exChangeRate:'0.0',receivingAmount : '0.0'});
}
 }
 onChangePayMethod(text){
 }
 onCountryChange(text){
  if(text){
    console.log('onchannge call............')
    console.log(text)
    this.setState({showImg:true});
    this.setState({sendTo:text});
    this.setState({receivingCurrency : text});
    var flagIcon = this.state.flagIcons;
    for(i=0;i<flagIcon.length;i++){
      if(flagIcon[i].name==text){
         console.log(flagIcon[i].flagIcon);
        this.setImgPath(text);
       // this.flagImage(text);
      }
    }
    //this.onSubmit();
    this.getExcahngeRate(this.state.sendingCurrency,text);
  }else{
    this.setState({showImg:false});
   // this.setState({receiverCountry : []});
this.setState({receivingCurrency : ''})
this.setState({HandlingFeeAmount : '0.0',ConFee : '0.0',exChangeRate:'0.0',receivingAmount : '0.0'});
  }
 }
 flagImage(flag){
  var flagIcons = this.state.flagIcons;

  for(var j=0 ; j<flagIcons.length; j++){
   // unique_array.push({value : res[i].value,label :res[i].value })
 if(flagIcons[j].name == flag){
   //unique_array.splice(j,1);
 // this.setState({uri: require(`${flagIcons[j].flagIcon}`)})
  console.log(flagIcons[j].flagIcon);
  //var uris = "require('./img/flg/"+flagIcons[j].flagIcon+"')";
 // console.log(uris);
  this.setState({uri: flagIcons[j].flagIcon})
  console.log(this.state.uri);
 }
}
}
 clickContinue(){
  const { navigate } = this.props.navigation;
  var sendingCurrency = this.state.sendingCurrency;
  var receivingCurrency = this.state.receivingCurrency;
  var conFeeCurrency = this.state.ConFee;
  var exChangeRate = this.state.exChangeRate;
  var receivingAmount = this.state.receivingAmount;
  var exactAmount = this.state.input;
  var HandlingFeeAmount = this.state.HandlingFeeAmount;
  if(receivingAmount>0){
    var amountJson = {
      "senderCountry" : sendingCurrency,
      "receiveCountry" : receivingCurrency,
      "exchangeFee" : conFeeCurrency,
      "exchageRate" : exChangeRate,
      "sendingAmount" : exactAmount,
      "beneficiaryAmount" : receivingAmount,
      "HandlingFeeAmount" : HandlingFeeAmount                   
                     }
                     console.log(amountJson);
  navigate('Beneficiary',{amountJson:amountJson});
  }else{
    //alert('your not allowed')
    global.setTimeout(() => {
      Alert.alert(
        'Warning',
'Recieving amout must be greater then 0 .',
        [
        {text : 'OK', }
        ]
       );
    }, 200);
  }

  
}
 setImgPath(imgName){
   console.log(imgName);
 if(imgName=='AUD'){
this.setState({uri: require('./img/round-flag.png'),})
 }else if(imgName=='EUR'){
  this.setState({uri: require('./img/france.png'),})
 }
 else if(imgName=='GBP'){
this.setState({uri: require('./img/gb.png'),}) 
}
else if(imgName=='INR'){
this.setState({uri: require('./img/tiranga.png'),})  
}
else if(imgName=='CZK'){
this.setState({uri: require('./img/czech-republic.png'),})   
}
else if(imgName=='USD'){
this.setState({uri: require('./img/usa.png'),}) 
}
else if(imgName=='JPY'){
this.setState({uri: require('./img/japan.png'),})  
}
else if(imgName=='PLN'){
this.setState({uri: require('./img/portugal.png'),}) 
}
else if(imgName=='BHD'){
  this.setState({uri: require('./img/flg/bhd.jpeg'),}) 
  }
  else if(imgName=='BDT'){
    this.setState({uri: require('./img/flg/bdt.png'),}) 
  }
  else if(imgName=='VUV'){
    this.setState({uri: require('./img/flg/vav.jpeg'),}) 
  }
  else if(imgName=='THB'){
    this.setState({uri: require('./img/flg/thb.jpeg'),}) 
  }
  else if(imgName=='CHF'){
    this.setState({uri: require('./img/flg/chf.png'),}) 
  }
  else if(imgName=='SEK'){
    this.setState({uri: require('./img/flg/sek.jpeg'),}) 
  }
  else if(imgName=='LKR'){
    this.setState({uri: require('./img/flg/lkr.jpeg'),}) 
  }
  else if(imgName=='SBD'){
    this.setState({uri: require('./img/flg/sbd.jpeg'),}) 
  }
  else if(imgName=='SAR'){
    this.setState({uri: require('./img/flg/sar.png'),}) 
  }
  else if(imgName=='WST'){
    this.setState({uri: require('./img/flg/wst.png'),}) 
  }
  else if(imgName=='SGD'){
    this.setState({uri: require('./img/flg/sgd.jpeg'),}) 
  }
  else if(imgName=='PHP'){
    this.setState({uri: require('./img/flg/wst.png'),}) 
  }
  else if(imgName=='PGK'){
    this.setState({uri: require('./img/flg/pgk.jpeg'),}) 
  }
  else if(imgName=='PKR'){
    this.setState({uri: require('./img/flg/pkr.jpeg'),}) 
  }
  else if(imgName=='OMR'){
    this.setState({uri: require('./img/flg/omr.png'),}) 
  }
  else if(imgName=='NOK'){
    this.setState({uri: require('./img/flg/nok.jpeg'),}) 
  }
  else if(imgName=='NZD'){
    this.setState({uri: require('./img/flg/nzd.png'),}) 
  }
  else if(imgName=='MXN'){
    this.setState({uri: require('./img/flg/mxn.jpeg'),}) 
  }
  else if(imgName=='KWD'){
    this.setState({uri: require('./img/flg/kwd.jpeg'),}) 
  }
  else if(imgName=='KES'){
    this.setState({uri: require('./img/flg/kes.jpeg'),}) 
  }
  else if(imgName=='JOD'){
    this.setState({uri: require('./img/flg/jod.jpeg'),}) 
  }
  else if(imgName=='ILS'){
    this.setState({uri: require('./img/flg/ils.png'),}) 
  }
  else if(imgName=='IDR'){
    this.setState({uri: require('./img/flg/idr.jpeg'),}) 
  } else if(imgName=='HUF'){
    this.setState({uri: require('./img/flg/huf.jpeg'),}) 
  } else if(imgName=='HKD'){
    this.setState({uri: require('./img/flg/hkd.jpeg'),}) 
  }
  else if(imgName=='FJD'){
    this.setState({uri: require('./img/flg/fjd.png'),}) 
  }
  else if(imgName=='DKK'){
    this.setState({uri: require('./img/flg/dkk.jpeg'),}) 
  } else if(imgName=='CNY'){
    this.setState({uri: require('./img/flg/cny.jpeg'),}) 
  } else if(imgName=='CAD'){
    this.setState({uri: require('./img/flg/cad.png'),}) 
  }
  else if(imgName=='XPF'){
    this.setState({uri: require('./img/flg/xpf.jpeg'),}) 
  }else{
this.setState({uri: require('./img/Australia.png'),}) 
}
console.log(this.state.uri)
}
 updateRef(name, ref) {
   this[name] = ref;
 }
 updateCuurencyRef(name, ref) {
  this[name] = ref;
}
typographyPayMethodRef(name,ref){
  this[name]=ref;
}
 onChangeDeliveryMethode(text){
  this.setState({deliveryMethodSelect:text});
 }
 onChangeDeliveryCurrency(text) {
  this.setState({deliverIn:text});
 }
 updateMethodRef(name, ref) {
  this[name] = ref;
}
 updateCountryRef(name, ref) {
  this[name] = ref;
}
 onSubmit(){
   let amt = this.state.input;
   var validAmount = false;
    var validPass = false;
   if(amt){
    this.loginFun();
    this.setState({validAmountErr : false}) 
   }else{
    this.setState({validAmountErr : true})    
    this.setState({error: 'Amount must be required'});
   }
  }
  onBlurFun(){
    
  }
  taskPeopleCheck = (text) => {
    if(text<0){
      this.setState({error: 'Amount must be greater than 0'});
      this.setState({validAmountErr : true})
    }else if(text==""){
      this.setState({validAmountErr : true})    
        this.setState({error: 'Amount must be required'});
        }else{
            this.setState({amountCheck : true});
      this.setState({validAmountErr : false})
      this.setState({error: ''});
      this.setState({amount:text})
    }
  }
  async getPayMentMethods(countryKey){
    var authDataToken = authData();
   console.log('getPayMentMethodsFUN'+CountryKey)
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/paymentmethods/'+countryKey, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,  
                    }
            }); 
            let res = await response.json(); 
                 console.log(res);
                 var jsonResult = res.Status[0];
                 if(jsonResult.errorCode == 1000){
                  var PaymentMethodsInfo = res.PaymentMethodsInfo[0];
                 console.log(PaymentMethodsInfo.PMCode);
                 this.setState({paymentMethodCode : PaymentMethodsInfo.PMCode})
                  }else{
                   let errors = await response;
                   throw errors;
                  }                         
  }catch (errors) { 
                 console.log(JSON.stringify(errors));
                 console.log(authDataToken);
               }
  }
  async getDeliveryMethods(countryKey){
    var authDataToken = authData();
    console.log('getDeliveryMethodsFUN'+CountryKey)
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/deliverymethods/'+countryKey, 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.json(); 
                 console.log(res);
                 var jsonResult = res.Status[0];
                 if(jsonResult.errorCode == 1000){
                  var PaymentMethodsInfo = res.DeliveryMethodsInfo[0];
                 console.log(PaymentMethodsInfo.DMCode);
                 this.setState({deliveryMethodCode : PaymentMethodsInfo.DMCode,
})
                  }else{
                   let errors = await response;
                   throw errors;
                  }                         
  }catch (errors) { 
                 console.log(JSON.stringify(errors));
                 console.log(authDataToken);
              
              
               }
  }
  async getAllCountry(){
    var authDataToken =authData();
   
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/countries', 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.json(); 
             //    console.log(res);
                 var jsonResult = res.Status[0];
                 if(jsonResult.errorCode == 1000){
                  var CurrenciesInfo = res.CountriesInfo;
                  var country = res.CountriesInfo;
                           var resArr = [];
                           for(var i=0;i<country.length;i++){ 
                               resArr.push({value : country[i].Key,label :country[i].Value });
                           }  
                        //  this.setState({senderCountry : resArr});
                        console.log(resArr);
                  }else{
                   let errors = await response;
                   throw errors;
                  }                         
  }catch (errors) { 
                 console.log(JSON.stringify(errors));
                 console.log(authDataToken);
              /*   global.setTimeout(() => {
                  Alert.alert(
                    'Warning',
      'Something Went  wrong while fetch data from sending currency! Contact to support team',
                    [
                    {text : 'OK', onPress:()=>{this.goBaneficiary()}}
                    ]
                   );
                }, 200);*/
              
               }
  }
  async getAllSenderCurrency(){
    var authDataToken =authData();
  //this.setState({ sendingCurrency : "AUD"});
  console.log(this.state.sendingCurrency);
   console.log('getAllSenderCurrency.....'+authDataToken);
      try
      { 
          let response = await fetch('http://api.remitanywhere.com/restapi/lists/currency/sending', 
          { 
              method: 'GET',
                  headers: 
                  { 
                      'Accept': 'application/json', 
                      'Content-Type': 'application/json', 
                      'Authorization' : authDataToken,
                      
                      }
              }); 
              let res = await response.json(); 
                 console.log(res);
                   var jsonResult = res.Status[0];
                   if(jsonResult.errorCode == 1000){
                    var CurrenciesInfo = res.CurrenciesInfo;
                    var country = res.CurrenciesInfo;
                             var resArr = [];
                             for(var i=0;i<country.length;i++){ 
                                 resArr.push({value : country[i].Key,label :country[i].Key });
                             }  
                            this.setState({senderCountry : resArr});
                            console.log(this.state.senderCountry);
                    }else{
                     let errors = await response.text();
                     throw errors;
                    }                         
    }catch (errors) { 
                   console.log(errors);
                   console.log(authDataToken);
               
                 }
                 
   }
   async fetchEvents() {
    var authDataToken =authData();

    fetch('http://api.remitanywhere.com/restapi/lists/currency/sending', {
      method: 'GET',
      headers: {
        'Accept': 'application/json', 
        'Content-Type': 'application/json', 
        'Authorization' : authDataToken,
      }
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
         // var CurrenciesInfo = responseJson.CurrenciesInfo;
       //   responseJson= JSON.parse(responseJson); 
          var country = responseJson.CurrenciesInfo;
               console.log(country);
                   var resArr = [];
                   for(var i=0;i<country.length;i++){ 
                       resArr.push({value : country[i].Key,label :country[i].Key });
                   }  
                  this.setState({senderCountry : resArr,sendingCurrency:'AUD'});
                  console.log(this.state.senderCountry);

          return responseJson;
        })
        .catch((error) => {
          console.error(error);
        }); 
  }
    uniqurArrayFun(res){
      let unique_array = []
      for(let i = 0;i < res.length; i++){
          if(unique_array.indexOf(res[i].CurrencyCode) == -1){
              unique_array.push({value : res[i].CurrencyCode,label :res[i].CurrencyCode })
          }
        }
        return unique_array;
}
  async getPayeeCurrency(sendingCurrency) {
    var authDataToken =authData();
    if(authDataToken==global.authToken){

    }else{
      authDataToken =authData();
    }
    this.setState({visible:true});
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/lists/currency/payout/'+sendingCurrency, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
             
                     console.log(res);
                      var authDataToken =authData();
                     let result= JSON.parse(res); 
                     console.log(result);
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                      this.setState({showImg:true});
                      this.setState({visible:false});
                      var receiverCountryCount = this.state.receiverCountry;
                      var country = result.CurrenciesInfo;
                      //  var country = result.CountriesInfo;
                        
                        var res = [];
                        for(var i=0;i<country.length;i++){ 
                            res.push({value : country[i].Key,label :country[i].Key });
                        }  
                       console.log("receiverCountryCount.length"+receiverCountryCount.length)
                      if(receiverCountryCount.length>0){
                        console.log('if'+country[0].Key);
                       // this.setState({ receivingCurrency : country[0].Key});
                     //  this.getExcahngeRate(sendingCurrency,country[0].Key);
                        this.setState({receiverCountry:[]})
                        this.setState({receiverCountry : res});
                      //  this.setImgPath(country[0].Key);
                        console.log(this.state.receiverCountry);
                        if(sendingCurrency=='AUD'){
                        //  this.setState({defaultSenderFlag: require('./img/france.png'),})  
                             this.setState({ receivingCurrency : 'EUR'});
                             this.setImgPath('EUR');
                        }else{
                          //this.setImgPath('AUD');
                        }
                      }else{
                        console.log('else');
                        this.setState({receiverCountry : res});
                      }
                     
                             /*
                             let unique_array = []
                             for(let i = 0;i < res.length; i++){
                                 if(unique_array.indexOf(res[i].value) == -1){                                  
                                     unique_array.push({value : res[i].value,label :res[i].value })
                                 }
                               }
                               for(var i=0;i<unique_array.length;i++){
                                for(var j=0 ; j<unique_array.length; j++){
                                    // unique_array.push({value : res[i].value,label :res[i].value })
                                  if(unique_array[j].value == unique_array[i].value){
                                    unique_array.splice(j,1);
                                  }
                                }
                              }*/
                          // unique_array.push({value : "INR",label :"INR" })
                        //    this.setState({receiverCountry : unique_array});
                           // console.log(res);
                     }else if(jsonResult.errorCode == 1002){
                     // alert(jsonResult.errorMessage);
                         global.setTimeout(() => {
        Alert.alert(
          'Message',
        'Recieving currency is not available .Please select other sending currency',
          [
          {text : 'OK'}
          ]
         );
      }, 50);
                      this.setState({HandlingFeeAmount : '0.0',ConFee : '0.0',exChangeRate:'0.0',receivingAmount : '0.0',receivingCurrency:''});
                      this.setState({receiverCountry : []});
                      this.setState({showImg:false});
                    }else{
                      let errors = await response.text();
                      throw errors;
                     }
     }catch (errors) { 
                   console.log(errors);
                    console.log('getPayeeCurrency +++++++++++++.'+authDataToken);
                    }
  }
   // Function for Get Exchange Rates
 async loginFun(){                     
    this.setState({visible:true});
    var authDataToken =authData();
  //  console.log('loginFun...'+authDataToken);
    try
    { 
      var sendCurrency = this.state.sendingCurrency;
      console.log('sendCurrency'+sendCurrency);
      var recCurrency = this.state.receivingCurrency;
      console.log('recCurrency'+recCurrency);
      var nextWeek = this.addDays(new Date(), 1);
      let n = nextWeek.getDate();
      n = n < 10 ? "0" + n : n;
      let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
      let datass = nextWeek.getFullYear()+'-'+mm+'-'+n;
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/rate/remittance/select', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,                 
                  },
                  body: JSON.stringify({
                    "sendingCurrency":sendCurrency
                  	,"receivingCurrency":recCurrency
	                  ,"date":datass
                  	,"deliveryMethodKey":3
	                  ,"paymentMethodKey":3
	                  ,"payeeLocationKey":1
                    ,"customerKey":global.CustomerKey
                    ,"byOwnerLocation": 1
                  })
             }); 
             //await response.json();
             let res = await response.json(); 
             console.log(authData());
             console.log(res);
            this.setState({visible:false});
            let result = res;
            var jsonResult =  result.Status['0'];
              if(jsonResult.errorCode == 1000){
                     var customerInfoJson = result.ExRateInfo['0'];
                   this.setState({FeeCurrency:customerInfoJson.CurrencyFrom});                  
                   let fix = customerInfoJson.ExRate;
                   if(typeof(fix)=='string'){
                    fix = parseFloat(fix);
                   }
                   this.setState({exChangeRate:fix.toFixed(6)});
                 let amt = parseInt(this.state.input);
                 var r = amt*fix;
                // var r = amt*fix.toFixed(2);
                 console.log('rece'+r);
                 this.setState({calculateRec : r});
                 r= r.toFixed(2);
                  r = r.toString();
                  console.log('rece after string'+r);
                  this.setState({receivingAmount : r})
               
                  this.getServiceFee();
                  this.getHandlingFee();
                  AsyncStorage.getItem("IsApproved").then((value) => {
                    if(value){
                    
                        console.log(value);
                      
                      if(value=='0'){
                       // alert(value)
                        global.IsApproved = value;
                        this.setState({isKyc:true});
                        console.log(global.IsApproved);
                      
   
                      }else{
                         global.IsApproved = value;
                         this.setState({isKyc:false});
                         this.getBeneficiary();
                      }
                   
                    }
                 }).done();
                
               }else if(jsonResult.errorCode == 1001){
               //  console.log(jsonResult.ErrorMessage);
                 this.setState({ErrorMessageView:true});
                 this.setState({ErrorMessage:jsonResult.ErrorMessage});
               }else{ 
                this.setState({visible:false});
                let errors = await response.text();
                throw errors;
                } 
              } catch (errors) { 
           
      console.log(errors);
      console.log(authDataToken);
      console.log(authData());
   /*    global.setTimeout(() => {
        Alert.alert(
          'Warning',
        'Something Went wrong! Contact to support team',
          [
          {text : 'OK', onPress:()=>{this.goBaneficiary()}}
          ]
         );
      }, 50);*/
   
     }
     
  }

     // Function for Get Exchange Rates
 async getExcahngeRate(sendCurrency,recCurrency){                     
  this.setState({visible:true});
  var authDataToken =authData();
  console.log(authData());
//  console.log('loginFun...'+authDataToken);
  try
  { 
    console.log('sendCurrency'+sendCurrency);
    console.log('recCurrency'+recCurrency);
    let d  = new Date();
    var nextWeek = this.addDays(new Date(), 1);
    let n = nextWeek.getDate();
    n = n < 10 ? "0" + n : n;
    let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
    let datass = nextWeek.getFullYear()+'-'+mm+'-'+n;
    let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/rate/remittance/select', 
      { 
        method: 'POST',
            headers: 
              { 
                 'Accept': 'application/json', 
                 'Content-Type': 'application/json', 
                 'Authorization' : authDataToken,                 
                },
                body: JSON.stringify({
                  "sendingCurrency":sendCurrency
                  ,"receivingCurrency":recCurrency
                  ,"date":datass
                  ,"deliveryMethodKey":3
                  ,"paymentMethodKey":3
                  ,"payeeLocationKey":1
                  ,"customerKey":global.CustomerKey
                  ,"byOwnerLocation": 1
                })
           }); 
           //await response.json();
           let res = await response.json(); 
           console.log(authData());
         console.log(res);
         console.log({"sendingCurrency":sendCurrency
         ,"receivingCurrency":recCurrency
         ,"date":datass
         ,"deliveryMethodKey":3
         ,"paymentMethodKey":3
         ,"payeeLocationKey":1
         ,"customerKey":global.CustomerKey
         ,"byOwnerLocation": 1});
          this.setState({visible:false});
         let result = res;
          var jsonResult =  result.Status['0'];
            if(jsonResult.errorCode == 1000){
                   var customerInfoJson = result.ExRateInfo['0'];
                 this.setState({FeeCurrency:customerInfoJson.CurrencyFrom});                  
                 let fix = customerInfoJson.ExRate;
                
               //  console.log('resfs'+fix);
                // console.log(typeof(fix));
                 //var man = 0.0054585;
                 console.log(typeof(fix));
                 if(typeof(fix)=='string'){
                  fix = parseFloat(fix);
                 }
                 this.setState({exChangeRate:fix.toFixed(6)});
               let amt = parseInt(this.state.input);
               var r = amt*fix;
              // var r = amt*fix.toFixed(2);
               console.log('rece'+r);
               this.setState({calculateRec : r});
               r= r.toFixed(2);
                r = r.toString();
                console.log('rece after string'+r);
                this.setState({receivingAmount : r});
                this.getServiceFee();
                this.getHandlingFee();
              /* if(fix>1){
                this.getServiceFee();
                this.getHandlingFee();
               }else{
                 this.setState({conFeeCurrency : this.state.sendingCurrency})

               }*/
                
             }else if(jsonResult.errorCode == 1001){
              console.log(jsonResult.ErrorMessage);
               this.setState({ErrorMessageView:true});
               this.setState({ErrorMessage:jsonResult.ErrorMessage});
             }else{ 
              this.setState({visible:false});
              let errors = result;
              throw errors;
              } 
            } catch (errors) { 
         
    console.log(errors);
    console.log(authDataToken);
   // console.log(authData());
 /*    global.setTimeout(() => {
      Alert.alert(
        'Warning',
      'Something Went wrong! Contact to support team',
        [
        {text : 'OK', onPress:()=>{this.goBaneficiary()}}
        ]
       );
    }, 50);*/
 
   }
   
}
  //Function start for get Service Fee 
  addDays(dateObj, numDays) {
    dateObj.setDate(dateObj.getDate() + numDays);
    return dateObj;
  }
  async getServiceFee(){
   
    var authDataToken =authData();
    if(authDataToken==global.authToken){
    }else{
      authDataToken =authData();
    }
    try
    {       
      let d  = new Date();
      var nextWeek = this.addDays(new Date(), 1);
      let n = nextWeek.getDate();
      n = n < 10 ? "0" + n : n;
      let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
      let datass = nextWeek.getFullYear()+'-'+mm+'-'+n;
      var amount = Number(this.state.input);
      var  receivingAmount = this.state.receivingAmount;
      var deliveryMethodCode = this.state.deliveryMethodCode;
      var paymentMethodCode = this.state.paymentMethodCode;
      console.log('deliveryMethodCode'+deliveryMethodCode);
      console.log('paymentMethodCode'+paymentMethodCode);
     console.log('receivingAmount*****************'+receivingAmount);    
      //var convertedAmt=Number(this.state.receivingAmount);  paymentMethodCode : PaymentMethodsInfo.PMCode,deliveryMethodCode
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/fee/select', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,                  
                  },
                  body: JSON.stringify({
                    "amount": amount 
                    ,"convertedAmount": receivingAmount 
                    ,"currencyFrom": this.state.sendingCurrency
                    ,"currencyTo" : this.state.receivingCurrency
                    ,"SCDate": datass
                    ,"customerKey":  global.CustomerKey             // Extra Field add after modification in api's
                    ,"byOwnerLocation": 1,
                    "DeliveryMethodCode":"DEPOSIT",
                    "PaymentMethodCode":"DEBNK"
                  })
             }); 
             /*
              "amount": 1000
	,"convertedAmount": 12.09
	,"currencyFrom": "JPY"
	,"currencyTo": "AUD"
	,"DeliveryMethodCode": "DEPOSIT"
	,"PaymentMethodCode": "DEBNK"
	,"SCDate": "2018-05-11"
	,"customerKey":  2068
	,"byOwnerLocation": 1
             */
             let res = await response.text(); 
             console.log(res);
            this.setState({visible:false});
             let result= JSON.parse(res); 
             console.log(result);
            var jsonResult =  result.Status['0'];
              if(jsonResult.errorCode == 1000){
                    var serFee = result.ServiceFee[0];
                      //console.log(serFee);
                    //  alert('test');
                    this.setState({conFeeCurrency : serFee.FeeCurrency});
                    this.setState({ConFee:serFee.SCAmount.toFixed(2) });
                       //    console.log(this.state.ConFee);
               }else if(jsonResult.ErrorCode==1001){
                var serFee = result.ServiceFee[0];
                //console.log(serFee);
              //  alert('test');
              this.setState({conFeeCurrency : serFee.FeeCurrency});
              this.setState({ConFee:serFee.SCAmount.toFixed(2) });
               }else{ 
                let errors = result;
                throw errors;
                } 
              } catch (errors) { 
      //this.setState({error: error});
     console.log('fetching service Fee! catch');
   

      console.log(errors);
    /*  global.setTimeout(() => {
        Alert.alert(
          'Warning',
          'Something Went wrong while fetching service Fee! Contact to support team',
          [
          {text : 'OK', onPress:()=>{this.goBaneficiary()}}
          ]
         );
      }, 50); */
      
    // console.log(JSON.parse(errors));
     }
     
  }
  async getGbg(){
   
   let response = await fetch('https://forexbfx.herokuapp.com/gbgAuth', 
   { 
     method: 'POST',
         headers: 
           { 
              'Accept': 'application/json', 
              'Content-Type': 'application/json'
                               
             },
             body: JSON.stringify({
                "title" : "Mr",
                "foreName" : "Gary",
                "middleName":"L",
                "surName":"Milburn",
                "gender":"Male",
                "dobDay":"06",
                "dobMonth":"10",
                "dobYear":"1973",
                "docNumber":"9876543213GBR7310065M080310806",
                "expiryDay":"08",
                "expiryMonth":"03",
                "expiryYear":"2010",
                "country":"United Kingdom"
         })
        });
        let res = await response.text(); 
      // console.log(res);
       parseString(res, (err, result) => {
        if (err) {
          console.log(err);
          throw (err);
        }
       console.log(result);
       var tracksArray = result['s:Envelope']['s:Body'][0].AuthenticateSPResponse[0].AuthenticateSPResult[0].ResultCodes[0].GlobalItemCheckResultCodes[0]
       ;
       console.log(tracksArray);
      });
  }
  async getHandlingFee(){
    var authDataToken =authData();
    try
    {       
      let d  = new Date();
      var nextWeek = this.addDays(new Date(), 1);
      let n = nextWeek.getDate();
      n = n < 10 ? "0" + n : n;
      let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
      let datass = nextWeek.getFullYear()+'-'+mm+'-'+n;
      var amount = Number(this.state.input);
      var  receivingAmount = this.state.receivingAmount;
      console.log('receivingAmount*****************'+receivingAmount);   
      var paymentMethodCode = this.state.paymentMethodCode; 
      //var convertedAmt=Number(this.state.receivingAmount);
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/handlingfee/select', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,                  
                  },
                  body: JSON.stringify({
                          "amount": amount
                          ,"convertedAmount": receivingAmount
                          ,"SCDate": datass
                          ,"customerKey":  global.CustomerKey
                          ,"byOwnerLocation": 1
                          ,"currencyTo":this.state.receivingCurrency
                          ,"currencyFrom":this.state.sendingCurrency
                          ,"DeliveryMethodCode": "DEPOSIT"
                          ,"PaymentMethodCode": paymentMethodCode    
              })
             }); 
             let res = await response.text(); 
           console.log(res);
           var jsson = {
                    "amount": amount
                    ,"convertedAmount": receivingAmount
                    ,"SCDate": datass
                    ,"customerKey":  global.CustomerKey
                    ,"byOwnerLocation": 1
                    ,"currencyTo":this.state.receivingCurrency
                    ,"currencyFrom":this.state.sendingCurrency
                    ,"DeliveryMethodCode": "DEPOSIT"
                    ,"PaymentMethodCode": paymentMethodCode
                        }
           console.log(jsson);
            this.setState({visible:false});
             let result= JSON.parse(res); 
             console.log(result);
            var jsonResult =  result.Status['0'];
              if(jsonResult.errorCode == 1000){
                   console.log(result.HandlingFee);
                  var HandlingFee =  result.HandlingFee[0];
                //console.log(this.state.ConFee);
                if(HandlingFee.HandlingFeeAmount){
                  this.setState({HandlingFeeAmount : HandlingFee.HandlingFeeAmount.toFixed(2)});
                }else{
                  this.setState({HandlingFeeAmount : HandlingFee.HandlingFee.toFixed(2)});
                  
                }
               
                console.log(HandlingFee.HandlingFeeAmount);
               }else{ 
                let errors = await response;
                throw errors;
                } 
              } catch (errors) { 
     console.log('fetching handling  Fee! catch');
     console.log(errors);
     }
     
  }
  async getUserDetails(){
    var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/'+global.CustomerKey, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
                     console.log(authData());
                     let result= JSON.parse(res); 
                     console.log(result);
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                             var CustomerInfo = result.CustomerInfo[0];
                             AsyncStorage.setItem("usersName", CustomerInfo.FirstName+' '+CustomerInfo.LastName);
                           console.log(CustomerInfo);
                                    
                     }else{
                       console.log('main else')
                      let error = await response.text();
                      throw error;
                     }
     }catch (error) { 
                      console.log('catch'+global.CustomerKey);
                      console.log(error);
                    }
  }
  async getBeneficiary(){
    // console.log(sendingCurrency,receivingCurrency,currentDate,ExRate);
    var authDataToken =authData();
    if(authDataToken==global.authToken){
   console.log('if/*/*/*/*/*/*/*/*//*/**/*/////////////////');
    }else{
      console.log('else.....................................')
     authDataToken =authData();
    }
    this.setState({visible:true});
      console.log(global.CustomerKey);
   try
   { 
     let response = await fetch('http://api.remitanywhere.com/restapi/customer/beneficiaries/select', 
       { 
         method: 'POST',
             headers: 
               { 
                  'Accept': 'application/json', 
                  'Content-Type': 'application/json', 
                  'Authorization' : authDataToken,
                 
                 },
                 body : JSON.stringify({
                   "customerKey":global.CustomerKey
                ,"beneWithDMofMobileTopup":0
   
                 })
            }); 
            let res = await response.text(); 
            this.setState({visible:false});
            var d = {
              "customerKey":global.CustomerKey
           ,"beneWithDMofMobileTopup":0

            };
            console.log(d);
          console.log(res);
         
        //  console.log(authDataToken);
        // console.log(authData());
       if(res){
              var result= JSON.parse(res); 
              var jsonResult =  result.Status[0];
              if(jsonResult.errorCode == 1000){
                console.log(result.BeneficiariesInfo);
                if(result.BeneficiariesInfo.length>0){
                   this.setState({isBenifieciary : false});
                   
                }else{
                 this.setState({isBenifieciary : true})
                }
             // var ExRateInfo = result.ExRateInfo[0];
            //  return ExRateInfo;
              }else if(jsonResult.errorCode == 1002){
                this.setState({isBenifieciary : true});
                console.log('errorCode'+jsonResult.errorCode+'isisBenifieciary'+this.state.isBenifieciary)
              }else{
                  console.log(result);
   
               let errors = await response;
               throw errors;
              }
           }else{
              
       
            let errors = await response;
            throw errors;
           }
             
          
             } catch (errors) { 
   
     console.log('catch');
     console.log(errors);
             }
   }
  goBaneficiary(){
  //  console.log('error');
    this.setState({visible:false});
  }
clickSkip(){
  console.log('sdfddsfdsf');
  this.setState({isKyc:false});
}
  pressNav(){
    Keyboard.dismiss();
    
    this.props.navigation.navigate("DrawerOpen",{user : 'abc'});
  }
  changeAmountFun(text){
    if(text>0){
      this.setState({input: text});
     }else{
      this.setState({input: text});
      this.setState({validAmountErr : true})    
      this.setState({error: 'Amount must be required'});
     }
  }
  changeBlurAmountFun(){
    
    var text = this.state.input;
   if(text>0){
     this.onSubmit();
   }else{
    this.setState({input: text});
    this.setState({validAmountErr : true})    
    this.setState({error: 'Amount must be required'});
   }
  }

  render() {
    let { sending,receive } = this.state;
    const { navigate } = this.props.navigation;
   return (
     <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
            <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.isKyc}
      >
      <View style={{flex: 1,
    alignItems: 'center',
   paddingTop:100,
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#eee',
    height: 200,
    width: 300,
    borderRadius: 3,
   
    flexDirection:'column'
  }}> 
  <View style={{flexDirection:'row',borderBottomColor:'#fff',borderBottomWidth:1,paddingVertical:10,backgroundColor:'#F06124'}}>
   <Text style={{textAlign:'center',fontSize:16,flex:1,color:'#fff',fontWeight:'bold'}}>Notification</Text>
   </View>
   <Text style={{paddingVertical:25,paddingHorizontal:10}}>Please complete  your KYC for getting the benefits of transactions</Text>
  <View style={{flexDirection:'row',paddingHorizontal:10,paddingTop:25}}>
  <Text style={styles.continueBtn} onPress={()=>navigate('Kyc')}>Continue</Text>
      <Text style={styles.skipBtn} onPress={()=>this.setState({isKyc:!this.state.isKyc})}>Skip</Text>
  </View>
   </View>
   </View>
   </Modal>
   <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.isBenifieciary}
      >
      <View style={{flex: 1,
    alignItems: 'center',
   paddingTop:100,
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#eee',
    height: 200,
    width: 300,
    borderRadius: 3,
   
    flexDirection:'column'
  }}> 
  <View style={{flexDirection:'row',borderBottomColor:'#fff',borderBottomWidth:1,paddingVertical:10,backgroundColor:'#F06124'}}>
   <Text style={{textAlign:'center',fontSize:16,flex:1,color:'#fff',fontWeight:'bold'}}>Notification</Text>
   </View>
   <Text style={{paddingVertical:25,paddingHorizontal:10}}>Please add atleast one beneficiary to get kyc approved</Text>
  <View style={{flexDirection:'row',paddingHorizontal:10,paddingTop:25}}>
  <Text style={styles.continueBtn} onPress={()=>navigate('AddBeneficiary')}>Continue</Text>
      <Text style={styles.skipBtn} onPress={()=>this.setState({isBenifieciary:!this.state.isBenifieciary})}>Skip</Text>
  </View>
   </View>
   </View>
   </Modal>
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.isTerms}>
      <View style={{flex: 1,
    flexDirection: 'column',
   }} >
      <View style={{backgroundColor: '#fff',
    
   }}> 
   <View style={{borderBottomWidth:1,borderBottomColor:'#000',paddingBottom:10,flexDirection:'row',marginTop:10}}>
   <Text style={{flex:0.9,paddingHorizontal:5,paddingTop:20,fontSize:18,color:'#094F66',textAlign:'center',fontWeight:'600'}}> PRIVACY & POLICY </Text>
   <Text style={{flex:0.1,textAlign:'center',color:'#094F66',marginTop:20}} onPress={()=>this.setState({isTerms : !this.state.isTerms})}><FontAwesome style={{fontSize: 18,color:'teal',textAlign:'center',}}>{Icons.close}</FontAwesome></Text>
   </View>
   <ScrollView>
   <Privacy />
   </ScrollView>
   </View>
   </View>
   </Modal>
       <View
       >
        {/* <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />*/}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center',}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 20,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Fee Calculator</Text>
            </View>
            {/*<View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Home')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
            */}
        </View> 
        <ScrollView style = {styles.scrollViewStyle}>
        {/* <View style={styles.logoContainer}>
        <Image
           source ={require('./img/app-logo-small.png')} 
         />
         
         </View> */}
        
         
       <View style={{paddingVertical:15,paddingHorizontal:5,flexDirection:'column',marginTop:15}}>
     {/*  
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Sending From" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#000'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      value={this.state.sendFrom}
      onChangeText={(text)=>this.setState({sendFrom:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Sending To" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#000'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      value={this.state.sendTo}
      onChangeText={(text)=>this.setState({sendTo:text})}
       ></TextInput>
         </View>  */}
       {/*  <View style={{paddingHorizontal : 25,paddingVertical:6,}}>
         
         <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label=''
              value={sending}
              data={this.state.senderCountry}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              dropdownOffset = { {top:32,left: 10}}
              baseColor = {'#484848'}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}
             
            />
         </View>  */}
       {/*  <View style={{paddingHorizontal : 25,paddingVertical:9,}}>
         <TextInput
       placeholder="Sending Amount" style={[styles.InputText,this.state.validAmountErr&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#484848'
       returnKeyType={ "done" }
       keyboardType = { "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
      onChangeText={text => this.changeAmountFun(text)}
     
      value={this.state.input}
       ></TextInput>
       {this.state.validAmountErr &&<Text style={styles.errorText}>{this.state.error}</Text>}
         </View>

*/}


<View style={{flex:1,flexDirection:'row',paddingHorizontal : 14,paddingVertical:10,borderColor:'#ccc',height:80,borderRadius:5,borderWidth:1}}>
            
             <View style={{flex:0.5,flexDirection:'column'}}>
                  <Text style={{color:'#7b8376',fontSize:18}}> 
                  You send  
                  </Text>
                  <TextInput
       placeholder="Sending Amount" style={{backgroundColor : 'transparent', paddingVertical : 10,
        paddingHorizontal: 5,fontSize:18,color:'#026d7f',fontSize:18
       }} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#026d7f'
       returnKeyType={ "done" }
       keyboardType = { "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
      value={this.state.input}
      onChangeText={text => this.changeAmountFun(text)}
      onBlur = {e=>this.changeBlurAmountFun()}
       ></TextInput>
             </View>
           
             <View style={{flex:0.5,flexDirection:'row',backgroundColor:'#026d7f',borderRadius:5,borderWidth:1,overflow:'hidden',borderColor:'#026d7f',}}>
             <View style={styles.imageDiv}>
      {this.state.sendImg==true &&<Image source={this.state.defaultSenderFlag} style={styles.flagImage} /> }
                            {/* <Thumbnail square source={require('./img/flag/Afghanistan.png')} style={{height:40,width:40,borderRadius:80}}/> */}
                      </View>
              <View style={styles.DropdownDiv}>
              <RNPickerSelect           
                      items={this.state.senderCountry}
                      onValueChange={
                          (item) => {
                            this.onChangeText(item);
                            console.log(item);
                            this.setState({sendingCurrency : item});
                            console.log(this.state.sendingCurrency);
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.sendingCurrency}
                    
                    />
                      </View>
             </View>
         </View> 
         
         
      {/*   <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         
         <Dropdown
              ref={this.countryRef}
              onChangeText={this.onCountryChange}
              label={this.state.blankMsg}
              value={receive}
              data={this.state.receiverCountry}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              baseColor = {'#484848'}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,paddingHorizontal:5}}
             
            />
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:9,}}>
         <TextInput
       placeholder="Receiving Amount" style={[styles.InputText,]} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#484848'
       returnKeyType={ "done" }
       keyboardType = { "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
      editable={false}
     
      value={this.state.receivingAmount}
      
       ></TextInput>
      
         </View>
*/}

<View style={{flex:1,flexDirection:'row',paddingHorizontal : 14,borderColor:'#ccc',height:80,}}>

<View style={{flex:0.1,borderLeftColor:'#026d7f',borderLeftWidth:1,marginLeft:15}}>
<Image source ={require('./img/down.png')}  style={{position: 'absolute',
    top: 30,
    left: -11,
    
    }}/>
</View>
<View style={{flex:0.8,flexDirection:'column',paddingVertical:10,}}>
<Text style={{color:'#026d7f',fontSize:14,paddingVertical:0}}>{this.state.HandlingFeeAmount} {this.state.conFeeCurrency} Handling Fees</Text> 
<Text style={{color:'#026d7f',fontSize:14,paddingVertical:5}}>{this.state.ConFee} {this.state.conFeeCurrency} Service Fees</Text> 
<Text style={{color:'#026d7f',fontSize:14,paddingVertical:0}}>{this.state.exChangeRate}<Text style={{color:'#026d7f'}}> Exchange Rate</Text>
</Text>
</View>
</View>
         <View style={{flex:1,flexDirection:'row',paddingHorizontal : 14,paddingVertical:10,borderColor:'#ccc',height:80,borderRadius:5,borderWidth:1}}>
             
             <View style={{flex:0.5,flexDirection:'column'}}>
                  <Text style={{color:'#7b8376',fontSize:18}}> 
                  Receiver gets 
                  </Text>
                  <TextInput
       placeholder="Receiving Amount" style={{backgroundColor : 'transparent', paddingVertical : 10,
        paddingHorizontal: 5,fontSize:18,color:'#026d7f',fontSize:18
       }} 
       underlineColorAndroid = "transparent" returnKeyType={"next"} 
       placeholderTextColor='#026d7f'
       returnKeyType={ "done" }
       keyboardType = { "phone-pad" }
      autoCapitalize="none"
      autoCorrect={false}
      editable={false}
     
      value={this.state.receivingAmount}
     
      
       ></TextInput>
             </View>
            
             <View style={{flex:0.5,flexDirection:'row',backgroundColor:'#026d7f',borderRadius:5,borderWidth:1,overflow:'hidden',borderColor:'#026d7f'}}>
             <View style={styles.imageDiv}>
             {this.state.showImg==true && <Image source={this.state.uri} style={styles.flagImage}/>}
                              
                      </View>
                      <View style={styles.DropdownDiv}>
                   {/*}   <Dropdown
              ref={this.countryRef}
              onChangeText={this.onCountryChange}
              label={this.state.blankMsg}
              value={receive}
              data={this.state.receiverCountry}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={18}
              dropdownOffset = { {top:32,left: 10}}
              baseColor = {'#F06124'}
              textColor = {'#fff'}
              selectedItemColor = {'#484848'}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#026d7f',borderRadius:3,borderColor: '#026d7f',borderWidth: 1,paddingHorizontal:5,}}
             
            /> */}
              <RNPickerSelect
           
          items={this.state.receiverCountry}
          onValueChange={
          (item) => {
            this.setState({receivingCurrency : item});
            this.onCountryChange(item);
            console.log(item);
           
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.receivingCurrency}
         
        />
                      </View>
             </View>
             
       
         </View> 
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Deliver By" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#000'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      value={'Bank Deposit'}
      
       ></TextInput>
        </View>  */}
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Dropdown
              ref={this.MethodRef}
              onChangeText={this.onChangeDeliveryMethode}
              label='Select your delivery method'
              data={this.state.deliveryMethod}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              baseColor = {'#484848'}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,}}
             
            />
         </View>  */}
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Deliver In" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#000'
      keyboardType="default"
      autoCapitalize="none"
      value={this.state.deliverIn}
      autoCorrect={false}
      onChangeText={(text)=>this.setState({deliverIn:text})}
       ></TextInput>
         </View>
         */}
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         
         <Dropdown
              ref={this.typographyCurrencyRef}
              onChangeText={this.onChangeDeliveryCurrency}
              label='Select your currency'
              data={this.state.delievryCurrency}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              baseColor = {'#484848'}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,}}
             
            />
         </View>  */}
       {/*  <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         {/*<TextInput
       placeholder="Send" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      
         ></TextInput>
         <Dropdown
              ref={this.typographyPayMethodRef}
              onChangeText={this.onChangePayMethod}
              label='Send Amount/Receive'
              data={this.state.payMethod}
              pickerStyle={styles.textContainers}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              baseColor = {'#484848'}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',backgroundColor:'#fff',borderRadius:3,borderColor: '#fff',borderWidth: 1,}}
             
            />
         </View>*/}
       {/* <View style={{paddingHorizontal : 25,paddingVertical:25,flexDirection:'column'}}>
       <Text style={{color:'#484848',fontSize:15,textAlign:'center'}}>Exchange rate : {this.state.exChangeRate}</Text>
       <Text style={{color:'#484848',fontSize:13,textAlign:'center',paddingVertical:6}}>Service Fee : 15 AUD </Text>
      
       <Text style={{color:'#484848',fontSize:12,textAlign:'center',paddingVertical:6}}>This calculator displays market live rates.</Text>
         </View> */}
       {this.state.result&&<View style={{paddingHorizontal : 25,paddingVertical:10,flexDirection:'column'}}>
       <Text style={{color:'#fff'}}>Service Fee : {this.state.serviceFee}</Text>
        <Text style={{color:'#fff',paddingVertical:10}}>Fee Currency : {this.state.FeeCurrency}</Text>
         </View>}
         
         <View style={{paddingHorizontal : 25,paddingVertical:20,}}>
         <Text style={{fontSize:16}}>By continuing you agree to our <Text  onPress={()=>this.setState({isTerms : !this.state.isTerms})} style={{color:'#026d7f'}}> terms of use FSG & PDS</Text></Text>
         </View>
        
       </View>
       </ScrollView>
       <View style={styles.sendDiv}>
         {/*<Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Send Money</Text> */}
         <Text style={styles.nextButton} onPress={()=>this.clickContinue()}>Send Money</Text>
         
         </View>
       </View>
       </View>
     </SafeAreaView>
   );
 }
}
const styles = EStyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#fff',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:25},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
nextButton : {

backgroundColor:'#F06124',
borderRadius: 4,
paddingVertical : 20,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
 
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
  },
  itemTextStyle : {
  
  },
  skipBtn : {flex:0.5,backgroundColor:'#F06124',textAlign:'center',paddingVertical:10,borderRadius:5,overflow:'hidden',marginLeft:5,color:'#fff'},
  continueBtn : {flex:0.5,backgroundColor:'#F06124',textAlign:'center',paddingVertical:10,borderRadius:5,overflow:'hidden',color:'#fff'},
  flagImage : {height:40,width:40, borderRadius: 40/2,},
  sendDiv : {paddingHorizontal : 25,paddingBottom:10,},
  DropdownDiv : {flex:0.7,paddingVertical:10},
  imageDiv : {flex:0.3,paddingVertical:10,paddingHorizontal:10},
  '@media (max-width: 320)': {
    sendDiv : {
      paddingHorizontal : 25,
      paddingBottom:0
    },
    flagImage : {height:35,width:35, borderRadius: 35/2,},
  imageDiv : {flex:0.3,paddingVertical:10,paddingHorizontal:6},
  DropdownDiv : {flex:0.7,},
  nextButton : {
    marginBottom : 25,
    backgroundColor:'#F06124',
    borderRadius: 4,
    paddingVertical : 15,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    fontSize:18,
    },
    
  }
 
});
EStyleSheet.build();
const pickerSelectStyles = EStyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    color:'#fff',
    backgroundColor:'#026d7f'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: '#F06124',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  
  }
});