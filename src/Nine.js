import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,AsyncStorage,Modal,ActivityIndicator,SafeAreaView
  
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import authData from '../headerData';
export default class Nine extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 async onSubmit(){
  // console.log(authData);
   this.setState({visible:true});
   var authDataToken =authData();
   try
   { 
     let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/signup', 
       { 
         method: 'POST',
             headers: 
               { 
                  'Accept': 'application/json', 
                  'Content-Type': 'application/json', 
                  'Authorization' : authDataToken,
                 
                 },
                 body: JSON.stringify({
                   "customerLoginId": global.customerLoginId,
                    "customerPassword": global.customerPassword,
"firstName": global.firstName,
"middleName": global.middleName,
"lastName": global.lastName,
"addressLine1": global.addressLine1,
"addressLine2": global.addressLine2,
"cityName": global.cityName,
"stateKey": global.stateKey,
"zipCode": global.zipCode,
"countryKey": global.countryKey,
"landlineNo": global.landlineNo,
"mobileNo": global.mobileNo,
"emailAddress": global.customerLoginId,
"customerID1Tp": global.customerID1Tp,
"customerID1No": global.customerID1No,
"id1IssueDate": global.id1IssueDate,
"id1ExpiryDate": global.id1ExpiryDate, 
"id1IssueAuthority": global.id1IssueAuthority,
"genderKey": 1,
"occupa onIndustryKey": 1,
"occupa onTitle": "A orney",
"isCompany": 1,
"businessName": global.businessName, 
"businessLicenseNumber": global.businessLicenseNumber

                 })
            }); 
     this.setState({visible:false});
             let res = await response.text(); 
           console.log(res);
           this.setState({visible:false});
            //console.log(response.status);
            let result= JSON.parse(res); 
           // console.log(result.Status);
          
           var jsonResult =  result.Status['0'];
           console.log(jsonResult.ErrorMessage);
          
           // alert(JSON.stringify(result));
           if(jsonResult.ErrorCode == 1000){
            // alert('suucess');
             var customerInfoJson = result.CustomerInfo['0'];
             const { navigate } = this.props.navigation;
             AsyncStorage.setItem("CustomerKey", customerInfoJson.customerKey.toString());
             AsyncStorage.setItem("CustomerID", customerInfoJson.customerLoginID);
             global.CustomerKey = customerInfoJson.customerKey;
           console.log(customerInfoJson);
             navigate('FeeCalculator');
              }else if(jsonResult.ErrorCode == 1001){
               // alert(jsonResult.ErrorMessage);
                this.setState({ErrorMessageView:true});
                this.setState({ErrorMessage:jsonResult.ErrorMessage});
              }else if(jsonResult.ErrorCode == 1003){
                this.setState({ErrorMessageView:true});
                this.setState({ErrorMessage:jsonResult.ErrorMessage});
              }else{ 
               let error = res;
               throw error;
               } 
             } catch (error) { 
     //this.setState({error: error});
     console.log('catch');
     console.log(JSON.stringify(error));
    // console.log(JSON.parse(error));
    }
    }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
          <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:40,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
              <Text style={{textAlign:'left',color:'#fff',paddingLeft:15}} onPress={()=>navigate('Sixth')}><FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
               
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Register</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}}><FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Personal Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}>
      
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
           {this.state.ErrorMessageView==true && <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>{this.state.ErrorMessage}</Text>}
       
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="ID Issue Date" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       
      onSubmitEditing={()=>this.issueAuthInput.focus()}
      onChangeText={(text) => this.setState({id1IssueDate:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Issuing Authority" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.issueAuthInput=input}
      onSubmitEditing={()=>this.securityInput.focus()}
      onChangeText={(text) => this.setState({id1IssueAuthority:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Security 4 Digit Pin" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.securityInput=input}
    
      onChangeText={(text) => this.setState({securityKey:text})}
       ></TextInput>
         </View>
        
         <View style={{paddingHorizontal : 25,paddingVertical:10,marginTop:'80%'}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Register </Text>
         </View>
       </View>
</ScrollView>

       </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column'
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
}
});