import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,AsyncStorage,Modal,ActivityIndicator,Keyboard,FlatList,TouchableHighlight,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import RadioButton from 'radio-button-react-native';
import authData from '../../headerData';
class FlatListItem extends Component{
  
  render(){
    console.log(this.props)
       return(
         <View>
         {this.props.index%2==0 &&  <View style={{flex:1,borderBottomColor:'#fff',borderBottomWidth:1, }}>
         <TouchableHighlight onPress={()=>this.props.nav.navigate('SingleTransaction',{remittanceKey : this.props.item.RemittanceKey})} underlayColor='transparent'>
             <View style={styles.oddDiv}>
             <View style={{flex:0.3,paddingVertical:10,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#5b6455',paddingHorizontal:5,alignItems:'center',justifyContent:'center'}}>
             
            <Text style={{fontSize:14,color:'#fff', textAlign: 'center',
           }}>{this.props.item.BeneficiaryName}</Text>

       
             </View>
             <View style={{flex:0.4,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#5b6455',alignItems:'center',justifyContent:'center'}}>
            
             <Text style={{fontSize:13,color:'#fff',textAlign:'center',textDecorationLine : 'underline'}}>{this.props.item.ReferenceNo}</Text>
             
            
             </View>
             
                        <View style={{flex:0.3,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#5b6455',alignItems:'center',justifyContent:'center'}}>
                     <Text style={{color:'#fff',textAlign:'center',paddingVertical:10}}>{this.props.item.Status}</Text>
             </View>
         </View>
       
      </TouchableHighlight>
           </View> }

           {this.props.index%2!=0 &&  <View style={{flex:1,borderBottomColor:'#fff',borderBottomWidth:1, }}>
         <TouchableHighlight onPress={()=>this.props.nav.navigate('SingleTransaction',{remittanceKey : this.props.item.RemittanceKey})} underlayColor='transparent'>
             <View style={styles.oddDiv}>
             <View style={{flex:0.3,paddingVertical:10,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',paddingHorizontal:5,alignItems:'center',justifyContent:'center'}}>
             
            <Text style={{fontSize:14,color:'#5b6455', textAlign: 'center',
           }}>{this.props.item.BeneficiaryName}</Text>

       
             </View>
             <View style={{flex:0.4,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',alignItems:'center',justifyContent:'center'}}>
            
             <Text style={{fontSize:13,color:'#5b6455',textAlign:'center',textDecorationLine : 'underline',paddingBottom:10}}>{this.props.item.ReferenceNo}</Text>
             
            
             </View>
             
                        <View style={{flex:0.3,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',alignItems:'center',justifyContent:'center'}}>
                     <Text style={{color:'#5b6455',textAlign:'center',paddingVertical:10}}>{this.props.item.Status}</Text>
             </View>
         </View>
       
      </TouchableHighlight>
           </View> }
           </View>
       )

  }
}
export default class TransactionHistory extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      transactionId :'',
      isSuccessMsg : false,
      successMsg : '',
      transactions:[],
      isTransaction:true,
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
   this.getAllTransactions();
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 pressNav(){
 // Keyboard.dismiss();
  
  this.props.navigation.navigate("DrawerOpen");
}
 onSubmit(){
    var validMail = false;
    if (this.state.transactionId.length >= 3) {
        console.log('if......');
        this.setState({ firstNameError: false })
        validMail = false
  
      } else {
        this.setState({ firstNameError: true })
        validMail = true
      }
  
    
      if (!validMail) {
        console.log('true',validMail);
        this.loginFun();
      }
 }
 async getAllTransactions(){
    this.setState({visible:true});
    var authDataToken =authData();
    try
    { 
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/transaction/selectAll', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,
                  
                  },
                  body : JSON.stringify({
                    "customerKey":global.CustomerKey
                  })
             }); 
             let res = await response.text(); 
                this.setState({visible:false});
          if(res){
             res =  JSON.parse(res);
             var jsonResult  = res.Status[0];
          
             if(jsonResult.errorCode==1000){
                this.setState({isTransaction:true})
                this.setState({transactions:res.TransactionsInfo});
             }else if(jsonResult.errorCode==1001){

             }else if(jsonResult.errorCode==1002){
                this.setState({isTransaction:false})
            }else{
                let errors = res;
                throw errors;
            }
          }else{
            let errors = await response;
            throw errors;
        }
     
              } catch (error) { 
         console.log('catch');

     }
     
  }
  pressNav(){
    Keyboard.dismiss();
    
    this.props.navigation.navigate("DrawerOpen");
  }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
          <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
     
       <ScrollView >
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
        <View style={{flex:.2}}>
        <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View> 
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Transactions History</Text>
            </View>
            <View style={{flex:.2}}>
            <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('FeeCalculator')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
         </View>
        </View>  
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       
       <View style={{paddingHorizontal : 5,paddingVertical:10,}}>
       <View style={[styles.oddDiv,{borderBottomColor:'#fff',borderBottomWidth:1,}]}>
                        <View style={{flex:0.3,paddingVertical:10,backgroundColor:'#006072',paddingHorizontal:5,borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                
                       <Text style={{fontSize:14,color:'#fff', textAlign: 'center',
                      }}>Beneficiary Name</Text>

                  
                        </View>
                        <View style={{flex:0.4,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#006072',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                       
                        <Text style={{fontSize:13,color:'#fff',textAlign:'center'}}>Reference Number</Text>
                        </View>
                        
                                   <View style={{flex:0.3,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#006072',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'#fff',textAlign:'center',paddingVertical:10,paddingHorizontal:5,}}>Status</Text>
                        </View>
                    </View>
      {this.state.isTransaction&&<FlatList 
                      data={this.state.transactions}
                      keyExtractor={(x,i)=>i}
                    
                      renderItem={({item,index})=>{
                        return (
                          <FlatListItem item={item} index={index} nav={this.props.navigation}>
                          </FlatListItem>
                        );
                      } 
                    }/> }
                   
         </View>
        
         {!this.state.isTransaction&&<View style={{backgroundColor:'#5b6455',marginHorizontal:10,flex:1}}>
                      <Text style={{paddingVertical:10,paddingHorizontal:10,color:'#fff'}}>No Transactions available!</Text>
                    </View>}
       
       </View>
    {/*  7697087433 <Text style={styles.bottomInstructions}>With very competetive rates and a simple process, we'll help you pay your suppliers in over 200 countries.</Text> */}
       </View>
       </ScrollView >
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({
container : {
  flex: 1,
  backgroundColor: '#fff',
  flexDirection:'column',
  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',
},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 20,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 2,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
    paddingHorizontal : 25,paddingVertical:10,fontSize:14
    
  },
  oddDiv : {
    flexDirection:'row',
    paddingHorizontal:5,
   
},
});