import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Modal,ActivityIndicator,Alert,Keyboard,FlatList,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import authData from '../../headerData';
export default class SingleTransaction extends Component {
    constructor(props){
    super(props);
    global.count = 0;
  
    this.state ={
      accountType : 'Individual',
      visible: false,
      sendFrom : 'Australia - AUD',
      sendTo : 'India',
      deliverIn : 'INR',
      amount : '',
      result : false,
      validAmountErr:false,
      amountCheck:false,
      TransactionInfo : [],
      deliveryMethod : [],
      delievryCurrency : [],
      oldMsgError : false,
      newMsgError : false,
      oldPassword : '',
      newPassword :'',
      isSucess : false
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
    //this.getSenderCountry();
   this.getUserDetails();
 }

 async getUserDetails(){
    const { params } = this.props.navigation.state;
     var remittanceKey = params.remittanceKey;
     console.log(params);
    var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/transaction/select', 
             { 
                 method: 'POST',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         },
                         body : JSON.stringify({
                            "customerKey":global.CustomerKey
                            ,"remittanceKey":remittanceKey
                         })
                         
                 }); 
                 let res = await response.text(); 
                     console.log(res);
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                             this.setState({TransactionInfo : result.TransactionInfo});
                     }else{
                        let errors = await response.text();
                        throw errors;
                     }
     }catch (errors) { 
                      console.log('catch');
                      console.log(errors);
                    }
  }

  deletePress(){
   
    Alert.alert(
      'Confirmation',
      'Are sure want to delete this transaction?',
      [
        {text: 'Yes', onPress: () => this.deleteBeneficiary()},
        {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
      ]
  );

 }

 async deleteBeneficiary(){
  const { params } = this.props.navigation.state;
  var remittanceKey = params.remittanceKey;
  var authDataToken =authData();
this.setState({visible : true});
  try
  { 
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/transaction/delete', 
      { 
          method: 'POST',
              headers: 
              { 
                  'Accept': 'application/json', 
                  'Content-Type': 'application/json', 
                  'Authorization' : authDataToken,
                  
                  },body : JSON.stringify({
                      "remittanceKey": remittanceKey,
                      "deleteReason": "Wrong phone number"
                  })
          }); 
          let res = await response.text(); 
          var authDataToken =authData();
       // console.log('getBeneficiary///////// :-'+authDataToken);
              console.log(res);
              this.setState({visible : false});
              let result= JSON.parse(res); 
              var jsonResult =  result.Status[0];
              if(jsonResult.ErrorCode==1000){
                  const { navigate } = this.props.navigation;
                  navigate('TransactionList');
              }else{
                  let errors = res;
                  throw errors;
              }
             
}catch (errors) { 
             //  console.log(errors);
               console.log(JSON.stringify(errors));
             }
 }
              pressNav(){
                Keyboard.dismiss();
                
                this.props.navigation.navigate("DrawerOpen");
              }
  
  render() {
    const { navigate } = this.props.navigation;
   return (
        <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
     {/*  <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         />
       </View> */}
       <View style={styles.textContainer}>
        {/* <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <View style={{flex:.2}}>
            <Text style={{textAlign:'left',color:'#fff',paddingLeft:15,fontSize: 18}} onPress={()=>navigate('TransactionList')}> 
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
         </View>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Transaction History</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',}} onPress={()=>navigate('TransactionList')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View> */}
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
        <View style={{flex:.2}}>
        <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>navigate('TransactionList')} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome>
            </Text>
            </View> 
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Transactions History</Text>
            </View>
            <View style={{flex:.2}}>
            <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('TransactionList')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
         </View>
        </View>
        <ScrollView style = {styles.scrollViewStyle}>
        {/*<View style={styles.logoContainer}>
         <Image
           source ={require('../img/app-logo-small.png')} 
         />
         
         </View> */}
        
         
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row'}}>
          <Text style={styles.detailHeading}>Transaction's Information:</Text>
         {/* <View style={{flex:0.1,flexDirection:'row'}}>
            <Text style={{textAlign:'right'}} onPress={()=>this.deletePress()}> <FontAwesome style={{fontSize: 18,color:'#000',textAlign:'center'}}>{Icons.trash}</FontAwesome> </Text>
          </View> */}
      </View>
      <FlatList 
                      data={this.state.TransactionInfo}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>  
<View style={{paddingVertical:10,flexDirection:'column',}}>

      <View style={styles.labelContainerDiv}>
              
               <Text style={{fontSize:18}}>
                {item.FirstName} {item.LastName}
                </Text>
         </View> 
         <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
               <Text>
               ReferenceNo
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
              {item.ReferenceNo}
               </Text> 
               </View>
     
         </View> 
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Beneficiary Name 
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>             
              <View style={styles.valueDiv}>
                  <Text style={styles.textDetails}>{item.BeneficiaryName}</Text>                   
             </View>    
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
            Beneficiary Address
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
       {item.BeneficiaryAddress}
             </Text> 
        </View>
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                  Bank Name
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                  <Text style={styles.textDetails}>{item.DeliveryBankName}</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Sending Currency
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>{item.SendersCurrencyName}</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                 Sending Amount
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>{item.SendingAmount}</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
               Service Charge
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.ServiceCharge}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Handling Fee
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.SCHandlingFee}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Discount Amount
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.DiscountAmount}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Exchange Rate
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.ExchangeRate}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Receiving Currency
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.ReceivingCurrencyName}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Source Of Income
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.SourceOfIncome}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                 Purpose
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.RemitPurpose}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                  Status
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.PublicDescription}
                  </Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Deposit bank 
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                 {item.SendingLocationName}
                  </Text> 
                   
             </View>
     
         </View>
      


</View>
}
/>
</View>    

       </View>
       </ScrollView>
       
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
  },
  detailHeading : {color:'#094F66',fontSize:16,fontWeight:'bold',flex:0.9},
  labelContainerDiv : {paddingHorizontal : 10,paddingVertical:4,flexDirection:'row',},
  labelDiv:{flex:0.4,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
  valueDiv : {flex:0.5,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
  blankDiv : {flex:0.1,borderBottomColor:'gray',borderBottomWidth:1,},
});