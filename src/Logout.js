import React, { Component } from 'react';
import {
  AppRegistry,NetInfo,
  StyleSheet,
  Text,
  View,Image,Button,TouchableOpacity,TouchableHighlight,ScrollView,Alert,AsyncStorage,BackAndroid,SafeAreaView 
} from 'react-native';
import { LoginButton, AccessToken,LoginManager,GraphRequestManager,GraphRequest, } from 'react-native-fbsdk';
import LocalData from './Components/LocalData';
export default class Logout extends Component {
    static navigationOptions = {
        header: null ,
     };

     componentDidMount(){
        // AsyncStorage.setItem("loginStatus", "loggedIn");
        Alert.alert(
            'Message',
            'Are you sure want to logout!',
            [
                {text : 'Cancel', onPress:()=>{this.dasboard()}},
              {text : 'OK', onPress:()=>{this.logout()}}
            ]
           );
       
     //   this.logout()
         }
        logout(){
            AsyncStorage.removeItem("CustomerKey");
            AsyncStorage.removeItem("CustomerID");
            AsyncStorage.setItem("IsApproved",'1');
            global.CustomerKey = null;
            global.IsApproved = null;
            const { navigate } = this.props.navigation;
            AsyncStorage.clear();
            AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove)
            global.userName = '';
            global.usersName ='';
            global.loggedUsername = '';
            AsyncStorage.removeItem("LoggedUsername");
            AsyncStorage.removeItem("usersName");
            AsyncStorage.removeItem("userName");
            console.log("LoginManager authType "+global.authType)
            if(global.authType =='facebook'){
            console.log("IF authType facebook=="+global.authType)
              
              LoginManager.logOut(()=>console.log('test'));
              navigate('Second');
                 }else{
            console.log("IF authType User=="+global.authType)
              
              navigate('Second');
            }
           
        }
        dasboard(){
            const { navigate } = this.props.navigation;
            navigate('FeeCalculator');
        }
        render() {
            return(
               <SafeAreaView style={{flex: 1,backgroundColor:'#005068' }}>
              <View  style={styles.container}>
                <View
          style={styles.imageContainer}>
          <Image
            style={{
              flex: 1,
              
            }}
            source ={require('./img/bgImage.png')} 
          />
        </View>
              </View>
              </SafeAreaView>
            );
        }

}const styles = StyleSheet.create({

    container : {
      flex: 1,
      backgroundColor: '#eee',
      flexDirection:'column',
     
    }
});