import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,KeyboardAvoidingView,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
export default class Fifth extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 onSubmit(){
  global.cityName = this.state.cityName;
  global.stateKey = this.state.stateKey;
  global.zipCode = this.state.zipCode;
  global.mobileNo = this.state.mobileNo;
  global.landlineNo = this.state.landlineNo;
  global.customerID1Tp = this.state.customerID1Tp;
  global.customerID1No = this.state.customerID1No;
  global.id1ExpiryDate = this.state.id1ExpiryDate;
    const { navigate } = this.props.navigation;
                     navigate('Nine');
 }
  render() {
    const { navigate } = this.props.navigation;
   return ( 
   <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
    <KeyboardAvoidingView
    style={styles.container}
    behavior="padding" >
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:40,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15}} onPress={()=>navigate('Fifth')}><FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Register</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}}><FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Personal Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}>
      
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="City" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      onSubmitEditing={()=>this.stateInput.focus()}
      onChangeText={(text) => this.setState({cityName:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="State / Province" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.stateInput=input}
      onSubmitEditing={()=>this.zipInput.focus()}
      onChangeText={(text) => this.setState({stateKey:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Zip / Postal Code" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.zipInput=input}
      onSubmitEditing={()=>this.mobileInput.focus()}
      onChangeText={(text) => this.setState({zipCode:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Mobile#" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
     ref={(input)=>this.mobileInput=input}
      onSubmitEditing={()=>this.landlineInput.focus()}
      onChangeText={(text) => this.setState({mobileNo:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Landline#" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.landlineInput=input}
      onSubmitEditing={()=>this.idTypeInput.focus()}
      onChangeText={(text) => this.setState({landlineNo:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="ID Type" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
     ref={(input)=>this.idTypeInput=input}
      onSubmitEditing={()=>this.idNumberInput.focus()}
      onChangeText={(text) => this.setState({customerID1Tp:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="ID Number" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.idNumberInput=input}
      onSubmitEditing={()=>this.idExpInput.focus()}
      onChangeText={(text) => this.setState({customerID1No:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="ID Expiry Date" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.idExpInput=input}
    
      onChangeText={(text) => this.setState({id1ExpiryDate:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Continue to register </Text>
         </View>
       </View>
</ScrollView>

       </View>
     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column'
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
}
});