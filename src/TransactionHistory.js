import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,AsyncStorage,Modal,ActivityIndicator,Keyboard,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import RadioButton from 'radio-button-react-native';
import authData from '../headerData';
export default class TransactionHistory extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      transactionId :'',
      isSuccessMsg : false,
      successMsg : '',
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 pressNav(){
 // Keyboard.dismiss();
  
  this.props.navigation.navigate("DrawerOpen");
}
 onSubmit(){
    var validMail = false;
    if (this.state.transactionId.length >= 3) {
        console.log('if......');
        this.setState({ firstNameError: false })
        validMail = false
  
      } else {
        this.setState({ firstNameError: true })
        validMail = true
      }
  
    
      if (!validMail) {
        console.log('true',validMail);
        this.loginFun();
      }
 }
 async loginFun(){
    this.setState({visible:true});
    var authDataToken =authData();
    var transactionId = this.state.transactionId;
   // console.log(authDataToken);
    try
    { 
      let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/transaction/'+transactionId+'/status', 
        { 
          method: 'GET',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,
                  
                  }
             }); 
             let res = await response.text(); 
            console.log(res);
            this.setState({visible:false});
             //console.log(response.status);
             let result= JSON.parse(res); 
            // console.log(result.Status);
           
            var jsonResult =  result.Status['0'];
            console.log(jsonResult.ErrorMessage);
           
            // alert(JSON.stringify(result));
              if(jsonResult.ErrorCode == 1000){
                     console.log('suucess');
                    this.setState({isSuccessMsg : true, successMsg: jsonResult.ErrorMessage,ErrorMessageView:false,firstNameError:false})
                  
               }else if(jsonResult.ErrorCode == 1001){
                 console.log(jsonResult.ErrorMessage);
                 this.setState({ErrorMessageView:true});
                 this.setState({ErrorMessage:jsonResult.ErrorMessage});
               }else{ 
                let error = res;
                throw error;
                } 
              } catch (error) { 
      //this.setState({error: error});
      console.log('catch');
      console.log(JSON.stringify(error));
     // console.log(JSON.parse(error));
     }
     
  }
  pressNav(){
    Keyboard.dismiss();
    
    this.props.navigation.navigate("DrawerOpen");
  }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
          <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <ScrollView >
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
        <View style={{flex:.2}}>
        <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View> 
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Transaction Status</Text>
            </View>
            <View style={{flex:.2}}>
            <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Home')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
         </View>
        </View>
        <View style={styles.logoContainer}>
         <Image
           source ={require('./img/app-logo-small.png')} 
         />
         <Text style={[styles.logoText,{paddingTop:15}]}>Please enter transaction Id</Text>
         
         </View>
        
        
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       {this.state.isSuccessMsg===true&&<Text style={{paddingHorizontal : 25,paddingVertical:10,fontSize:14,color:'#fff'}}>{this.state.successMsg}</Text>}
       {this.state.ErrorMessageView==true && <Text style={styles.errorText}>{this.state.ErrorMessage}</Text>}
       {this.state.firstNameError===true && <Text style={styles.errorText}>TransactionId must be required </Text>}
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Transaction ID" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      autoFocus={true}
      onChangeText={(text) => this.setState({transactionId:text})}
       ></TextInput>
         </View>
        
      
        
       </View>
    {/*   <Text style={styles.bottomInstructions}>With very competetive rates and a simple process, we'll help you pay your suppliers in over 200 countries.</Text> */}
       </View>
       </ScrollView >
       <View style={{paddingHorizontal : 30,paddingVertical:10,marginBottom:10}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Get Status</Text>
         </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {

  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 20,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,

marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 2,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
    paddingHorizontal : 25,paddingVertical:10,fontSize:14
    
  }
});