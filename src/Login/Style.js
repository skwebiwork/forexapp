import EStyleSheet from 'react-native-extended-stylesheet';
const Styles = EStyleSheet.create({

    container : {
                      flex: 1,
                      backgroundColor: '#eee',
                      flexDirection : 'column',
                    
    },
    imageContainer : {
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
    },
    textContainer: {
                    flex: 1,
                    backgroundColor: 'transparent',
                    flexDirection:'column',
                    marginTop:20
    },
    logoContainer: {
                  flex:1,
                  flexDirection:'column',  
                  alignItems: 'center',
    },
    logoText : 
              {
                color:'#fff',
                fontSize:18,
                textAlign:'center',
                paddingVertical:15
              },
    next : {
                backgroundColor:'#F06124',
                borderRadius: 18,
                paddingVertical : 10,
                textAlign:'center',
                color:'#fff',
                overflow:"hidden",
                fontWeight:'bold',
    },
    login : {
      
      backgroundColor:'#0B3D49',
                borderRadius: 18,
                paddingVertical : 10,
                textAlign:'center',
                color:'#fff',
                overflow:"hidden",
    },
    fbLogin : {
      backgroundColor:'#4267b2',
      borderRadius: 18,
      paddingVertical : 10,
      textAlign:'center',
      color:'#fff',
      overflow:"hidden",
    },
    bottomInstructions : {
                color:'#fff',
                fontSize:14,
                textAlign:'center',
                paddingVertical:25,
                paddingHorizontal:10,
    },
    InputText : {
      paddingVertical : 10,
      borderRadius:3,
      borderColor: '#fff',
       borderWidth: 1,
       paddingHorizontal: 5,
       backgroundColor:'#fff'
      
    },
    errorInput : {
     borderColor: '#dd4b39',
      borderWidth: 1,
    },
    errorText : {
      color : '#dd4b39',
      paddingVertical : 10,
      fontSize:16,
    },
    logoImg : {marginTop:'15%',},
    formData : {flex:1,flexDirection:'column',paddingTop:'15%',},
    '@media (max-width: 320)': {
      logoImg : {marginTop:'10%',},
      formData : {flex:1,flexDirection:'column',paddingTop:'10%',},
    }
    });
    export default Styles;