import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,AsyncStorage,Modal,ActivityIndicator,Keyboard,ScrollView,Alert,Dimensions,AlertIOS,SafeAreaView
} from 'react-native';
import authData from '../../headerData';
import { StatusBar } from 'react-native';
import CheckBox from 'react-native-check-box'
import Privacy from '../Privacy';
import FontAwesome, { Icons } from 'react-native-fontawesome';
var {height, width} = Dimensions.get('window');
import styles from './Style';
import LocalData from '../Components/LocalData';
import { LoginButton, AccessToken,LoginManager,GraphRequestManager,GraphRequest, } from 'react-native-fbsdk';
export default class Forth extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
     email : '',
      visible: false,
      password : '',
      isChecked : false,
      chkError : false,
      resendLink : false,
      isTerms : false,
      fullName : '',
      IsEmailVerified : false,
      errorMsg : '',
      passwordMsg : '',
      isError : false,
    } 
  }
  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };
  changeBlurAmountFun(){
    Keyboard.dismiss();
  }
  onClick(chk){
    console.log(chk);
   var chkStatus = this.state.isChecked;
   this.setState({isChecked:chk});
   if(chkStatus){
     //console.log('true');
     //this.setState({isChecked:false});
   }else{
    // console.log(false);
   // this.setState({isChecked:true});
     
   }
  // console.log(this.state.isChecked);
}
handlePermission(){
  var _this = this;
  LoginManager.setLoginBehavior("web");
  LoginManager.logInWithReadPermissions(['public_profile','email']).then(
    
    function(result){
    //  var authDataToken =authData();
    //  console.log(authDataToken);
    if (result.isCancelled){
      console.log('Login cancelled');
    }else{
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            let accessToken = data.accessToken
              const responseInfoCallback = (error, result) => {
              if (error) {
              //  console.log(error)
               } else {
             //   console.log(result)
                console.log('Success fetching data: ' + result.toString());
               _this.LoginWithFB(result);
              }
            }
            const infoRequest = new GraphRequest(
              '/me',
              {
                accessToken: accessToken,
                parameters: {
                  fields: {
                    string: 'name,first_name,middle_name,last_name,email'
                  }
                }
              },
              responseInfoCallback
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start()
           // console.log(data);
          }
        )
      }
    },
    function(error) {
      console.log('Login fail with error: ' + error);
    }
  );
}
async LoginWithFB(res){
  console.log(res);
 var uName = res.name;
 var firstName = res.first_name;
 var lastName = res.last_name;
 var email = res.email;
 var id = res.id
    this.setState({visible:true});
    var authDataToken =authData();
    console.log(authDataToken);
    try
    { 
      let response = await fetch('http://api.remitanywhere.com/restapi/customer/oauth', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,
                  
                  },
                  body: JSON.stringify({
                    "customerLoginId": email,
                    "OAuthProvider": "Facebook",
                  	"OAuthUID": id
                  })
             }); 
             let res = await response.text(); 
            this.setState({visible:false});
             let result= JSON.parse(res);
            var jsonResult =  result.Status['0'];
            const { navigate } = this.props.navigation;
              if(jsonResult.ErrorCode == 1000){
                global.authType = "facebook";
                var customerInfoJson = result.CustomerInfo['0'];
                const { navigate } = this.props.navigation;
                global.CustomerKey = customerInfoJson.customerKey;
                AsyncStorage.setItem("LoggedUsername", customerInfoJson.CustomerFullName);  
                global.loggedUsername = customerInfoJson.CustomerFullName;
                global.loggedCustomerName=customerInfoJson.CustomerFullName;
                global.CustomerKey=customerInfoJson.customerKey;
                AsyncStorage.setItem("CustomerKey", customerInfoJson.customerKey.toString());
                AsyncStorage.setItem("CustomerID", customerInfoJson.customerID);
                AsyncStorage.setItem("CustomerApprovalStatus", customerInfoJson.customerApprovalStatus);
                global.CustomerApprovalStatus = customerInfoJson.customerApprovalStatus;
                this.getUserDetails()     
                this.setState({IsEmailVerified : customerInfoJson.isEmailVerified })
                if(customerInfoJson.isEmailVerified===true){
                    var fullName = this.state.fullName;
                    global.userName = fullName;
                }else{
                    this.setState({resendLink : true});  
                    let error = await response;
                    throw error;
                }         
               }else if(jsonResult.ErrorCode == 1001){
                  navigate('FbUserProfile',{first_name:firstName,last_name:lastName,customerLoginId:email,OAuthUID:id});
               }else{ 
                let error = await response;
                throw error;
                } 
              } catch (error) { 
            
              } 
    }
   onSubmit(){
   // console.log('click');
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    var email = this.state.email;
    var password = this.state.password;
    if(email=='') {
      this.setState({ isError:true,emailError: true,passwordError: false })
      this.setState({errorMsg : 'Email must be required'})
      validMail = true
    }else if(reg.test(email) === false){
      this.setState({ emailError: true,isError:true,passwordError: false})
      this.setState({errorMsg : 'Please enter a valid email id'})
    }else if(this.state.password=='') {
     // this.setState({ passwordError: false })
      this.setState({ emailError:false,errorMsg:'Password must be required',isError:true,passwordError:true })
      validPass = true
    }else if(this.state.password.length <= 3) {
      // this.setState({ passwordError: false })
       this.setState({ emailError: false,isError:true,passwordError:true,errorMsg:'Your password must consist of at least 8 characters (max: 15)' })
       validPass = true
     }else if(this.state.password.length >= 16) {
      // this.setState({ passwordError: false })
       this.setState({ emailError: false,isError:true,passwordError:true,errorMsg:'Your password must consist of at least 8 characters (max: 15)' })
       validPass = true
     }else if(this.state.isChecked==false){
       this.setState({ emailError: false,isError:true,passwordError:true,errorMsg:'Please confirm terms and conditions' })
     }else{
      this.setState({ emailError: false,passwordError: false,isError:false,passwordMsg:'' })
      this.loginFun();
    }
    
  }

   async resendLinkFun(){
    this.setState({visible:true});
    var authDataToken =authData();
     try
     { 
       let response = await fetch('http://api.remitanywhere.com/restapi/customer/activationlink', 
         { 
           method: 'POST',
               headers: 
                 { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                   },
                   body: JSON.stringify({
                    "customerKey": global.CustomerKey,
                    "SendEmail" : 1
                   })
              }); 
              let res = await response.text(); 
              this.setState({visible:false});
              let result= JSON.parse(res); 
             var jsonResult =  result.Status['0'];
               if(jsonResult.ErrorCode == 1000){
                       var ActivationLink = result.CustomerInfo[0].ActivationLink;
                       console.log(ActivationLink);
                     /*  global.setTimeout(() => {
                        AlertIOS.alert(
                          'Sucess',
                          'An activation email has been sent successfully. You should receive it shortly.',
                          [
                          {text : 'OK', onPress:()=>{this.resendSuccesFun()}}
                          ]
                         );
                      }, 50);  */
                      global.setTimeout(() => {
                        AlertIOS.alert(
                          'Sucess',
                          'An activation email has been sent successfully. You should receive it shortly.',
                          [
                          {text : 'OK', onPress:()=>{this.resendSuccesFun()}}
                          ]
                         );
                      }, 50);
                    //  var to = this.state.email;
                    //  this.sendActivationLink(to,ActivationLink);
                     //  AlertIOS.alert('An activation email has been sent successfully. You should receive it shortly.');
                }else if(jsonResult.ErrorCode == 1001){
                     console.log('elseIf');
                 
                }else{ 
                      console.log('else');
                 let error = res;
                 throw error;
                 } 
               } catch (error) { 
     
         }
  }
  resendSuccesFun(){
    this.setState({resendLink : false});  
  
  }
    async loginFun(){
    this.setState({visible:true});
    var authDataToken =authData();
    console.log(authDataToken);
    try
    { 
      let response = await fetch('http://api.remitanywhere.com/restapi/customer/authenticate', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,
                  },
                  body: JSON.stringify({
                    "customerLoginId": this.state.email
                    ,"customerPassword": this.state.password	                   
                  })
             }); 
             let res = await response.text(); 
             this.setState({visible:false});
             let result= JSON.parse(res); 
                // let result= res; 
                // console.log(result.Status);
            var jsonResult =  result.Status['0'];
              if(jsonResult.ErrorCode == 1000){
                    global.authType = "user";
                     var customerInfoJson = result.CustomerInfo['0'];
                     const { navigate } = this.props.navigation;
                     AsyncStorage.setItem("CustomerKey", customerInfoJson.CustomerKey.toString());
                     AsyncStorage.setItem("CustomerID", customerInfoJson.CustomerID);
                     AsyncStorage.setItem("CustomerApprovalStatus", customerInfoJson.CustomerApprovalStatus);
                     AsyncStorage.setItem("LoggedUsername", customerInfoJson.CustomerFullName);
                     global.CustomerApprovalStatus = customerInfoJson.CustomerApprovalStatus;
                     global.CustomerKey = customerInfoJson.CustomerKey;
                     global.loggedUsername = customerInfoJson.CustomerFullName;
                     global.loggedCustomerName=customerInfoJson.CustomerFullName;
                     global.myName = customerInfoJson.CustomerFullName;
                     this.getUserDetails();
                     this.setState({IsEmailVerified : customerInfoJson.IsEmailVerified })
                 if(customerInfoJson.IsEmailVerified===true){
                  var fullName = this.state.fullName;
                  global.userName = fullName;
                 // navigate('Feecalculator');
                 }else{
                    this.setState({resendLink : true});  
                    //this.setState({ErrorMessage:msg});
                    let error = await response;
                    throw error;
                    }
                  // navigate('FeeCalculator');
       }else if(jsonResult.ErrorCode == 1001){
                 this.setState({ErrorMessageView:true});
                 this.setState({ErrorMessage:jsonResult.ErrorMessage});
               }else{ 
                let error = await response;
                throw error;
                } 
              } catch (error) { 
   
          } 
  }

  async getUserDetails(){
    var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/'+global.CustomerKey, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                             var CustomerInfo = result.CustomerInfo[0];
                            if(CustomerInfo){
                             var FirstName = CustomerInfo.FirstName;
                             var LastName =  CustomerInfo.LastName;
                             var MiddleName= CustomerInfo.MiddleName;
                             global.userName = CustomerInfo.FirstName+' '+CustomerInfo.LastName;
                             global.usersName = CustomerInfo.FirstName+' '+CustomerInfo.LastName;
                             let uName = CustomerInfo.FirstName+' '+CustomerInfo.LastName;
                             AsyncStorage.setItem("usersName", CustomerInfo.FirstName+' '+CustomerInfo.LastName);
                             LocalData.setUserData({ uName: CustomerInfo.FirstName+' '+CustomerInfo.LastName, ...CustomerInfo });
                             LocalData.getUserData().then((response) => {
                              response = JSON.parse(response);
                              
                            });
                            
                             this.setState({fullName : uName});
                             global.customerCountryKey= CustomerInfo.CountryKey.toString();
                             AsyncStorage.setItem("customerCountryKey", CustomerInfo.CountryKey.toString());
                            
                             if(CustomerInfo.customerID1No!=''){
                              AsyncStorage.setItem("customerID1No","Yes");
                             }else{
                              AsyncStorage.setItem("customerID1No","No");
                             }
                             if(CustomerInfo.IsCompany==true){
                             AsyncStorage.setItem("IsApproved","1");
                             global.IsApproved = 1;
                             }else{
                              if(CustomerInfo.CustomerID1No==null||CustomerInfo.CustomerID1No==""){
                                global.IsApproved = 0;
                               AsyncStorage.setItem("IsApproved",CustomerInfo.IsApproved.toString());
                              }else{
                                var approve = 1;
                               AsyncStorage.setItem("IsApproved",approve.toString());
                               global.IsApproved = approve;
                              }
                             }
                             const { navigate } = this.props.navigation;
                             if(this.state.IsEmailVerified===true){
                              //navigate('Second');
                            navigate('FeeCalculator');
                               }
                              }else{
                                let error = await response.text();
                                throw error;
                              }
                                    
                     }else{
                      
                      let error = await response.text();
                      throw error;
                     }
     }catch (error) { 
                      console.log('catch'+global.CustomerKey);
                      console.log(error);
                    }
  }
  pressNav(){
    Keyboard.dismiss();
    this.props.navigation.navigate("DrawerOpen");
  }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
     <View
       style={styles.container}>
      <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
   </View>
   </View>
   </Modal>
   <Modal
      animationType="fade"
      transparent={true}
      visible={this.state.isTerms}>
      <View style={{flex: 1,
      flexDirection: 'column',
      }}>
      <View style={{backgroundColor: '#fff',
    
   }}> 
   <View style={{borderBottomWidth:1,borderBottomColor:'#000',paddingBottom:10,flexDirection:'row',marginTop:10}}>
   <Text style={{flex:0.9,paddingHorizontal:5,paddingTop:20,fontSize:18,color:'#094F66',textAlign:'center',fontWeight:'600'}}> PRIVACY & POLICY </Text>
   <Text style={{flex:0.1,textAlign:'center',color:'#094F66',marginTop:20}} onPress={()=>this.setState({isTerms : !this.state.isTerms})}><FontAwesome style={{fontSize: 18,color:'teal',textAlign:'center',}}>{Icons.close}</FontAwesome></Text>
   </View>
   <ScrollView>
   <Privacy />
   </ScrollView>
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
           }}
           source ={require('../img/bgImage.png')} 
         />
       </View>
       <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never"> 
       <View
         style={styles.textContainer}
       >
        <View style={styles.logoContainer}>
         <Image
           style={styles.logoImg}
           source ={require('../img/app-logo-small.png')} 
         />
         <Text style={styles.logoText}>International Currency Transfers</Text>
         </View>
         <View style={styles.formData}>
         
         {this.state.resendLink==true && <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]} onPress={(e)=>{this.resendLinkFun()}}>
         Your account is not activated yet. Please use activation link in the confirmation email to activate your account. <Text style={{color:'#fff',textDecorationLine: 'underline',}} onPress={(e)=>{this.resendLinkFun()}}>Click Here Resend Activation link</Text>
         </Text>}
         {this.state.ErrorMessageView==true && <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>{this.state.ErrorMessage}</Text>}
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         {this.state.isError===true && <Text style={styles.errorText}>{this.state.errorMsg} </Text>}
         <TextInput
                placeholder="User Name" style={[styles.InputText,this.state.emailError&&styles.errorInput]} 
                underlineColorAndroid = "transparent" returnKeyType="next" 
                placeholderStyle={{ fontFamily: "AnotherFont", borderColor: 'red' }}
                keyboardType="email-address"
                onSubmitEditing={()=>this.passwordInput.focus()}
                onChangeText={(text) => this.setState({email:text})}
                autoCapitalize="none"
                autoCorrect={false}
                placeholderTextColor='#000'></TextInput>
       
         </View>
        
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
                placeholder="Password" style={[styles.InputText,this.state.passwordError&&styles.errorInput]} 
                underlineColorAndroid = "transparent" returnKeyType="done" 
                placeholderTextColor='#000'
                keyboardType="default"
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry={true}
                ref={(input)=>this.passwordInput=input}
                onChangeText={(text)=>this.setState({password:text})}
                onBlur = {e=>this.changeBlurAmountFun()}></TextInput>
        {this.state.passwordError===true &&<Text style={[styles.errorText,{paddingVertical:10,fontSize:14}]} >{this.state.passwordMsg} </Text>}
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         {this.state.chkError&& <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>Please confirm terms and conditions</Text>}
            <View style={{flexDirection:'row'}}>
                        <CheckBox
                            style={{flex: 0.1,}}
                            checkBoxColor = {'#F06124'}
                            onClick={()=>this.onClick(!this.state.isChecked)}
                            isChecked={this.state.isChecked}
                            rightText={''}
                            rightTextStyle ={{color:'#fff'}}
                        />
                        <Text style={{flex:0.9,color:'#fff',alignItems:'center'}}>
                        I have agree to the <Text style={{textDecorationLine : 'underline',color:'#F06124'}} onPress={()=>this.setState({isTerms : !this.state.isTerms})}>terms and conditions </Text>.
                        </Text>
            </View>
        </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
                 <Text style={styles.login} onPress={(e)=>{this.onSubmit()}}>Login</Text>
         </View>
         
         <Text style={{color:'#fff',textAlign:'center',fontSize:12}} onPress={()=>navigate('Eight')}>Forgotten your username & password?</Text>
         {this.state.resendLink && 
             <Text style={{color:'#fff',textAlign:'center',fontSize:12,paddingVertical:6}} onPress={()=>this.resendLinkFun()}>Activate your account</Text>
         }
         </View>
         <Text style={{paddingHorizontal : 25,paddingVertical:10,color:'#fff',textAlign:'center',fontWeight:'bold'}}>Or</Text>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
             <Text style={styles.fbLogin} onPress={(e)=>{this.handlePermission()}}>Login With Facebook</Text>
         </View>
         <View style={{flexDirection:'column',paddingHorizontal : 25,paddingVertical:6,marginBottom:15}}>
         <Text style={{color:'#fff',textAlign:'center',fontSize:12,paddingVertical:6}} onPress={()=>navigate('Seventh')}>Don't have an account?</Text>
         <Text style={styles.next} onPress={()=>navigate('Seventh')}>Register now with BFX</Text>
         </View>
        
       </View>
       </ScrollView>
     </View>
     </SafeAreaView>
   );
 }
}
