import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,AsyncStorage,Modal,ActivityIndicator,Alert,AlertIOS,SafeAreaView
  
} from 'react-native';
import authData from '../headerData';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import RadioButton from 'radio-button-react-native';
export default class Fifth extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      email : '',
      visible: false,
      password : '',
      
    } 
  }
  componentDidMount() {
  //  StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
onSubmit(){
  console.log('click');
  var validMail = false;
  var validPass = false;
  if (this.state.email=='') {
       
    this.setState({ emailError: true,passwordError:true })
    this.setState({errorMsg : 'Email must be required'})
     validMail = true
  }else{
    this.setState({ emailError: false })
    validMail = false
  }
  if (this.validateEmail(this.state.email)) {
    this.setState({ emailError: false })
    validMail = false
  } else {
    this.setState({ emailError: true })
    this.setState({errorMsg : 'Please enter a valid email id'})
    validMail = true
  }

  // if (this.state.password.length >= 8) {
  //   this.setState({ passwordError: false })
  //   validPass = false

  // } else {
  //   this.setState({ passwordError: true })
  //   validPass = true
  // }


  if (!validMail) {
  //  console.log('true',validMail);
    this.loginFun();
  }
}
async loginFun(){
  this.setState({visible:true});
  var authDataToken =authData();
 console.log(authDataToken);
  try
  { 
    let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/forgotpassword', 
      { 
        method: 'POST',
            headers: 
              { 
                 'Accept': 'application/json', 
                 'Content-Type': 'application/json', 
                 'Authorization' : authDataToken,
                
                },
                body: JSON.stringify({
                
                   "emailAddress":this.state.email,
                   "sendEmail" : 1
	                 
                })
           }); 
           let res = await response.text(); 
           this.setState({visible:false});
          console.log(res);
        
          //this.setState({visible:false});
             //console.log(response.status);
             let result= JSON.parse(res); 
            // console.log(result.Status);
           
            var jsonResult =  result.Status['0'];
            console.log(jsonResult);
           
            // alert(JSON.stringify(result));
              if(jsonResult.errorCode == 1000){
                     console.log(jsonResult.errorMessage);
                     global.setTimeout(() => {
                      AlertIOS.alert(
                        'Notification',
                        'A Reset password email has been sent successfully. You should receive it shortly.',
                        [
                        {text : 'OK' , onPress:()=>{this.goToLogin()}}
                        ]
                       );
                    }, 50);
               }else if(jsonResult.errorCode == 1002){
                 console.log(jsonResult.errorMessage);
                 this.emailInput.focus();
                 global.setTimeout(() => {
                 AlertIOS.alert(
                  'Warning',
                  'Provided email address is not registered.',
                  [
                  {text : 'OK'}
                  ]
                 );
                }, 50);
                 
               }else{ 
                let error = res;
                throw error;
                } 
         
            } catch (error) { 
              this.setState({visible:false});
    //this.setState({error: error});
    console.log('catch');
    console.log(JSON.stringify(error));
   // console.log(JSON.parse(error));
            }
            }
            goToLogin(){
              const { navigate } = this.props.navigation;
              navigate('Forth');
            }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
        <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Forgot Password</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Forth')}><FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never"> 
        <View style={styles.logoContainer}>
         <Image
           source ={require('./img/app-logo-small.png')} 
         />
         <Text style={[styles.logoText,{paddingTop:15}]}>Please enter email address</Text>
         <Text style={styles.logoText}>(register with this account)</Text>
         </View>
        
        
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       {this.state.passwordError&&<Text style={styles.errorText}>{this.state.errorMsg}</Text>}
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Email Id" style={[styles.InputText,this.state.emailError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       keyboardType="email-address"
      onSubmitEditing={()=>this.onSubmit()}
      onChangeText={(text) => this.setState({email:text})}
      autoCapitalize="none"
      autoCorrect={false}
      placeholderTextColor='#000'
      ref={(input)=>this.emailInput=input}
       ></TextInput>
         </View>
         {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="New Password" style={[styles.InputText,this.state.passwordError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry={true}
      ref={(input)=>this.passwordInput=input}
        onChangeText={(text)=>this.setState({password:text})}
        placeholderTextColor='#000'
       ></TextInput>
         </View> */}
      
         
       </View>
    
      </ScrollView>
      <View style={{paddingHorizontal : 30,paddingVertical:10,marginBottom:10}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Reset your Password </Text>
         </View>
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
                  backgroundColor: '#eee',
                  flexDirection : 'column',
                  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {

backgroundColor:'#F06124',
borderRadius: 8,
paddingVertical : 14,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
/*backgroundColor:'#F06124',
borderRadius: 4,
paddingVertical : 20,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
fontSize:18, */
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
  borderColor: '#dd4b39',
  borderWidth: 1,
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
  paddingHorizontal : 25,
  fontSize:16
}
});