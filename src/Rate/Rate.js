import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Alert,AsyncStorage,Modal,ActivityIndicator,ListView,FlatList,TouchableHighlight
  
} from 'react-native';
import { StatusBar } from 'react-native';
import { Container, Header, Content, Thumbnail,  } from 'native-base'
import FontAwesome, { Icons } from 'react-native-fontawesome';
import RadioButton from 'radio-button-react-native';
//import SideBar from '../sidebar';
import styles from './RateStyle';
import authData from '../../headerData';
export default class Welcome extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      //9425063536
      dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      currencyList : [],
      sendingCurrencyList : [],
      receivingCurrencyList : [],
      getExRateList : [],
      addCurrency : false,
      sendingCurrency : "AUD",
      sendingCurrencyDes : "Australian dollor",
      addSendingCurrency : false,
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
   this.getCustomerRates();         
   this.getAllSenderCurrency();
   /* AsyncStorage.getItem('currenctList', (err, result) => {
      // console.log(result);
      if(result){
       let cat = JSON.parse(result);
       this.setState({
         // dataSource: this.state.dataSource.cloneWithRows(data)
         currencyList : cat,
        // addCurrency : true
        })
      }else{
        this.getAllCountriesFun();
      }
     });*/
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 async getAllCountriesFun(){
  this.setState({visible:true});
  var authDataToken =authData();
//  console.log(authDataToken);
  try
  { 
    let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/countries', 
      { 
        method: 'GET',
            headers: 
              { 
                 'Accept': 'application/json', 
                 'Content-Type': 'application/json', 
                 'Authorization' : authDataToken,
                }
           }); 
           let res = await response.text(); 
          //console.log(res);
             let result= JSON.parse(res);              
            var jsonResult =  result.Status[0];
              if(jsonResult.errorCode == 1000){
                  var country = result.CountriesInfo;
                 // this.setState({currencyList : country});
                   //this.setState({addCurrency : true});
                   for(var i=0;i<country.length;i++){
                       var key = country[i].Key;
                       var resData= await this.getTaskList(country[i].Key);
                      // console.log(resData);
                       country[i].currenctKey = resData.Key;
                       country[i].currencyValue = resData.Value;
                       country[i].currencyLongName = resData.LongName;
                    }
                    var sl = country;
                    var out = [];
                    for (var i = 0, l = sl.length; i < l; i++) {
                        var unique = true;
                        for (var j = 0, k = out.length; j < k; j++) {
                            if (sl[i].currenctKey === out[j].currenctKey) {
                                unique = false;
                                //console.log('true'+unique);
                            }
                        }
                        if (unique) {
                            out.push(sl[i]);
                        }
                    }
                    this.setState({visible:false});
                  
                    AsyncStorage.setItem('currenctList', JSON.stringify(out), () => {
 
                      AsyncStorage.getItem('currenctList', (err, result) => {
                       console.log(result);
                      });
                   
                  });
               }else if(jsonResult.errorCode == 1002){
                this.setState({visible:false});
               }else{ 
                let error = res;
                throw error;
                } 
            } catch (error) { 
   // console.log('catch');

   this.setState({visible:false});
            }
            }

 getTaskList = async(key) =>{
  var authDataToken =authData();
  const response =await fetch('http://api.remitanywhere.com/restapi/lists/payee/currencies/'+key, {  
method: 'GET',
headers: {
'Accept': 'application/json',
'Content-Type': 'application/json',
'Authorization' : authDataToken,
}
});
const res = await response.json();
//let result= JSON.parse(res); 
//console.log(res.CurrenciesInfo[0]);
return res.CurrenciesInfo[0];
}
              
               async getCurrencyCode(key){
                var authDataToken =authData();
                 //  console.log(key);
                try
                { 
                  let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/currencies/'+key, 
                    { 
                      method: 'GET',
                          headers: 
                            { 
                               'Accept': 'application/json', 
                               'Content-Type': 'application/json', 
                               'Authorization' : authDataToken,
                              
                              }
                         }); 
                         let res = await response.text(); 
                     //   console.log(res);
                       
                           let result= JSON.parse(res); 
                           var jsonResult =  result.Status[0];
                           if(jsonResult.errorCode == 1000){
                            var country = result.CurrenciesInfo[0];
                            return country;
                           }else{
                            let errors = res;
                            throw errors;
                           }
                          
                       
                          } catch (error) { 
               
                  console.log('catch');
             
                          }
                          }
                    
 onSubmit(){
    const { navigate } = this.props.navigation;
    Alert.alert(
        'Message',
        'Are you sure want to logout!',
        [
            {text : 'Cancle', onPress:()=>{console.log('cancel')}},
          {text : 'OK', onPress:()=>{navigate('Forth')}}
        ]
       );
   
  
     
 }
 /*getAllCountries(){

  AsyncStorage.getItem('currenctList', (err, result) => {
    // console.log(result);
    if(result){
     let cat = JSON.parse(result);
     this.setState({
       // dataSource: this.state.dataSource.cloneWithRows(data)
       currencyList : cat,
       addCurrency : true
      })
    }else{
      this.getAllCountriesFun();
    }
   });
 }  */
 async getAllSenderCurrency(){
  var authDataToken =authData();
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/currency/sending', 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.json(); 
                 console.log(res);
                 var jsonResult = res.Status[0];
                 if(jsonResult.errorCode == 1000){
                  var CurrenciesInfo = res.CurrenciesInfo;
                  console.log(CurrenciesInfo);
                  this.setState({ sendingCurrencyList : CurrenciesInfo})
                //  return ExRateInfo;
                  }else{
                   let errors = res;
                   throw errors;
                  }
         
               
  }catch (error) { 
                 console.log(error);
               }
 }
 async getAllReceivingCurrency(){
  var authDataToken =authData();
    try
    { 
        let response = await fetch('http://api.remitanywhere.com/restapi/lists/sender/countries', 
        { 
            method: 'GET',
                headers: 
                { 
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json', 
                    'Authorization' : authDataToken,
                    
                    }
            }); 
            let res = await response.json(); 
                 console.log(res);
                 var jsonResult = res.Status[0];
                 if(jsonResult.errorCode == 1000){
                  var CurrenciesInfo = res.CountriesInfo;
                  console.log(CurrenciesInfo);
                  this.setState({ receivingCurrencyList : CurrenciesInfo,addCurrency : true})
                //  return ExRateInfo;
                  }else{
                   let errors = res;
                   throw errors;
                  }
         
               
  }catch (error) { 
                 console.log(error);
               }
 }
  addSendingCurrencyList(item){
console.log(item);
this.setState({addSendingCurrency : false});
this.setState({sendingCurrency : item.Key, sendingCurrencyDes : item.Value,
  })
 }
 async addCurrencyList(data){
   //console.log('gsgdsgdsgdsgds');
   console.log(data);
   var sendingCurrency = this.state.sendingCurrency;
   var receivingCurrency = data.CurrencyCode;
   var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth()+1; //January is 0!
   
   var yyyy = today.getFullYear();
   if(dd<10){
       dd='0'+dd;
   } 
   if(mm<10){
       mm='0'+mm;
   } 
   var currentDate = mm+'/'+dd+'/'+yyyy;

   console.log(today+'\n'+sendingCurrency+'\n'+receivingCurrency);
   this.setState({addCurrency : false});
  var ExRateInfo = await this.getRates(sendingCurrency,receivingCurrency,currentDate);
  
  console.log(ExRateInfo);
  var ExRate = ExRateInfo.ExRate;
  var CurrencyFrom = ExRateInfo.CurrencyFrom;
  var CurrencyTo = ExRateInfo.CurrencyTo;
  this.saveCustomerRate(CurrencyFrom,CurrencyTo,currentDate,ExRate);
   {/*
  "sendingCurrency":"USD"
	,"receivingCurrency":"GMD"
	,"date":"02/15/2018"
	,"deliveryMethodKey":1
	,"paymentMethodKey":1
	,"payeeLocationKey":1
	,"customerKey":null

  */}
 }
  getRates = async(sendingCurrency,receivingCurrency,currentDate)=>{
  var authDataToken =authData();
  //  console.log(key);
 try
 { 
   let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/rate/remittance/select', 
     { 
       method: 'POST',
           headers: 
             { 
                'Accept': 'application/json', 
                'Content-Type': 'application/json', 
                'Authorization' : authDataToken,
               
               },
               body: JSON.stringify({
                
                "sendingCurrency":sendingCurrency
	             ,"receivingCurrency":receivingCurrency

                ,"date":currentDate
                ,"deliveryMethodKey":1
                ,"paymentMethodKey":1
                ,"payeeLocationKey":1
                ,"customerKey":global.CustomerKey

               })
          }); 
          let res = await response.text(); 
        console.log(res);
        
            let result= JSON.parse(res); 
            var jsonResult =  result.Status[0];
            if(jsonResult.errorCode == 1000){
            var ExRateInfo = result.ExRateInfo[0];
            return ExRateInfo;
            }else{
              let errors = res;
              throw errors;
             }
           
        
           } catch (error) { 

   console.log('catch');

           }
 }
 async saveCustomerRate(sendingCurrency,receivingCurrency,currentDate,ExRate){
   console.log(sendingCurrency,receivingCurrency,currentDate,ExRate);
   var today = new Date();
   var dd = today.getDate()+1;
   var mm = today.getMonth()+1; //January is 0!
   
   var yyyy = today.getFullYear();
   if(dd<10){
       dd='0'+dd;
   } 
   if(mm<10){
       mm='0'+mm;
   } 
    currentDate = mm+'/'+dd+'/'+yyyy;
  var authDataToken =authData();
  //  console.log(key); http://api.remitanywhere.com/restapi/moneytransfer/rate/remittance/select
 try
 { 
   let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/rate/customer/insert', 
     { 
       method: 'POST',
           headers: 
             { 
                'Accept': 'application/json', 
                'Content-Type': 'application/json', 
                'Authorization' : authDataToken,
               
               },
               body: JSON.stringify({
                "CurrencyFrom": sendingCurrency,
                "CurrencyTo": receivingCurrency,
               
                "ExRate": ExRate,
                "StartDate": currentDate
                


               })
          }); 
          let res = await response.text(); 
        console.log(res);
        
            let result= JSON.parse(res); 
            var jsonResult =  result.Status[0];
            if(jsonResult.errorCode == 1000){
           // var ExRateInfo = result.ExRateInfo[0];
          //  return ExRateInfo;
            }else{
              let errors = res;
              throw errors;
             }
           
        
           } catch (error) { 

   console.log('catch');

           }
 }
 async getCustomerRates(){
   // console.log(sendingCurrency,receivingCurrency,currentDate,ExRate);
 var authDataToken =authData();
 //  console.log(key);
try
{ 
  let response = await fetch('http://api.remitanywhere.com/restapi/moneytransfer/rate/customer/select', 
    { 
      method: 'POST',
          headers: 
            { 
               'Accept': 'application/json', 
               'Content-Type': 'application/json', 
               'Authorization' : authDataToken,
              
              },
              body: JSON.stringify({
                "payeeLocationCode": null
	,"paymentMethodCode": null
	,"deliveryMethodCode": null
	,"exRateDate": null
	,"currencyFrom": null
	,"currencyTo": null     
              })
         }); 
         let res = await response.json(); 
       console.log(res);
       
           //let result= JSON.parse(res); 
           var jsonResult =  res.Status[0];
           if(jsonResult.ErrorCode == 1000){
           var ExRates = res.ExRates;
           this.setState({getExRateList : ExRates});
           }else{
            let errors = res;
            throw errors;
           }
          
       
          } catch (error) { 

  console.log('catch');

          }
}
calculateRate(exRate){
  console.log(exRate);
}

  render() {
    const { navigate } = this.props.navigation;
   return (
     <View
       style={styles.container}>
     
        <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
   <Modal 
    style={{paddingHorizontal:'20%',}}
   animationType="fade"
      transparent={true}
      visible={this.state.addCurrency}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    paddingVertical:60,
    marginBottom:50,
    backgroundColor: '#00000040'}} >

<View style={{backgroundColor: '#156a82',
   
    width: 250,
   
    borderTopLeftRadius : 10,
    borderTopRightRadius : 10,
    borderBottomLeftRadius : 10,
    borderBottomRightRadius : 10,
    borderWidth : 1,
    borderColor : '#156a82'   
    }}> 
      <View style={{flexDirection:'row',paddingVertical:15}}>
         <Text style={{flex:0.9,color:'#fff',fontSize:16,textAlign:'center',fontWeight:'bold',paddingLeft:'10%'}}>Add Currency</Text>
        <Text style={{flex:0.1,color:'#fff',fontWeight:'bold',}} onPress={()=>this.setState({addCurrency : false})}> <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
      </View>
       <ScrollView style = {[styles.scrollViewStyle,{ borderBottomLeftRadius : 10,borderBottomRightRadius : 10,borderWidth : 1, borderColor : '#156a82'}]}> 
   <FlatList 
                      data={this.state.receivingCurrencyList}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                      <View>
                         <TouchableHighlight onPress={()=>this.addCurrencyList(item)}>
                      <View style={styles.oddDiv}>
                     
                      <View style={{flex:0.1}}>
                              <Image source={require('../img/Australia.png')} />
                      </View>
                      <View style={{flex:0.8,flexDirection:'column',marginLeft:30,alignSelf:'center'}}>
                            <Text style={{fontSize:16,color:'#5b6553'}}>{item.CurrencyCode}</Text>
                            <Text style={{color:'#5b6553'}}>{item.Currency}</Text>                 
                      </View>
                    
                  </View>
                  </TouchableHighlight>
                    
                  </View>
                      }/>
                      </ScrollView>
                      </View>
                      </View>
   </Modal>
   <Modal 
    style={{paddingHorizontal:'20%',}}
   animationType="fade"
      transparent={true}
      visible={this.state.addSendingCurrency}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    paddingVertical:60,
    backgroundColor: '#00000040'}} >

<View style={{backgroundColor: '#156a82',
   
    width: 250,
   
    borderTopLeftRadius : 10,
    borderTopRightRadius : 10,
    borderWidth : 1,
    borderColor : '#156a82'   
    }}> 
      <View style={{flexDirection:'row',paddingVertical:15}}>
         <Text style={{flex:0.9,color:'#fff',fontSize:16,textAlign:'center'}}>Select Currency</Text>
        <Text style={{flex:0.1,color:'#fff',fontWeight:'bold',}} onPress={()=>this.setState({addSendingCurrency : false})}> <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
      </View>
       <ScrollView style = {styles.scrollViewStyle}> 
   <FlatList 
                      data={this.state.sendingCurrencyList}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                      <View>
                         <TouchableHighlight onPress={()=>this.addSendingCurrencyList(item)}>
                      <View style={styles.oddDiv}>
                     
                      <View style={{flex:0.1}}>
                              <Image source={require('../img/Australia.png')} />
                      </View>
                      <View style={{flex:0.8,flexDirection:'column',marginLeft:30,alignSelf:'center'}}>
                            <Text style={{fontSize:16,color:'#5b6553'}}>{item.Key}</Text>
                            <Text style={{color:'#5b6553'}}>{item.Value}</Text>                 
                      </View>
                    
                  </View>
                  </TouchableHighlight>
                    
                  </View>
                      }/>
                      </ScrollView>
                      </View>
                      </View>
   </Modal>
       <View
         style={styles.imageContainer}>
        {/* <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         /> */}
       </View>
       <View style={styles.textContainer}>
        <View style={{height:40,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Rates</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>this.getAllReceivingCurrency()}> <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.plus}</FontAwesome></Text>
            </View>
        </View>
       {/* <View style={styles.logoContainer}>
         <Image source ={require('../img/app-logo-small.png')} />
         </View> */}
       <View style={{paddingHorizontal:15,flexDirection:'row',paddingVertical:15}}>
       <Text style={{flex:0.5,backgroundColor:'#5b6553',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,marginRight:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#5b6553'}}>Rates</Text>
       <Text style={{flex:0.5,backgroundColor:'#F06124',color:'#fff',fontWeight:'600',fontSize:16,paddingVertical:10,textAlign:'center',borderRadius:4,borderWidth:1, overflow:"hidden",borderColor:'#F06124'}} onPress={()=>navigate('FeeCalculator')}>Calculator</Text>
       </View>
       <View style={styles.oddDiv}>
           <View style={{flex:0.1}}>
                   <Image source={require('../img/Australia.png')} />
           </View>
           <View style={{flex:0.4,flexDirection:'column',marginLeft:30,alignSelf:'center'}}>
          
          {/* <Text style={{fontSize:16,color:'#5b6553'}}>{this.state.sendingCurrency}</Text>
                 <Text style={{color:'#5b6553'}} >{this.state.sendingCurrencyDes}</Text>    */}    
                 <Text style={{fontSize:16,color:'#5b6553'}} onPress={()=>this.setState({addSendingCurrency : true})}>{this.state.sendingCurrency}</Text>
                 <Text style={{color:'#5b6553'}} onPress={()=>this.setState({addSendingCurrency : true})}>{this.state.sendingCurrencyDes}</Text>      
                          
           </View>
           <View style={{flex:0.5,flexDirection:'column',alignSelf:'center'}}>
                           <TextInput
                            placeholder="Amount" style={[styles.InputText,this.state.passwordError&&styles.errorInput]} 
                            underlineColorAndroid = "transparent"  
                            keyboardType = { "numeric" }
                           autoCapitalize="none"
                           autoCorrect={false}
                           placeholderTextColor='#fff'
                           returnKeyType={"done"}
                             onChangeText={(text)=>this.exAmountFun(text)}
                             value={this.state.amount}
                           ></TextInput>
                      </View>
       </View>
       <View style={styles.secondDiv}>
         <Text style={{color:'#fff',paddingVertical:8,textAlign:'right',flex:1,marginRight:15}}>Customers Rate</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}> 
   <FlatList 
                      data={this.state.getExRateList}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                      <View>
                         
                      <View style={styles.exRateDiv}>
                     
                      <View style={{flex:0.1}}>
                             
                      </View>
                      <View style={{flex:0.4,flexDirection:'column',marginLeft:30,alignSelf:'center'}}>
                            <Text style={{fontSize:16,color:'#fff'}}>{item.CurrencyTo}</Text>
                                           
                      </View>
                      <View style={{flex:0.5,flexDirection:'column',alignSelf:'center'}}>
                      <Text style={{color:'#fff',textAlign:'center'}}>{this.calculateRate(item.ExRate)}</Text>  
                      </View>
                  </View>
                 
                    
                  </View>
                      }/>
                      </ScrollView>
       <View style={{paddingHorizontal : 25,paddingVertical:10,marginTop:'80%'}}>
         <Text style={styles.next} >Transfer Now </Text>
         </View>
       </View>
     </View>
   );
 }
}
