import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({

    container : {
      flex: 1,
      backgroundColor: '#eee',
      flexDirection:'column',
      marginTop:20,
    },
    imageContainer : {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    textContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    
    
    },
    logoContainer: { alignItems: 'center',paddingVertical:35},
    logoText:{color:'#fff',fontSize:12,textAlign:'center',},
    next : {
    
    backgroundColor:'#F06124',
    borderRadius: 18,
    paddingVertical : 10,
    textAlign:'center',
    color:'#fff',
    overflow:"hidden",
    fontWeight:'bold',
    },
    bottomInstructions : {
    color:'#fff',
    fontSize:14,
    textAlign:'center',
    paddingVertical:25,
    paddingHorizontal:10,
    marginTop:'40%',
    marginBottom:'5%',
    },
    InputText : {
      paddingVertical : 10,
      borderRadius:3,
      borderColor: '#5b6553',
       borderWidth: 1,
       paddingHorizontal: 5,
       backgroundColor:'#5b6553',
       textAlign:'center',
       color:'#ffffff',
      
    },
    radioText : {
      paddingHorizontal:7,
     color:'#000',
      justifyContent : 'center',
    },
    scrollViewStyle : {
      paddingVertical:'1%',
    },
    oddDiv : {
       
        backgroundColor:'#fff',
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:8
    },
    exRateDiv : {
        backgroundColor:'#5b6553',
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:8
        
    },
    secondDiv : {
        marginTop : 0,
        backgroundColor:'#F06124',
        flexDirection:'row',
        paddingHorizontal:10,
        
    }
    });
    export default styles;