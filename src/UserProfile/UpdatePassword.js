import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Modal,ActivityIndicator,Alert,Keyboard,KeyboardAvoidingView,AlertIOS,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import authData from '../../headerData';
export default class UpdatePassword extends Component {
    constructor(props){
    super(props);
    global.count = 0;
    this.onChangeText = this.onChangeText.bind(this);
    this.onCountryChange = this.onCountryChange.bind(this);
    this.typographyRef = this.updateRef.bind(this, 'typography');
    this.countryRef = this.updateCountryRef.bind(this, 'typography');
    this.onChangeDeliveryMethode = this.onChangeDeliveryMethode.bind(this);
    this.onChangeDeliveryCurrency = this.onChangeDeliveryCurrency.bind(this);
    this.typographyCurrencyRef = this.updateCuurencyRef.bind(this,'typography');
   
    this.MethodRef = this.updateMethodRef.bind(this,'typography');
    this.state ={
      accountType : 'Individual',
      visible: false,
      sendFrom : 'Australia - AUD',
      sendTo : 'India',
      deliverIn : 'INR',
      amount : '',
      result : false,
      validAmountErr:false,
      amountCheck:false,
      senderCountry : [],
      deliveryMethod : [],
      delievryCurrency : [],
      oldMsgError : false,
      newMsgError : false,
      oldPassword : '',
      newPassword :'',
      isSucess : false,
      cNewPassword : '',
      newPError : false,
      isError : false
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
    //this.getSenderCountry();
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 onChangeText(text) {
  console.log(text);
  this.setState({sendFrom:text});
 // this.getSenderState(text)
 var currency = this.getPayeeCurrency(text);
 this.getDeliveryMethodeFun(text);
 console.log(currency);
 }
 onCountryChange(text){
  console.log(text);
  this.setState({sendTo:text});
     
 }
 updateRef(name, ref) {
   this[name] = ref;
 }
 updateCuurencyRef(name, ref) {
  this[name] = ref;
}
 onChangeDeliveryMethode(text){
  console.log(text);
  this.setState({deliveryMethodSelect:text});
  //this.getbankName();
 }
 onChangeDeliveryCurrency(text) {
  console.log(text);
  this.setState({deliverIn:text});
//  this.getSenderState(text)
 }
 updateMethodRef(name, ref) {
  this[name] = ref;
}
 updateCountryRef(name, ref) {
  this[name] = ref;
}
 onSubmit(){
//   var isOld = false;
//   var isNew = false;
//   var isSame = false;
//   var iscNew = false;
//   var oldPassword = this.state.oldPassword;
//   var newPassword = this.state.newPassword;
//   if(this.state.oldPassword.length>=8){
//     isOld = false;
//     this.setState({oldMsgError : false});
//     this.setState({isShowError:false});
//     console.log('old True'+isOld);
//   }else{
//     isOld = true;
//     this.setState({errorOldMsg : 'Old Password must be contain 8 charactor'});
//     this.setState({oldMsgError : true});
//     this.setState({isShowError:true});
//     console.log('Old Password must be contain 8 charactor'+isOld);
// }
//   if(this.state.newPassword.length>=8){
//        isNew = false;
//        this.setState({newMsgError : false});
//        this.setState({isShowError:false});
//        console.log('New True'+isNew);
//   }else{
//        isNew = true;
//        this.setState({errorMsg : 'New Password must be contain 8 charactor'});
//        this.setState({newMsgError : true});
//        this.setState({isShowError:true});
//        console.log('New Password must be contain 8 charactor'+isNew);
//   }
//   if(this.state.cNewPassword==this.state.newPassword){
//       iscNew = false;
//       this.setState({newPError : false});
//       this.setState({isShowError:false});
//       console.log('New CTrue'+isNew);
//   }else{
//       iscNew = true;
//       this.setState({errorMsg : 'Password and confirm must be same!'});
//       this.setState({newPError : true});
//       this.setState({isShowError:true});
//       console.log('Password and confirm must be same!'+iscNew);
// }
//   if(!isOld) {
//     if(!isNew){
//         if(!iscNew){
//           this.changePasswordFun();
//         }
//       //alert('success');
//     }
//   }else{

//   }
var oldPassword = this.state.oldPassword;
var newPassword = this.state.newPassword;
if(oldPassword==''){
    this.setState({errorMsg : 'Please enter required field'});
    this.setState({isError : true});
    this.setState({isShowError:true});
    this.setState({oldMsgError : true});
}else if(oldPassword.length <= 7){
  console.log(oldPassword.length);
  this.setState({errorMsg : 'Old password must consist of at least 8 characters (max: 15)'});
  this.setState({isError : true});
  this.setState({isShowError:true});
  this.setState({oldMsgError : true});
  this.setState({oldPassword:''});
  }else if(newPassword== ''){
    console.log('if');
    this.setState({errorMsg : 'Please enter required field'});
    this.setState({isError : true});
    this.setState({oldMsgError : false,newMsgError : true});
    }else if(newPassword.length <= 7){
      console.log('newPassword'+newPassword.length);
      this.setState({errorMsg : 'New password must consist of at least 8 characters (max: 15)'});
      this.setState({isError : true});
      this.setState({isShowError:true});
      this.setState({oldMsgError : false,newMsgError : true});
      }else if(this.state.cNewPassword== ''){
        console.log('if');
        this.setState({errorMsg : 'Please enter required field'});
        this.setState({isError : true});
        this.setState({newPError:true});
        this.setState({oldMsgError : false,newMsgError : false});
        }else if(this.state.cNewPassword!=this.state.newPassword){
        this.setState({errorMsg : 'New Password and confirm password must be same!'});
        this.setState({isError : true});
        this.setState({isShowError:true});
        this.setState({newPError:true});
        }else{
      this.setState({isError : false});
      this.setState({oldMsgError : false,newMsgError : false,newPError:false});
      this.changePasswordFun();
  }
}
  async changePasswordFun(){
  
    var authDataToken =authData();
   console.log(authDataToken);
    try
    { 
      this.setState({visible:true});
      let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/changepassword', 
        { 
          method: 'POST',
              headers: 
                { 
                   'Accept': 'application/json', 
                   'Content-Type': 'application/json', 
                   'Authorization' : authDataToken,
                  
                  },
                  body: JSON.stringify({
                  
                    "customerKey":global.CustomerKey
                    ,"customerOldPWD":this.state.oldPassword
                    ,"customerNewPWD":this.state.newPassword
                    ,"securityPinNumber":null
                  
                  })
             }); 
             let res = await response.text(); 
             this.setState({visible:false});
            console.log(res);
          
            //this.setState({visible:false});
               //console.log(response.status);
               let result= JSON.parse(res); 
              // console.log(result.Status);
              var jsonResult =  result.Status['0'];
              //console.log(jsonResult);
             
              // alert(JSON.stringify(result));
                if(jsonResult.errorCode == 1000){
                       this.setState({oldPassword : '',newPassword : '',cNewPassword:''})
                       this.setState({visible:false});
                       console.log(jsonResult.errorMessage);
                     // this.setState({isSucess:true});
                     global.setTimeout(() => {
                      AlertIOS.alert(
                        'Success',
                        'Password Updated Successfully !',
                        [
                        {text : 'OK', onPress:()=>{this.goToLogin()}}
                        ]
                       );
                    }, 50);
                 }else if(jsonResult.errorCode == 1002){
                  this.setState({visible:false});
                   console.log(jsonResult.errorMessage);
                      this.setState({oldMsgError:true});
                     this.setState({errorOldMsg : jsonResult.errorMessage});
                       
                   
                 }else{ 
                  this.setState({visible:false});
                  let error = res;
                  throw error;
                  } 
           
              } catch (error) { 
                this.setState({visible:false});
      //this.setState({error: error});
      console.log('catch');
      console.log(JSON.stringify(error));
      
     // console.log(JSON.parse(error));
              }
              }
              pressNav(){
                Keyboard.dismiss();
                this.props.navigation.navigate("DrawerOpen");
              }
              goToLogin(){
                const { navigate } = this.props.navigation;
                navigate('FeeCalculator')
              }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <KeyboardAvoidingView
       style={styles.container} behavior="padding">
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       <View
         style={styles.imageContainer}>
            <Image
              style={{
                flex: 1,
              }}
              source ={require('../img/bgImage.png')} 
            />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Password Change</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('FeeCalculator')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <ScrollView style = {styles.scrollViewStyle}  keyboardShouldPersistTaps="never">
        <View style={styles.logoContainer}>
         <Image
           source ={require('../img/app-logo-small.png')} 
         />
         
         </View>
        
         
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       {this.state.isSucess && <View style={{paddingHorizontal : 25,paddingVertical:6,}}>
       <Text style={{color:'#fff',fontWeight:'bold'}}>Password Updated Successfully !</Text>
       </View>}
       {/* {this.state.oldMsgError&&<View style={{paddingHorizontal : 25,paddingVertical:6,}}>
       <Text style={styles.errorText}>{this.state.errorOldMsg}</Text>
       </View>}
       {this.state.newMsgError&&<View style={{paddingHorizontal : 25,paddingVertical:6,}}>
       <Text style={styles.errorText}>{this.state.errorMsg}</Text>
       </View>}
       {this.state.newPError&&<View style={{paddingHorizontal : 25,paddingVertical:6,}}>
       <Text style={styles.errorText}>{this.state.errorMsg}</Text>
       </View>} */}
       {this.state.isError&&<View style={{paddingHorizontal : 25,paddingVertical:6,}}>
       <Text style={styles.errorText}>{this.state.errorMsg}</Text>
       </View>}
       <View style={{paddingHorizontal : 25,paddingVertical:10,flexDirection:'row'}}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Enter Old Password" style={[styles.InputText,this.state.oldMsgError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
     autoFocus={true}
     secureTextEntry={true}
      onChangeText={(text)=>this.setState({oldPassword:text})}
      value={this.state.oldPassword}
      onSubmitEditing={()=>this.passwordInput.focus()}
     
       ></TextInput>
        </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,flexDirection:'row'}}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Enter New Password" style={[styles.InputText,this.state.newMsgError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry={true}
      onChangeText={(text)=>this.setState({newPassword:text})}
      value={this.state.newPassword}
      onSubmitEditing={()=>this.nPasswordInput.focus()}
      ref={(input)=>this.passwordInput=input}
       ></TextInput>
       </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>  
         <View style={{paddingHorizontal : 25,paddingVertical:10,flexDirection:'row'}}>
         <View style={{flex:1}}>
         <TextInput
       placeholder="Confirm New Password" style={[styles.InputText,this.state.newPError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry={true}
      onChangeText={(text)=>this.setState({cNewPassword:text})}
      value={this.state.cNewPassword}
      ref={(input)=>this.nPasswordInput=input}
       ></TextInput>
       </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
       </View>
       </ScrollView>
       
       </View>
       <View style={{paddingHorizontal : 30,paddingVertical:10,marginBottom:10,paddingTop:20}}>
         <Text style={styles.next} onPress={()=>{this.onSubmit()}}>Update Password</Text>
         </View>
     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',
},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {
  backgroundColor:'#F06124',
  borderRadius: 4,
  paddingVertical : 18,
  textAlign:'center',
  color:'#fff',
  overflow:"hidden",
  fontWeight:'bold',
  fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
   paddingVertical : 10,
   borderRadius:3,
   borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 5,
    fontSize:16
  },
  stickDiv : {
    color:'red',fontSize:22,paddingLeft:5
  },
});