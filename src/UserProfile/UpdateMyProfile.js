import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,KeyboardAvoidingView,AsyncStorage,Modal,ActivityIndicator,Keyboard,Alert,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import authData from '../../headerData';
import RNPickerSelect from 'react-native-picker-select';
export default class UpdateMyProfile extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      firstNameError : false,
      lastNameError : false,
      areaError : false,
      addressError : false,
      postCodeError : false,
      phoneNumberError : false,
      isError:false,
      errorMsg : "",
      firstName : '',
      lastName : '',
      area : '',
      postCode :'',
      phoneNumber:'',
      address:'',
      businessName : '',
      director : '',
      acn : '',
      abn :'',
      ContactNumber:'',
      addressBusiness:'',
      businessNameError : false,
      directorError : false,
      acnError : false,
      abnError : false,
      ContactNumberError : false,
      addressBusinessError : false,
      ErrorMessageView : false,
      senderCountry : [],
      country : '',
      id1ExpiryDate :'',
      customerID1No : ''
      ,customerIDType : ''
      
    } 
  }
  componentDidMount() {
  //  StatusBar.setHidden(true);
  this.getSenderCountry();
  this.getUserDetails();
  
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
   if(value=='Individual'){
    this.setState({isCompany : false});

   }else{
    this.setState({isCompany : true});

   }
 }
 onSubmit1(){
  global.firstName = this.state.firstName;
  global.lastName = this.state.lastName;
  global.middleName = this.state.lastName;
  global.addressLine1 = this.state.addressLine1;
  global.addressLine2 = this.state.addressLine2;
  if(this.state.accountType=='Individual'){
    global.isCompany = false;
     global.businessName = "";
    global.businessLicenseNumber = "";
  }else{
    global.isCompany = true;
    global.businessName = this.state.businessName;
    global.businessLicenseNumber = this.state.businessLicenseNumber;
  }
  global.countryKey = this.state.countryKey;
  const { navigate } = this.props.navigation;
                     navigate('Sixth');
 }
 onIndivisualSubmit(){
var firstName = this.state.firstName;
var lastName = this.state.lastName;
var address = this.state.address;
var postCode = this.state.postCode;
var area = this.state.area;
var phoneNumber = this.state.phoneNumber;
var country = this.state.country;
if(firstName==''){
    this.setState({firstNameError:true,errorMsg:'Name is required!',isError:true})
}else if(lastName==''){
    this.setState({firstNameError:false,errorMsg:'Surname is required!',lastNameError:true,isError:true})
    
}else if(country==''){
  this.setState({lastNameError:false,errorMsg:'Country  must be  required!',isError:true})

}else if(address==''){
    this.setState({lastNameError:false,errorMsg:'Address is required!',addressError:true,isError:true})
    
}else if(postCode==''){
    this.setState({addressError:false,errorMsg:'PostCode is required!',postCodeError:true,isError:true})
    
}else if(postCode==null){
  this.setState({addressError:false,errorMsg:'PostCode is required!',postCodeError:true,isError:true})
  
}else if(area==''){
    this.setState({postCodeError:false,errorMsg:'Area is required!',areaError:true,isError:true})
    
}else if(phoneNumber==''){
    this.setState({areaError:false,errorMsg:'Phone Number is required!',phoneNumberError:true,isError:true})
    
}else if(phoneNumber==null){
  this.setState({areaError:false,errorMsg:'Phone Number is required!',phoneNumberError:true,isError:true})
  
}else if(phoneNumber.length>=15){
  this.setState({areaError:false,errorMsg:'Phone number should be max length 15!',phoneNumberError:true,isError:true})
  
}else{
    this.setState({areaError:false,errorMsg:'Phone Number is required!',phoneNumberError:false,isError:false})
    
    var isCompany = 0;
    var businessName ='';
                          var id1ExpiryDate= this.state.id1ExpiryDate;
                          var customerID1No =  this.state.customerID1No;
                          var customerIDType= this.state.customerIDType;
    this.onSubmit(firstName,lastName,address,area,postCode,phoneNumber,isCompany,businessName,country,id1ExpiryDate,customerID1No,customerIDType);
}

 }
 onCompanySubmit(){
  
    var businessName = this.state.businessName;
    var director = this.state.director;
    var acn = this.state.acn;
    var abn = this.state.abn;
    var ContactNumber = this.state.ContactNumber;
    var addressBusiness = this.state.addressBusiness;
    var country = this.state.country;
    if(businessName==''){
        this.setState({businessNameError:true,errorMsg:'Business Name is required!',isError:true})
    }else if(director==''){
        this.setState({businessNameError:false,errorMsg:'Director is required!',directorError:true,isError:true})
        
    }else if(country==''){
      this.setState({businessNameError:false,errorMsg:'Country  must be  required!',isError:true})
    
    }else if(addressBusiness==''){
        this.setState({directorError:false,errorMsg:'Address is required!',addressBusinessError:true,isError:true})
        
    }else if(abn==''){
        this.setState({addressBusinessError:false,errorMsg:'ABN is required!',abnError:true,isError:true})
        
    }else if(acn==''){
        this.setState({abnError:false,errorMsg:'ACN is required!',acnError:true,isError:true})
        
    }else if(ContactNumber==''){
        this.setState({acnError:false,errorMsg:'Contact Number is required!',contactNumberError:true,isError:true})
        
    }else{
        this.setState({contactNumberError:false,errorMsg:'Phone Number is required!',phoneNumberError:false,isError:false})
        
        console.log('submit',businessName,director,abn,acn,ContactNumber,addressBusiness);
        var isCompany = 1;
       var area ='';
       var postCode = '';
       var id1ExpiryDate= this.state.id1ExpiryDate;
       var customerID1No =  this.state.customerID1No;
       var customerIDType= this.state.customerIDType;
       this.onSubmit(director,director,addressBusiness,area,postCode,ContactNumber,isCompany,businessName,country,id1ExpiryDate,customerID1No,customerIDType);
    }
    
     }
     async getSenderCountry(){
      var authDataToken =authData();
      this.setState({visible:true});
           try
           { 
               let response = await fetch('http://api.remitanywhere.com/restapi/lists/payee/countries', 
               { 
                   method: 'GET',
                       headers: 
                       { 
                           'Accept': 'application/json', 
                           'Content-Type': 'application/json', 
                           'Authorization' : authDataToken,
                           
                           }
                   }); 
                   let res = await response.text(); 
                       // console.log(res);
                       let result= JSON.parse(res); 
                       var jsonResult =  result.Status[0];
                       if(jsonResult.errorCode == 1000){
                         this.setState({visible:false});
                               var country = result.CountriesInfo;
                               var res = [];
                               for(var i=0;i<country.length;i++){ 
                                   res.push({value : country[i].Key,label :country[i].Value });
                               }  
                               console.log(res);
                               this.setState({senderCountry : res});
                       }else{
                         this.setState({visible:false});
                         let errors = res;
                         throw errors;
                       }
       }catch (errors) { 
   
         this.setState({visible:false});
                        console.log('catch');
                        console.log(JSON.stringify(errors))
                      }
    }
     async onSubmit(firstName,lastName,address,cityName,postCode,cNumber,isCompany,businessName,country){
         this.setState({visible:true});
         var authDataToken =authData();
         /* this.setState({id1ExpiryDate : CustomerInfo.id1ExpiryDate});
                                    this.setState({customerID1No : CustomerInfo.customerID1No});
                                    this.setState({customerIDType : CustomerInfo.customerIDType}); */
         try
         { 
           let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/update', 
             { 
               method: 'POST',
                   headers: 
                     { 
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json', 
                        'Authorization' : authDataToken,                       
                       },
                       body: JSON.stringify({
                        customerKey : global.CustomerKey,
                           "customerLoginId": global.customerLoginId,
                           "firstName": firstName,
                           "lastName": lastName,
                           "addressLine1": address,
                           "cityName": cityName,
                           "zipCode": postCode,
                           "landlineNo": cNumber,
                           "mobileNo": cNumber,
                           "emailAddress": global.CustomerID,
                           "isCompany": isCompany,
                           "businessName": businessName, 
                           "countryKey" : country,
                           "ID1ExpiryDate" : this.state.id1ExpiryDate,
                           "CustomerID1No" : this.state.customerID1No,
                           "CustomerID1Tp" : this.state.customerIDType
                       })
                  }); 
                    this.setState({visible:false});
                    let res = await response.text();              
                    console.log(res);
                    this.setState({visible:false});
                    let result= JSON.parse(res);               
                    var jsonResult =  result.Status['0'];
                   if(jsonResult.ErrorCode == 1000){
                 
                   global.setTimeout(() => {
                    Alert.alert(
                      'Success',
                      'Your Profile Updated Successfully!',
                      [
                      {text : 'OK', onPress:()=>{this.goMyProfile()}}
                      ]
                     );
                  }, 50);
               
                    }else if(jsonResult.ErrorCode == 1001){
                      this.setState({ErrorMessageView:true});
                      alert(JSON.stringify(jsonResult));
                      this.setState({ErrorMessage:jsonResult.ErrorMessage});
                    }else if(jsonResult.ErrorCode == 1003){
                      this.setState({ErrorMessageView:true});
                      alert(JSON.stringify(jsonResult));
                      this.setState({ErrorMessage:jsonResult.ErrorMessage});
                    }else if(jsonResult.ErrorCode == 10001){
                      alert(JSON.stringify(jsonResult));
                      this.setState({ErrorMessageView:true});
                      this.setState({ErrorMessage:jsonResult.ErrorMessage});
                    }else{ 
                     let error = res;
                     throw error;
                     } 
                   } catch (error) { 
           console.log('catch');
           console.log(error);
           alert(JSON.stringify(error));
          }
          }
          goMyProfile(){
            const { navigate } = this.props.navigation;
    navigate('MyProfile');
          }
          async getUserDetails(){
            this.setState({visible:true});
            var authDataToken =authData();
                 try
                 { 
                     let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/'+global.CustomerKey, 
                     { 
                         method: 'GET',
                             headers: 
                             { 
                                 'Accept': 'application/json', 
                                 'Content-Type': 'application/json', 
                                 'Authorization' : authDataToken,
                                 
                                 }
                         }); 
                         let res = await response.text(); 
                             console.log(res);
                             let result= JSON.parse(res); 
                             var jsonResult =  result.Status[0];
                             if(jsonResult.errorCode == 1000){
                                     var CustomerInfo = result.CustomerInfo[0];
                                     var FirstName = CustomerInfo.FirstName=='null'?'':CustomerInfo.FirstName;
                                    var LastName =  CustomerInfo.LastName=='null'?'':CustomerInfo.LastName;
                                    var MiddleName= CustomerInfo.MiddleName=='null'?'':CustomerInfo.MiddleName;
                                    console.log(CustomerInfo.CountryKey);
                                    this.setState({country : CustomerInfo.CountryKey});
                                    this.setState({firstName:CustomerInfo.FirstName})
                                    this.setState({lastName:CustomerInfo.LastName})
                                    this.setState({address:CustomerInfo.AddressLine1})
                                    this.setState({postCode:CustomerInfo.ZipCode})
                                   this.setState({phoneNumber:CustomerInfo.MobileNo})
                                    this.setState({area:CustomerInfo.CityName})
                                   this.setState({id1ExpiryDate : CustomerInfo.ID1ExpiryDate});
                                   this.setState({customerID1No : CustomerInfo.CustomerID1No});
                                    this.setState({customerIDType : CustomerInfo.CustomerID1Tp});
                                    
                                    console.log(this.state.id1ExpiryDate);
                                    console.log(this.state.customerID1No);
                                      console.log(this.state.customerIDType);
                                    if(CustomerInfo.isCompany){
                           
                                        var businessName = this.state.businessName;
                                        var director = this.state.director;
                                        var acn = this.state.acn;
                                        var abn = this.state.abn;
                                        var ContactNumber = this.state.ContactNumber;
                                        var addressBusiness = this.state.addressBusiness;
                                        this.setState({addressBusiness:CustomerInfo.AddressLine1})
                                        this.setState({businessName:CustomerInfo.BusinessName})
                                        this.setState({abn:CustomerInfo.AddressLine1})
                                        this.setState({acn:CustomerInfo.AddressLine1})
                                        this.setState({director:CustomerInfo.FirstName})
                                        this.setState({ContactNumber : CustomerInfo.MobileNo})
                                    }

                                   // this.setState({area:CustomerInfo.FirstName})
                                    /*ar firstName = this.state.firstName;
var lastName = this.state.lastName;
var address = this.state.address;
var postCode = this.state.postCode;
var area = this.state.area;
var phoneNumber = this.state.phoneNumber;
var country = this.state.country; */
                                     global.userName = FirstName+' '+LastName;
                                     console.log(global.userName);
                                     this.setState({CustomerInfo : result.CustomerInfo})
                                     console.log(this.state.CustomerInfo);
                                     this.setState({visible:false});
                             }else{
                                let errors = await response;
                                throw errors;
                             }
             }catch (errors) { 
              this.setState({visible:false});
                              console.log('catch');
                              console.log(JSON.stringify(errors));
                            }
          }
          goToLogin(){
            const { navigate } = this.props.navigation;
            navigate('Forth');
          }
          async sendActivationLink(to,msg){
            var authDataToken =authData();
             console.log(authDataToken);
             try
             { 
               let response = await fetch('http://api.remitanywhere.com/restapi/misc/sendemail', 
                 { 
                   method: 'POST',
                       headers: 
                         { 
                            'Accept': 'application/json', 
                            'Content-Type': 'application/json', 
                            'Authorization' : authDataToken,                  
                           },
                           body: JSON.stringify({
                            "emailTo": to
                            ,"emailSubject":"Activate Your Account With BFX Australia"
                            ,"emailBody": msg                                     
                           })
                      }); 
                      let res = await response.text(); 
                   //  console.log(res);
                     this.setState({visible:false});
                      let result= JSON.parse(res);         
                     var jsonResult =  result.Status['0'];      
                       if(jsonResult.ErrorCode == 1000){
                        global.setTimeout(() => {
                          Alert.alert(
                            'Success',
                            'You have successfully created your account with BFX Australia. Shortly you will receive a confirmation email containing important information related to your account.',
                            [
                            {text : 'OK', onPress:()=>{this.goToLogin()}}
                            ]
                           );
                        }, 50);
                               AsyncStorage.setItem("CustomerKey", "");
                               AsyncStorage.setItem("CustomerID", "");
                               global.CustomerKey = null;    
                        }else if(jsonResult.ErrorCode == 1001){
                          console.log('elseIf'); 
                        }else{ 
                          console.log('else');
                         let error = res;
                         throw error;
                         } 
                       } catch (error) { 
               console.log('catch');
              }
          }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
    <KeyboardAvoidingView
    style={styles.container}
    behavior="padding" >
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
       
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15}} onPress={()=>navigate('MyProfile')}><FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>Update Profile</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('MyProfile')}><FontAwesome style={{fontSize: 22,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Personal Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}>
       {this.state.isError&&<View style={{paddingHorizontal:25,}}>
           <Text style={styles.errorText}>
                {this.state.errorMsg}
           </Text>
       </View>}
       {this.state.ErrorMessageView==true && <Text style={[styles.errorText,{paddingHorizontal : 25,paddingVertical:10,fontSize:14}]}>{this.state.ErrorMessage}</Text>}
       
       <View style={{flexDirection:'row',backgroundColor:'transparent',alignItems:'center',paddingLeft:25,marginTop:15,paddingRight:35}}>
             <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,paddingLeft:15,borderTopLeftRadius:3,borderBottomLeftRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' outerCircleColor ='#F06124' value={'Individual'} onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Individual</Text>
                 </RadioButton></View>
                 <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,borderTopRightRadius:3,borderBottomRightRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' value={'Company'} outerCircleColor ='#F06124' onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Company</Text>
                 </RadioButton></View>
       </View>
       {!this.state.isCompany&& <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onSubmitEditing={()=>this.lastNameInput.focus()}
      onChangeText={(text) => this.setState({firstName:text})}
      value={this.state.firstName}
      autoFocus={true}
       ></TextInput>
        </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
        
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Surname" style={[styles.InputText,this.state.lastNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.lastNameInput=input}
       onSubmitEditing={()=>this.addressInput.focus()}
      onChangeText={(text) => this.setState({lastName:text})}
      value={this.state.lastName}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>

     
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
     <RNPickerSelect
            placeholder={{
              label: 'Select country ',
              value: '',
            }}
          items={this.state.senderCountry}
          onValueChange={
          (item) => {
            this.setState({
              country: item,
              
            });
            console.log(item);
           
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.country}
         
        />
     </View>
     <Text style={styles.stickDiv}>*</Text>
         </View>
     <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Address" style={[styles.InputText,this.state.addressError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({address:text})}
       ref={(input)=>this.addressInput=input}
       onSubmitEditing={()=>this.postcodeInput.focus()}
       value={this.state.address}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Postcode" style={[styles.InputText,this.state.postCodeError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="numeric"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.postcodeInput=input}
       onSubmitEditing={()=>this.areaInput.focus()}
       onChangeText={(text) => this.setState({postCode:text})}
       value={this.state.postCode}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
         </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Area" style={[styles.InputText,this.state.areaError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.areaInput=input}
       onSubmitEditing={()=>this.phoneInput.focus()}
       onChangeText={(text) => this.setState({area:text})}
       value={this.state.area}
       ></TextInput>
         </View>
          <Text style={styles.stickDiv}>*</Text>
          </View>
          <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Phone Number" style={[styles.InputText,this.state.phoneNumberError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="numeric"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.phoneInput=input}
       value={this.state.phoneNumber}
       onChangeText={(text) => this.setState({phoneNumber:text})}
       ></TextInput>
         </View>
          <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={(e)=>{this.onIndivisualSubmit()}}>Update Profile </Text>
         </View>
       </View> }
       {this.state.isCompany&& <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={styles.inputDiv}>
       <View style={{flex:1}}>
       <TextInput
       placeholder="Business Name" style={[styles.InputText,this.state.businessNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({businessName:text})}
       autoFocus={true}
       value={this.state.businessName}
       onSubmitEditing={()=>this.directorInput.focus()}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Director" style={[styles.InputText,this.state.directorError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.directorInput=input}
      onSubmitEditing={()=>this.address1Input.focus()}
      value={this.state.director}
      onChangeText={(text) => this.setState({director:text})}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
     <RNPickerSelect
            placeholder={{
              label: 'Select country ',
              value: '',
            }}
          items={this.state.senderCountry}
          onValueChange={
          (item) => {
            this.setState({
              country: item,
              
            });
            console.log(item);
           
           }
        }
          
          style={{ ...pickerSelectStyles }}
          value={this.state.country}
         
        />
     </View>
     <Text style={styles.stickDiv}>*</Text>
          </View>
     <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Address" style={[styles.InputText,this.state.addressBusinessError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.address1Input=input}
       onSubmitEditing={()=>this.abnInput.focus()}
       onChangeText={(text) => this.setState({addressBusiness:text})}
       value={this.state.addressBusiness}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="ABN" style={[styles.InputText,this.state.abnError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({abn:text})}
       ref={(input)=>this.abnInput=input}
       onSubmitEditing={()=>this.acnInput.focus()}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="ACN" style={[styles.InputText,this.state.acnError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({acn:text})}
       ref={(input)=>this.acnInput=input}
       onSubmitEditing={()=>this.contactInput.focus()}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={styles.inputDiv}>
       <View style={{flex:1}}>
         <TextInput
       placeholder="Contact Number" style={[styles.InputText,this.state.contactNumberError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#e1e1e1'
      keyboardType="numeric"
      autoCapitalize="none"
      autoCorrect={false}
      ref={(input)=>this.contactInput=input}
      value={this.state.ContactNumber}
       onChangeText={(text) => this.setState({ContactNumber:text})}
       ></TextInput>
         </View>
         <Text style={styles.stickDiv}>*</Text>
          </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={(e)=>{this.onCompanySubmit()}}>Update Profile </Text>
         </View>
       </View> }
</ScrollView>

       </View>
     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
  
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

backgroundColor:'#F06124',
borderRadius: 4,
paddingVertical : 20,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
fontSize:18,
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    /*borderColor: '#dd4b39',
    borderWidth: 2,*/
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
    fontSize:16
  },
  inputDiv : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:20},
inputDivSecond : {paddingLeft : 25,paddingVertical:10,flexDirection:'row',paddingRight:30}
,stickDiv : {
  color:'red',fontSize:22,paddingLeft:5
},
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    backgroundColor: 'white',
    color:'#484848'
  },
   icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: 'gray',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  }
});