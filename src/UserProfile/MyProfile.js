import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Modal,ActivityIndicator,Alert,Keyboard,FlatList,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Dropdown } from 'react-native-material-dropdown';
import RadioButton from 'radio-button-react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import authData from '../../headerData';
export default class MyProfile extends Component {
    constructor(props){
    super(props);
    global.count = 0;
  
    this.state ={
      accountType : 'Individual',
      visible: false,
      sendFrom : 'Australia - AUD',
      sendTo : 'India',
      deliverIn : 'INR',
      amount : '',
      result : false,
      validAmountErr:false,
      amountCheck:false,
      CustomerInfo : [],
      deliveryMethod : [],
      delievryCurrency : [],
      oldMsgError : false,
      newMsgError : false,
      oldPassword : '',
      newPassword :'',
      isSucess : false
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
    //this.getSenderCountry();
    this.getUserDetails();
 }

 async getUserDetails(){
    var authDataToken =authData();
         try
         { 
             let response = await fetch('http://api.remitanywhere.com/restapi/customer/profile/'+global.CustomerKey, 
             { 
                 method: 'GET',
                     headers: 
                     { 
                         'Accept': 'application/json', 
                         'Content-Type': 'application/json', 
                         'Authorization' : authDataToken,
                         
                         }
                 }); 
                 let res = await response.text(); 
                     console.log(res);
                     let result= JSON.parse(res); 
                     var jsonResult =  result.Status[0];
                     if(jsonResult.errorCode == 1000){
                             var CustomerInfo = result.CustomerInfo[0];
                             var FirstName = CustomerInfo.FirstName=='null'?'':CustomerInfo.FirstName;
                            var LastName =  CustomerInfo.LastName=='null'?'':CustomerInfo.LastName;
                            var MiddleName= CustomerInfo.MiddleName=='null'?'':CustomerInfo.MiddleName;
                             global.userName = FirstName+' '+LastName;
                          //   console.log(global.userName);
                             this.setState({CustomerInfo : result.CustomerInfo})
                            // console.log(this.state.CustomerInfo);
                     }else{
                        let errors = await response;
                        throw errors;
                     }
     }catch (errors) { 
                      console.log('catch');
                      console.log(JSON.stringify(errors));
                    }
  }


             varifyStatus(){
              var CustomerInfo = this.state.CustomerInfo;
             }
   
              pressNav(){
                Keyboard.dismiss();
                
                this.props.navigation.navigate("DrawerOpen");
              }
              isCheck(item){
               console.log(item);
             
                if(item.IsApproved==0){
                  return(
                    <Text style={{color:'red'}}>Un Verified</Text>
                  )
                                    }else if(item.IsApproved==1){
                return(
                  <Text style={{color:'green'}}>Verified</Text>
                    )
                  }else if(item.IsApproved==2){
                    return(
                      <Text style={{color:'red'}}>Approval Denied</Text>
                    )

                  }else if(item.IsApproved==3){
                    return(
                      <Text style={{color:'green'}}>Waiting For Approval</Text>
                    )

                  }else if(item.IsApproved==4){
                    return(
                      <Text style={{color:'red'}}>Un Verified</Text>
                    )

                  }else if(item.IsApproved==5){
                    return(
                      <Text style={{color:'red'}}>Un Verified</Text>
                    )

                  }else if(item.IsApproved==6){
                    return(
                      <Text style={{color:'red'}}>Un Verified</Text>
                    )

                  }else if(item.IsApproved==7){
                    return(
                      <Text style={{color:'red'}}>Un Verified</Text>
                    )

                  }else if(item.IsApproved==8){
                    return(
                      <Text style={{color:'red'}}>Kyc Rejected</Text>
                    )

                  }else{
                    return(
                      <Text style={{color:'green'}}>In Progress</Text>
                    )
                  }
              }
              
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
           <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
    <ActivityIndicator color = '#F06124' size = "large" />
    
   </View>
   </View>
   </Modal>
     {/*  <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('../img/bgImage.png')} 
         />
       </View> */}
       <View style={styles.textContainer}>
        <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',fontSize: 18}}>User Information</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold',fontSize: 18}} onPress={()=>navigate('Home')}>  <FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <ScrollView style = {styles.scrollViewStyle}>
        {/*<View style={styles.logoContainer}>
         <Image
           source ={require('../img/app-logo-small.png')} 
         />
         
         </View> */}
        
         
       <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
       <View style={{borderColor:'gray',borderRadius:4,borderWidth:1,overflow:'hidden',marginHorizontal:5,paddingVertical:10,marginVertical:10,backgroundColor:'#eee'}}>
       <View style={{paddingHorizontal : 10,paddingVertical:10,flexDirection:'row'}}>
          <Text style={styles.detailHeading}>User's Information:</Text>
          <View style={{flex:0.1,flexDirection:'row'}}>
            <Text style={{textAlign:'right'}} onPress={()=>navigate('UpdateMyProfile')}> <FontAwesome style={{fontSize: 18,color:'#000',textAlign:'center'}}>{Icons.pencil}</FontAwesome> </Text>
          </View>
      </View>
      <FlatList 
                      data={this.state.CustomerInfo}
					  keyExtractor={(x,i)=>i}
                      renderItem={({item})=>  
<View style={{paddingVertical:10,flexDirection:'column',}}>

      <View style={styles.labelContainerDiv}>
              
               <Text style={{fontSize:18}}>
                {item.FirstName} {item.LastName}
                </Text>
         </View> 
         {/* <View style={styles.labelContainerDiv}>
               <View style={styles.labelDiv}>
               <Text>
                   Join Date:
                </Text>
               </View>
               <View style={styles.blankDiv}></View>
               <View style={styles.valueDiv}>
               <Text style={styles.textDetails}>
               Dec 17, 2017
               </Text> 
               </View>
     
         </View>  */}
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                   Location 
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>             
              <View style={styles.valueDiv}>
                  <Text style={styles.textDetails}>{item.CityName}, {item.CountryName}</Text>                   
             </View>    
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
            <Text>
            Customer Account#
            </Text>
        </View>
        <View style={styles.blankDiv}></View>
        <View style={styles.valueDiv}>
             <Text style={styles.textDetails}>
       {item.PinCode}
             </Text> 
        </View>
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                  Contact No
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                  <Text style={styles.textDetails}>{item.MobileNo}</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                  Email Id
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>{item.EmailAddress}</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                  Email Verified
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
              <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>Yes</Text> 
                   
             </View>
     
         </View>
         <View style={styles.labelContainerDiv}>
         <View style={styles.labelDiv}>
                   <Text>
                KYC Status
                  </Text>
              </View>
              <View style={styles.blankDiv}></View>
              
            <View style={styles.valueDiv}>
                    
             
                  <Text style={styles.textDetails}>
                  {/* {item.CustomerID1No=='' && item.CustomerID1No == null &&<Text style={{color:'red'}}>Unverified</Text>}
                  {item.CustomerID1No!=''&&<Text>
                  {item.IsApproved==1&&<Text style={{color:'green'}}>Verified</Text>}
                  {item.IsApproved!=1&&<Text style={{color:'green'}}>In Progress</Text>}
                  
                  </Text>} */}
                  {this.isCheck(item)}
                  </Text> 
                   
             </View>
     
         </View>

</View>
}
/>
</View>    

  
      
     
    
  
       
       </View>
       </ScrollView>
       
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  errorText : {
    color : '#dd4b39',
    paddingVertical : 10,
  },
  detailHeading : {color:'#094F66',fontSize:16,fontWeight:'bold',flex:0.9},
  labelContainerDiv : {paddingHorizontal : 10,paddingVertical:4,flexDirection:'row',},
  labelDiv:{flex:0.4,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
  valueDiv : {flex:0.5,borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:8},
  blankDiv : {flex:0.1,borderBottomColor:'gray',borderBottomWidth:1,},
});