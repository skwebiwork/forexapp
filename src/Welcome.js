import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,Alert,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import RadioButton from 'radio-button-react-native';
import SideBar from '../sidebar';
export default class Welcome extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      
    } 
  }
  componentDidMount() {
   // StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
 }
 onSubmit(){
    const { navigate } = this.props.navigation;
    Alert.alert(
        'Message',
        'Are you sure want to logout!',
        [
            {text : 'Cancle', onPress:()=>{console.log('cancel')}},
          {text : 'OK', onPress:()=>{navigate('Forth')}}
        ]
       );
   
  
     
 }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:40,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
            <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Dashoboard</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}}>x</Text>
            </View>
        </View>

        <View style={styles.logoContainer}>
         <Image
           source ={require('./img/app-logo-small.png')} 
         />
        
         </View>
        
        
       <View style={{paddingVertical:100,paddingHorizontal:30,flexDirection:'column',}}>
       
       
        <Text style={{color:'#fff',fontSize:18,textAlign:'center',fontWeight:'bold',paddingTop:20}}>Welcome BFX Australia</Text>
        <Text style={{color:'#fff',fontSize:12,textAlign:'center',fontWeight:'normal',paddingVertical:5}} onPress={()=>navigate('Logout')}>Logout</Text>
        
         
       </View>
       
       </View>
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column'
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:12,textAlign:'center',},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
marginTop:'40%',
marginBottom:'5%',
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
}
});