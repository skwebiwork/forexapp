import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,KeyboardAvoidingView,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
export default class Fifth extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      isCompany : false,
      
    } 
  }
  componentDidMount() {
  //  StatusBar.setHidden(true);
 }
 handleOnRadioClick(value){
   this.setState({accountType : value});
   console.log('text'+value);
   if(value=='Individual'){
    this.setState({isCompany : false});

   }else{
    this.setState({isCompany : true});

   }
 }
 onSubmit(){
  global.firstName = this.state.firstName;
  global.lastName = this.state.lastName;
  global.middleName = this.state.lastName;
  global.addressLine1 = this.state.addressLine1;
  global.addressLine2 = this.state.addressLine2;
  if(this.state.accountType=='Individual'){
    global.isCompany = false;
     global.businessName = "";
    global.businessLicenseNumber = "";
  }else{
    global.isCompany = true;
    global.businessName = this.state.businessName;
    global.businessLicenseNumber = this.state.businessLicenseNumber;
  }
  global.countryKey = this.state.countryKey;
  const { navigate } = this.props.navigation;
                     navigate('Sixth');
 }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}>
    <KeyboardAvoidingView
    style={styles.container}
    behavior="padding" >
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
        <View style={{height:40,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'left',color:'#fff',paddingLeft:15}} onPress={()=>navigate('Seventh')}><FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.chevronLeft}</FontAwesome></Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Register</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Seventh')}><FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Personal Information</Text>
       </View>
       <ScrollView style = {styles.scrollViewStyle}>
       <View style={{flexDirection:'row',backgroundColor:'transparent',alignItems:'center',paddingHorizontal:30,marginTop:15}}>
             <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,paddingLeft:20,borderTopLeftRadius:3,borderBottomLeftRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' outerCircleColor ='#F06124' value={'Individual'} onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Individual</Text>
                 </RadioButton></View>
                 <View style={{flex:.5,backgroundColor:'#fff',paddingVertical:10,borderTopRightRadius:3,borderBottomRightRadius:3}}>
             <RadioButton currentValue={this.state.accountType} innerCircleColor ='#000' value={'Company'} outerCircleColor ='#F06124' onPress={this.handleOnRadioClick.bind(this)}>
                <Text  style={styles.radioText}>Company</Text>
                 </RadioButton></View>
       </View>
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onSubmitEditing={()=>this.middleNameInput.focus()}
      onChangeText={(text) => this.setState({firstName:text})}
       ></TextInput>
         </View>
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Middle Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.middleNameInput=input}
      onSubmitEditing={()=>this.lastNameInput.focus()}
      onChangeText={(text) => this.setState({middleName:text})}
       ></TextInput>
         </View> */}
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Surname" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.lastNameInput=input}
     
      onChangeText={(text) => this.setState({lastName:text})}
       ></TextInput>
         </View>

           {this.state.isCompany&& <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Business Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({businessName:text})}
       ></TextInput>
         </View>}
             {this.state.isCompany&& <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="License No" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
         onChangeText={(text) => this.setState({businessLicenseNumber:text})}
       ></TextInput>
         </View>
       }
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Address" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({addressLine1:text})}
       ></TextInput>
         </View>
        {/* <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Address Line 2" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({addressLine2:text})}
       ></TextInput>
         </View>*/}
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <TextInput
       placeholder="Country" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.setState({countryKey:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
         <Text style={styles.next} onPress={(e)=>{this.onSubmit()}}>Continue to register </Text>
         </View>
       </View>
</ScrollView>

       </View>
     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column'
},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {

backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
}
});