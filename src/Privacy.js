import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,SafeAreaView
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
export default class Privacy extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
     
      visible: false,
      
    } 
  }
  render() {
   
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
       <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never"> 
       <Text style={{paddingHorizontal:10,paddingVertical:5,color:'#000'}}>This Privacy Policy applies to the collection, storage, use and disclosure by BFX Australia Pty Ltd
(ABN 89 611 376 820) (<Text style={styles.highLight}>BFX, we, us</Text>) of personal information in respect of individuals (you). It also
provides information on how you can obtain access to, or seek correction of, your personal
information. BFX is committed to the protection of personal information and has developed this
Privacy Policy in line with the Australian Privacy Principles in the Privacy Act 1988 (Cth) (<Text style={styles.highLight}>Privacy
Act</Text>).</Text>
     
     <Text style={{paddingHorizontal:10,paddingVertical:5,color:'#000'}}>
     You agree that providing us with your personal information as defined below implies your consent to
the collection and use of your personal information in accordance with this Privacy Policy. We may
change this Privacy Policy from time to time. BFX encourages you to periodically review this page
for the latest information on our privacy practices. Your use of our website following any such
change to our Privacy Policy will confirm your acceptance of the changes. You may request BFX to
provide you with this Privacy Policy in hard copy.

     </Text>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>1.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}>Personal Information</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}>Personal information is any information or opinion about an identified individual, or an
individual who is reasonably identifiable whether the information or opinion is true or not and
whether the information or opinion is recorded in material form or not. Personal information
commonly includes a person’s name, occupation, signature, residential address, email
address, telephone number, bank account details, date of birth, passport number, driver’s
licence number etc. (<Text style={{color:'#000',fontWeight:'bold'}}>Personal Information</Text>)</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>2.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}>Method of collection</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>2.1.</Text>
     <Text style={{flex:0.9}}>BFX collects Personal Information when you:</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • complete the form on the “Start Now” portal on the <Text style={{textDecorationLine: 'underline',color:'blue'}}>www.bfxaustralia.com.au </Text>website </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • complete the BFX corporate registration form; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • send information in emails or correspondence to BFX; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • speak to a staff member of BFX. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>2.2.</Text>
     <Text style={{flex:0.9}}>BFX may access third party electronic databases to identify you and may collect Personal
Information in this process. These databases may contain information about your
creditworthiness, including consumer credit reports from a credit reporting body.</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>3.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> The type and scope of collection </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>3.1</Text>
     <Text style={{flex:0.9}}>Every visit to our Website and each inquiry made from our Website is logged by the IP
address of the computer from which it was requested. This information is saved by us for
internal system-related and statistical purposes. The following information is logged: the
name of the inquirer, the date and the time of the inquiry, the IP address, the notification of a
successful inquiry.</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>3.2</Text>
     <Text style={{flex:0.9}}>BFX is required to collect certain Personal Information for purposes of complying with the
Anti-Money Laundering and Counter-Terrorism Financing Act 2006 (Cth) and related
legislation (<Text style={styles.highLight}>AML legislation</Text>). For all corporate clients (including without limitation,
registered domestic and foreign companies, registered associations, registered cooperatives,
trusts, partnerships, government bodies or other statutory bodies ( <Text style={styles.highLight}>Corporate
Clients</Text>) this may include requesting Personal Information of individuals (being a natural
person or persons) who ultimately own or control (whether directly or indirectly) the
Corporate Client. Where such Personal Information is obtained from you or your authorised
representative(s), you are taken to have informed such persons of its contents.</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>4.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Purpose of Collecting Your Personal Information </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>4.1</Text>
     <Text style={{flex:0.9}}>collection of Personal Information is required to enable us to provide our services and to
ensure the highest quality of service provision. Individuals do not have to supply BFX with
their Personal Information, however, if the individual chooses not to do so BFX may be
unable to provide the services required or sought. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}>4.2</Text>
     <Text style={{flex:0.9}}> BFX collects Personal Information for the following purposes: </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to provide its services and products and undertake associated business processes and
functions. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to process registration with BFX;</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to administer and manage registration with BFX; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to identify you; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to monitor, develop and improve the quality of its services; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • if you are registered with BFX, to send you information that is relevant to the provision of
its services; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to answer or process inquiries or complaints, and provide information or advice; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to notify you of any products, services and benefits BFX provides and may be of interest
to you; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • to comply with any law, rule or regulation or binding determination, or to cooperate with
     any governmental authority; and </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1}}></Text>
     <Text style={{flex:0.9}}> • any other purpose disclosed to you at the time BFX collects your Personal Information. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>5.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Direct Marketing</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> BFX may use Personal Information for the purpose of direct marketing where you would
reasonably expect BFX to use or disclose the information for the purpose of direct
marketing, unless you have requested not to receive such direct marketing communications.
At any time you may opt-out of receiving direct marketing communications through the
unsubscribe function that will be made available with direct marketing communications that
BFX sends. Alternatively, you may opt-out of receiving direct marketing communications by
emailing us at <Text style={{textDecorationLine: 'underline',color:'blue'}}>accounts@bfxaustralia.com.au</Text>.</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>6.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Storage and Security of Your Personal Information </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>6.1</Text>
     <Text style={{flex:0.9,}}>  From time to time we may hold Personal Information in any combination of data storage
facilities, such as for example G-remit, GBG software, cloud computing facilities or paper
based files which may be operated or held by us. We will use all reasonable endeavours to
maintain the security of your Personal Information from unauthorised access, modification,
or disclosure. We cannot ensure or warrant that your Personal Information will always be
secure during transmission or protected from unauthorised access during storage therefore
you provide your Personal Information to us at your own risk. Please contact us immediately
if you become aware or have reason to believe there has been any unauthorised use of your
Personal Information held by BFX.  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>6.2</Text>
     <Text style={{flex:0.9,}}> Unless otherwise required by law, BFX will store Personal Information for a long as it is
required to provide a service requested of BFX or to which you have given consent. BFX is
obliged to retain information relating to personal identity for 7 years under relevant AML
legislation. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>7.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Use and Disclosure of Your Personal Information  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>7.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Use and Disclosure of Your Personal Information </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>7.1</Text>
     <Text style={{flex:0.9,}}> BFX will use and disclose Personal Information to third parties only for the purposes
described below. You hereby consent to BFX making your Personal Information available: </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to BFX contractors or service providers to conduct our business and to provide our
     services or products to you, including web hosting providers, IT systems
     administrators, mailing houses, couriers, payment processors, data entry service
     providers, electronic network administrators, debt collectors, and professional
     advisors such as accountants, solicitors, business advisors and consultants;</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to any partners, agents or intermediaries who are a necessary part of the provision
of BFX’s products and services; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to intermediary banks engaged by BFX in the processing of certain transactions; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to international intermediaries to complete transactions;  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to credit reporting bodies; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to any government body that requests such information or is required by law to
obtain such information, as for example the Australian Transaction Reports and
Analysis Centre with regulatory responsibility under the AML Legislation; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to our related bodies corporate (if any).Contacting you to ask you to provide
feedback about the services. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>7.2</Text>
     <Text style={{flex:0.9,}}> Where practicable we will endeavour to collect, hold, use and disclose personal information
     for the purposes described above on a de-identified basis. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>7.3</Text>
     <Text style={{flex:0.9,}}> You acknowledge that your personal information is likely to be disclosed to overseas
     recipients referred to in paragraph 7.1 of this policy. The location of such overseas recipient
     will depend on the country to which the relevant transaction relates. The likely countries that
     your personal information may be sent to include countries in Europe, the Middle East, Asia
     and America. For a full list of countries to which your personal information may be sent
     please refer to bfxaustralia.com.au or request such list from
     accounts@bfxaustralia.com.au. You expressly consent to BFX disclosing your personal
     information to recipients located outside Australia and you acknowledge in respect of such
     disclosure that a) you will not be able to seek redress under the Privacy Act, b) the overseas
     recipient may not be subject to any privacy obligations or to any principles similar to the
     Australian Privacy Principles, c) you may not be able to seek redress in the overseas
     jurisdiction; and d) the overseas recipient may be subject to a foreign law that could compel
     the disclosure of personal information to a third party, such as an overseas authority. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>7.2</Text>
     <Text style={{flex:0.9,}}> Where practicable we will endeavour to collect, hold, use and disclose personal information
     for the purposes described above on a de-identified basis. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>8.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Access, Amendment and Deletion of Your Personal Information</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>8.1</Text>
     <Text style={{flex:0.9,}}> We will use all reasonable endeavours to keep your Personal Information accurate,
     complete, up-to-date, relevant and not misleading. Please contact us to examine your
     Personal Information if required and we will provide a complete list of your Personal
     Information within a reasonable period of receipt of your request. You may contact us to
     amend any of your Personal Information that is inaccurate, incomplete or out-of-date or
     request that your personal information be deleted. We will amend or delete your records as
     requested within a reasonable period of receipt of notice. If applicable, any legal
     requirement on us to maintain certain records of your personal information shall prevail over
     any of your requests. BFX may require identification to be provided before releasing copies
     of Personal Information. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>8.2</Text>
     <Text style={{flex:0.9,}}> If you wish to examine or amend your Personal Information, please contact our privacy
     officer at: </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <View style={{flex:0.9,flexDirection:'column'}}> 
          <Text style={{textAlign:'center',paddingVertical:3}}>accounts@bfxaustralia.com.au</Text>
          <Text style={{textAlign:'center',paddingVertical:3}}>Or</Text>
          <Text style={{textAlign:'center',paddingVertical:3}}>BFX Australia Pty Ltd </Text>
          <Text style={{textAlign:'center',paddingVertical:3}}>
Level 23, 52 Martin Place </Text>
          <Text style={{textAlign:'center',paddingVertical:3}}>
Sydney New South Wales 2000 </Text>
          <Text style={{textAlign:'center',paddingVertical:3}}>Australia</Text>
      </View>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>8.3</Text>
     <Text style={{flex:0.9,}}> There may be circumstances which preclude BFX from providing access to some or all of
     your Personal Information. For example, those circumstances could include:  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • the information may impact on the privacy of other individuals; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • the information is commercially sensitive evaluative information; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • the information is subject to solicitor-client or litigation privilege; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • BFX is prohibited by law from providing you with access; or </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • the disclosure could reasonably be expected to threaten the safety, physical or
mental health or life of an individual </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}>8.4</Text>
     <Text style={{flex:0.9,}}> If BFX decides that it cannot grant you access to your Personal Information, or grant access
     in the manner requested by you, it will set out the reasons for its decision in writing and the
     mechanisms available to complain about the refusal.  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>9.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Making a Complaint</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> If you have any concerns about how we manage your personal information, you may write to
     our Privacy Officer at accounts@bfxaustralia.com.au. We will reasonably endeavour to
     provide a response within 30 days of receipt of your complaint. </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> If you are not satisfied with our response, you may lodge a complaint with the Office of the
     Australian Information Commissioner (OAIC) by writing to the OAIC at GPO Box 5218,
     Sydney NSW 2001. For further information about the OAIC, please visit <Text style={{textDecorationLine: 'underline',color:'blue'}}>www.oaic.gov.au. </Text> </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,color:'#000',fontWeight:'bold'}}>10.</Text>
     <Text style={{flex:0.9,color:'#000',fontWeight:'bold'}}> Web Tracking</Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> BFX and its marketing partners, affiliates, or analytics or service providers, use technologies
     such as cookies, beacons, tags, and scripts, to analyse trends, administer the website,
     tracking users’ movements around the website, and to gather demographic information
     about our user base as a whole. BFX may receive reports based on the use of these
     technologies by these companies on an individual and aggregated basis. The use of such
     technologies is for a variety of purposes and may include  </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to assist us in providing services to you; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to allow you to change pages during your visit to our Website without having to reenter
     your password; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to temporarily track activity on our Website; </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to determine whether you came to our site from a banner ad or an affiliated website;
     or </Text>
     </View>
     <View style={{flexDirection:'row',flex:1,paddingHorizontal:10,paddingVertical:5,}}>
     <Text style={{flex:0.1,}}></Text>
     <Text style={{flex:0.9,}}> • to identify you when you visit the Website, to personalize the content of the Website
     for you and to assist you in carrying out transactions </Text>
     </View>
     </ScrollView>
     </View>
   </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
 
  flexDirection:'column',
 
  marginBottom : 80
},
imageContainer : {
    position: 'absolute',
    
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',
flexDirection:'column',

},
highLight : {
    color:'#000',fontWeight:'bold'
},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {
  paddingHorizontal : 25,
backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
}
});