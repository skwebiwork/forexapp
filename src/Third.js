import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,SafeAreaView
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
export default class Third extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
     
      visible: false,
      
    } 
  }
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
     <View
       style={styles.container}>
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             width:null
           }}
           source ={require('./img/step-hdpi.jpg')} 
           resizeMode={'cover'}
         />
       </View>
       <View
         style={styles.textContainer}>
           <View style={{paddingHorizontal : 25,flex:4}}>
         </View>
         <View style={{paddingHorizontal:25}}>
         <Text style={styles.next} onPress={()=>navigate('Forth')}>Next</Text>
         </View>
         <Text style={styles.bottomInstructions}>With very competetive rates and a simple process, we'll help you pay your suppliers in over 200 countries.</Text>     
       </View> 
     </View>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
 
  flexDirection:'column',
 
},
imageContainer : {
    position: 'absolute',
    
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',
flexDirection:'column',

},
logoContainer: {flex:1,flexDirection:'column',  alignItems: 'center',},
logoText:{color:'#fff',fontSize:18,textAlign:'center',paddingVertical:15},
next : {
  paddingHorizontal : 25,
backgroundColor:'#F06124',
borderRadius: 18,
paddingVertical : 10,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
}
});