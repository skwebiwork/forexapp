import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,ScrollView,KeyboardAvoidingView, Keyboard,SafeAreaView
  
} from 'react-native';
import { StatusBar } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import RadioButton from 'radio-button-react-native';
export default class Fifth extends Component {
    constructor(){
    super();
    global.count = 0;
    this.state ={
      accountType : 'Individual',
      visible: false,
      email : '',
      visible: false,
      password : '',
      cpassword:'',
      errorMsg : '',
      passwordMsg : '',
      isError : false,
    } 
  }
  componentDidMount() {
  //  StatusBar.setHidden(true);
 }

 validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
onSubmit(){
  // console.log('click');
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
   var email = this.state.email;
   var password = this.state.password;
   if(email=='') {
     this.setState({ isError:true,emailError: true,passwordError: false })
     this.setState({errorMsg : 'Email must be required'})
      validMail = true
   }else if(reg.test(email) === false){
     this.setState({ emailError: true,isError:true,passwordError: false})
    this.setState({errorMsg : 'Please enter a valid email id'})
   }else if(this.state.password=='') {
    // this.setState({ passwordError: false })
     this.setState({ emailError:false,errorMsg:'Password must be required',isError:true,passwordError:true })
     validPass = true
   }else if(this.state.password.length <= 3) {
     // this.setState({ passwordError: false })
      this.setState({ emailError: false,isError:true,passwordError:true,errorMsg:'Your password must consist of at least 8 characters (max: 15)' })
      validPass = true
    }else if(this.state.password.length >= 16) {
     // this.setState({ passwordError: false })
      this.setState({ emailError: false,isError:true,passwordError:true,errorMsg:'Your password must consist of at least 8 characters (max: 15)' })
      validPass = true
    }else if(this.state.cpassword== ''){
      this.setState({ emailError: false,isError:true,passwordError:false,cpasswordError : true,errorMsg:'Confirm Password must be required.' })
 
     }else if(this.state.cpassword!= this.state.password){
     this.setState({ emailError: false,isError:true,passwordError:false,cpasswordError : true,errorMsg:'Confirm Password does not match password.' })

    }else{
     this.setState({ emailError: false,passwordError: false,isError:false,passwordMsg:'',cpasswordError : false })
      
     global.customerLoginId = this.state.email;
    global.customerPassword = this.state.password;
    const { navigate } = this.props.navigation;
                     navigate('Fifth');
   }
   
 }
onSubmit11(){
  console.log('click');
  var validMail = false;
  var validPass = false;
  var cvalidPass = false;

  if (this.validateEmail(this.state.email)) {
    this.setState({ emailError: false })
    validMail = false
  } else {
    this.setState({ emailError: true })
    validMail = true
  }

  if (this.state.password.length >= 3) {
    this.setState({ passwordError: false })
    validPass = false

  } else {
    this.setState({ passwordError: true })
    validPass = true
  }
  if (this.state.cpassword== this.state.password) {
    this.setState({ cpasswordError: false })
    cvalidPass = false

  } else {
    this.setState({ cpasswordError: true })
    cvalidPass = true
  }

  if (!validMail && !validPass && !cvalidPass) {
    console.log('true',validMail);
    //this.loginFun();
    global.customerLoginId = this.state.email;
    global.customerPassword = this.state.password;
    const { navigate } = this.props.navigation;
                     navigate('Fifth');
  }
}

pressNav(){
  Keyboard.dismiss();
  
  this.props.navigation.navigate("DrawerOpen");
}
  render() {
    const { navigate } = this.props.navigation;
   return (
      <SafeAreaView style={{flex: 1,backgroundColor:'#fff' }}>
    <KeyboardAvoidingView
    style={styles.container}
    behavior="padding" >
       <View
         style={styles.imageContainer}>
         <Image
           style={{
             flex: 1,
             
           }}
           source ={require('./img/bgImage.png')} 
         />
       </View>
       <View style={styles.textContainer}>
     
       
          <View style={{height:60,backgroundColor:'#F06124',flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.2}}>
              {/* <Text style={{textAlign:'left',color:'#fff',paddingLeft:15}} onPress={()=>navigate('Forth')}>Back</Text> */}
               <Text style={{backgroundColor : 'transparent',paddingLeft:10}} onPress={()=>this.pressNav()} underlayColor={'transparent'}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
            <View style={{flex:.6}}>
               <Text style={{color:'#fff',textAlign:'center',fontWeight:'bold'}}>Register</Text>
            </View>
            <View style={{flex:.2}}>
               <Text style={{textAlign:'right',color:'#fff',paddingRight:15,fontWeight:'bold'}} onPress={()=>navigate('Forth')}><FontAwesome style={{fontSize: 18,color:'#fff',textAlign:'center'}}>{Icons.close}</FontAwesome></Text>
            </View>
        </View>
        <ScrollView style = {styles.scrollViewStyle} keyboardShouldPersistTaps="never"> 
        <View style={styles.logoContainer}>
         <Image
           source ={require('./img/app-logo-small.png')} 
         />
         <Text style={styles.logoText}>Our stramlined two minute registration process is designed to protect your identify & money. Select the account which your require</Text>
         </View>
        
         <View style={{height:40,backgroundColor:'#094F66',}}>
             <Text style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingTop:10}}>Choose an ID and password</Text>
       </View>
       <View style={{paddingVertical:10,flexDirection:'column',}}>
       
       <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
       {this.state.isError===true && <Text style={styles.errorText}>{this.state.errorMsg} </Text>}
        <TextInput
       placeholder="Email Id" style={[styles.InputText,this.state.emailError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
      onSubmitEditing={()=>this.passwordInput.focus()}
      onChangeText={(text) => this.setState({email:text})}
      
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingTop:10,}}>
       
        <TextInput
       placeholder="Password" style={[styles.InputText,this.state.passwordError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry={true}
      ref={(input)=>this.passwordInput=input}
      onSubmitEditing={()=>this.cpasswordInput.focus()}
      onChangeText={(text) => this.setState({password:text})}
       ></TextInput>
         
         </View>
         <Text style={{paddingHorizontal : 25,textAlign:'right',color:'#fff',fontSize:12,paddingTop:6,paddingBottom:10,}}>Max Length 15 character</Text>      

         <View style={{paddingHorizontal : 25,paddingVertical:10,}}>
        
        <TextInput
       placeholder="Confirm Password" style={[styles.InputText,this.state.cpasswordError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="done" 
       placeholderTextColor='#484848'
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry={true}
      ref={(input)=>this.cpasswordInput=input}
      onChangeText={(text) => this.setState({cpassword:text})}
       ></TextInput>
         </View>
         <View style={{paddingHorizontal : 25,paddingVertical:10,marginTop:40}}>
         <Text style={styles.next}  onPress={(e)=>{this.onSubmit()}}>Continue to register </Text>
         </View>
       </View>

</ScrollView>
       </View>
    
     </KeyboardAvoidingView>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({

container : {
  flex: 1,
  backgroundColor: '#eee',
  flexDirection:'column',

},
imageContainer : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
},
textContainer: {
flex: 1,
backgroundColor: 'transparent',


},
logoContainer: { alignItems: 'center',paddingVertical:35},
logoText:{color:'#fff',fontSize:15,textAlign:'center',paddingVertical:15},
next : {
backgroundColor:'#F06124',
borderRadius: 8,
paddingVertical : 14,
textAlign:'center',
color:'#fff',
overflow:"hidden",
fontWeight:'bold',
// backgroundColor:'#F06124',
// borderRadius: 18,
// paddingVertical : 10,
// textAlign:'center',
// color:'#fff',
// overflow:"hidden",
// fontWeight:'bold',
},
bottomInstructions : {
color:'#fff',
fontSize:14,
textAlign:'center',
paddingVertical:25,
paddingHorizontal:10,
},
InputText : {
  paddingVertical : 10,
  borderRadius:3,
  borderColor: '#fff',
   borderWidth: 1,
   paddingHorizontal: 5,
   backgroundColor:'#fff'
  
},
radioText : {
  paddingHorizontal:7,
 color:'#000',
  justifyContent : 'center',
},
scrollViewStyle : {
  paddingVertical:'1%',
},
errorInput : {
  borderColor: '#dd4b39',
  borderWidth: 2,
},
errorText : {
  color : '#dd4b39',
  paddingVertical : 10,
}
});